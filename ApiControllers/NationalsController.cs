﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NationalsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public NationalsController(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }

        [HttpPost]
        [Route("addorupdatematerials")]
        public async Task<ActionResult> AddOrUpdateMaterials(NationalMaterialDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                if (user.Type != UserType.System) throw new Exception("You are not permitted to add or update national materials");
                //clear existing
                var existing = db.NationalMaterials.Where(x => x.Id> 0);
                db.NationalMaterials.RemoveRange(existing);
                foreach (var matId in model.MaterialId)
                {
                    db.NationalMaterials.Add(new NationalMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();
                return Created("AddOrUpdateMaterials", new { Message = "Added Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getmaterials")]
        public async Task<ActionResult> GetMaterials()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                
                var data = db.NationalMaterials.Where(x => x.Id > 0).Include(x => x.Material).ToList().Select(x => new {
                    x.Id,
                    x.MaterialId,
                    x.Material.Name
                }).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }



    }
}
