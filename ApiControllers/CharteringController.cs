﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using RoyalRangersAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class CharteringController : BaseController<Chartering>
    {
        public CharteringController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {

            return BadRequest("Not Implemented");
        }

        public override async Task<ActionResult> Get(long id)
        {

            try
            {
                var res = _context.Charterings.Include(x => x.Outpost.District.Region.Sector).ToList().Select(x => new
                {
                    x.Id,
                    x.OutpostId,
                    Outpost = x.Outpost.Name,
                    District = x.Outpost.District.Name,
                    Region = x.Outpost.District.Region.Name,
                    Sector = x.Outpost.District.Region.Sector.Name,
                    x.Date,
                    x.StartDate,
                    x.EndDate,
                    x.Fee,
                    Status = x.Status.ToString(),
                    x.CommanderId,
                    Commander = x.Commander.Name,
                    x.PastorId,
                    Pastor = x.Pastor.Name,
                    x.MmPresidentId,
                    MmPresident = x.MmPresident.Name,
                    x.TotalRangers,
                    x.NumberOfRagerKids,
                    x.NumberOfDiscoveryRangers,
                    x.NumberOfAdventureRangers,
                    x.NumberOfExpeditionRangers,
                    x.NumberOfChallengerRangers,
                    x.NumberOfCommanders,
                    x.VettedOn,
                    x.VettedBy,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Delete(long id)
        {

            return BadRequest("Not Implemented");
        }
        [HttpPost]
        [Route("charter")]
        public async Task<ActionResult> Charter(CharteringDTO model)
        {
            try
            {
                var dt = DateTime.UtcNow;
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized to access this feature");

                var bal = db.OutpostBalances.Where(x => x.OutpostId == user.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check your account");
                if (bal.Amount < Config.Setting.AppSettings.CharteringFee) throw new Exception("You do not have sufficient balance to charter");

                var executives = db.Executives.Where(x => x.OutpostId == bal.OutpostId).Include(x => x.Position).ToList();
                var rangers = db.Rangers.Where(x => x.OutpostId == bal.OutpostId && x.Status == RangerStatus.Active).ToList();

                //int startMonthDays = DateTime.DaysInMonth(dt.Year, Config.Setting.AppSettings.CharteringStartMonth);
                int endMonthDays = DateTime.DaysInMonth(dt.Year, Config.Setting.AppSettings.CharteringEndMonth);
                var rangerRepo = new RangerRepository(_context);
                var kids = rangers.Count(x => rangerRepo.GetRank(x.Age).ToLower().Contains("kids"));
                var discovery = rangers.Count(x => rangerRepo.GetRank(x.Age).ToLower().Contains("discovery"));
                var adventure = rangers.Count(x => rangerRepo.GetRank(x.Age).ToLower().Contains("adventure"));
                var expedition = rangers.Count(x => rangerRepo.GetRank(x.Age).ToLower().Contains("expedition"));
                var challenger = rangers.Count(x => rangerRepo.GetRank(x.Age).ToLower().Contains("challenger"));

                db.Charterings.Add(new Chartering
                {
                    OutpostId = bal.OutpostId,
                    Fee = Config.Setting.AppSettings.CharteringFee,
                    Status = CharteringStatus.Pending,
                    Date = dt,
                    StartDate = new DateTime(dt.Year, Config.Setting.AppSettings.CharteringStartMonth, 1),
                    EndDate = new DateTime(dt.Year, Config.Setting.AppSettings.CharteringEndMonth, endMonthDays),
                    CommanderId = model.CommanderId,
                    PastorId = model.PastorId,
                    MmPresidentId = model.MmPresidentId,
                    TotalRangers = rangers.Count(),
                    NumberOfCommanders= rangers.Count(x=> x.IsCommander),
                    NumberOfRagerKids = kids,
                    NumberOfDiscoveryRangers = discovery,
                    NumberOfAdventureRangers = adventure,
                    NumberOfExpeditionRangers = expedition,
                    NumberOfChallengerRangers = challenger,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    ModifiedBy = user.UserName,
                    CreatedBy = user.UserName
                });
                db.SaveChanges();
                return Created("NewChartering", new { Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        public override async Task<ActionResult> Post(Chartering model)
        {

            return BadRequest("Not Implemented");
        }
        public override async Task<ActionResult> Put(Chartering model)
        {

            return BadRequest("Not Implemented");
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(CharteringFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized access this feature");
                filter.OutpostId = user.OutpostId.Value;

                var raw = _repository.Query(filter);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.EndDate).Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Charter Data Found" });
                var data = raw.Include(x => x.Outpost.District.Region.Sector).OrderByDescending(x => x.Id).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.OutpostId,
                        Outpost = x.Outpost.Name,
                        District = x.Outpost.District.Name,
                        Region = x.Outpost.District.Region.Name,
                        Sector = x.Outpost.District.Region.Sector.Name,
                        x.Date,
                        x.StartDate,
                        x.EndDate,
                        x.Fee,
                        Status = x.Status.ToString(),
                        x.CommanderId,
                        Commander = x.Commander.Name,
                        x.PastorId,
                        Pastor = x.Pastor.Name,
                        x.MmPresidentId,
                        MmPresident = x.MmPresident.Name,
                        x.TotalRangers,
                        x.NumberOfRagerKids,
                        x.NumberOfDiscoveryRangers,
                        x.NumberOfAdventureRangers,
                        x.NumberOfExpeditionRangers,
                        x.NumberOfChallengerRangers,
                        x.NumberOfCommanders,
                        x.VettedOn,
                        x.VettedBy,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("admin/query")]
        public async Task<ActionResult> AdminQuery(CharteringFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized access this feature");
                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            filter.SectorId = user.SectorId.Value;
                            break;
                        }

                    case UserType.Regional:
                        {
                            filter.RegionId = user.RegionId.Value;
                            break;
                        }

                    case UserType.District:
                        {
                            filter.DistrictId = user.DistrictId.Value;
                            break;
                        }
                }

                var raw = _repository.Query(filter);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.EndDate).Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Charter Data Found" });
                var data = raw.Include(x => x.Outpost.District.Region.Sector).OrderByDescending(x => x.Id).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.OutpostId,
                        Outpost = x.Outpost.Name,
                        District = x.Outpost.District.Name,
                        Region = x.Outpost.District.Region.Name,
                        Sector = x.Outpost.District.Region.Sector.Name,
                        x.Date,
                        x.StartDate,
                        x.EndDate,
                        x.Fee,
                        Status = x.Status.ToString(),
                        x.CommanderId,
                        Commander = x.Commander.Name,
                        x.PastorId,
                        Pastor = x.Pastor.Name,
                        x.MmPresidentId,
                        MmPresident = x.MmPresident.Name,
                        x.TotalRangers,
                        x.NumberOfRagerKids,
                        x.NumberOfDiscoveryRangers,
                        x.NumberOfAdventureRangers,
                        x.NumberOfExpeditionRangers,
                        x.NumberOfChallengerRangers,
                        x.NumberOfCommanders,
                        x.VettedOn,
                        x.VettedBy,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("admin/approve")]
        public async Task<ActionResult> AdminApprove(long id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized access this feature");

                var rec = db.Charterings.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                if(rec.Status != CharteringStatus.Pending) throw new Exception("You are not allowed to approve this record because it is not pending");
                rec.Status = CharteringStatus.Approved;
                rec.VettedBy = user.UserName;
                rec.VettedOn = DateTime.Now;
                rec.ModifiedAt = DateTime.Now;
                rec.ModifiedBy = user.UserName;

                var bal = db.OutpostBalances.First(x => x.OutpostId == rec.OutpostId);
                if (rec.Fee > bal.Amount) throw new Exception($"The outpost has insufficient balance to charter.");
                bal.Amount = bal.Amount - rec.Fee;

                db.BalanceDebits.Add(new BalanceDebit
                {
                    OutpostBalanceId = bal.Id,
                    Amount = rec.Fee,
                    Date = DateTime.Now,
                    Type = BalanceDebitType.Chartering,
                    Notes = $"Approved Chartering::  ",
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    ModifiedBy = user.UserName,
                    CreatedBy = user.UserName
                });

                var outpost = db.Outposts.First(x => x.Id == rec.OutpostId);
                outpost.Status = OutpostStatus.Chartered;

                db.SaveChanges();
                return Created("AdminApprove", new { Message = "Approved Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("admin/reject")]
        public async Task<ActionResult> AdminReject(long id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized access this feature");

                var rec = db.Charterings.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                if (rec.Status != CharteringStatus.Pending) throw new Exception("You are not allowed to reject this record because it is not pending");
                rec.Status = CharteringStatus.Rejected;
                rec.VettedBy = user.UserName;
                rec.VettedOn = DateTime.Now;
                rec.ModifiedAt = DateTime.Now;
                rec.ModifiedBy = user.UserName;

                db.SaveChanges();
                return Created("AdminReject", new { Message = "Rejected Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("cancel")]
        public async Task<ActionResult> Cancel(long id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized access this feature");

                var rec = db.Charterings.FirstOrDefault(x => x.Id == id && x.OutpostId == user.OutpostId.Value);
                if (rec == null) throw new Exception("Please check the selected record");
                if (rec.Status != CharteringStatus.Pending) throw new Exception("You are not allowed to cancel this record because it is not pending");
                rec.Status = CharteringStatus.Cancelled;
                rec.VettedBy = user.UserName;
                rec.VettedOn = DateTime.Now;
                rec.ModifiedAt = DateTime.Now;
                rec.ModifiedBy = user.UserName;

                db.SaveChanges();
                return Created("Cancel", new { Message = "Cancelled Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }

}
