﻿using Microsoft.AspNetCore.Mvc;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class ExecutivePositionsController : BaseController<ExecutivePosition>
    {
        public ExecutivePositionsController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var res = _repository.Get().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    Type = x.Type.ToString()
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("Getbytype")]
        public async Task<ActionResult> GetByType(ExecutiveType type)
        {
            try
            {
                var res = _repository.Get().Where(x=> x.Type == type).Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    Type = x.Type.ToString()
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }

}
