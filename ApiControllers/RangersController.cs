﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using RoyalRangersAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class RangersController : BaseController<Ranger>
    {
        public RangersController(ApplicationDbContext context) : base(context)
        {
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(RangersFilter filter)
        {
            try
            {
                var repo = new RangerRepository(_context);
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Id > 0);
                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            raw = raw.Where(x => x.Outpost.District.Region.SectorId == user.SectorId);
                            break;
                        }

                    case UserType.Regional:
                        {
                            raw = raw.Where(x => x.Outpost.District.RegionId == user.RegionId);
                            break;
                        }

                    case UserType.District:
                        {
                            raw = raw.Where(x => x.Outpost.DistrictId == user.DistrictId);
                            break;
                        }
                    case UserType.Outpost:
                        {
                            raw = raw.Where(x => x.OutpostId == user.OutpostId);
                            break;
                        }
                }
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Outpost.District.Region.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Ranger Found" });
                var data = raw.OrderBy(x => x.Name).ThenBy(x=> x.Outpost.Name).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        //x.Photo,
                        x.Code,
                        x.Name,
                        x.PhoneNumber,
                        x.Email,
                        x.DateOfBirth,
                        x.Age,
                        x.IsCommander,
                        x.Occupation,
                        Gender = x.Gender.ToString(),
                        //x.ResidentialAddress,
                        //x.PostalAddress,
                        //x.SchoolOrWork,
                        //x.Hobbies,
                        //x.Allergies,
                        //x.MedicalHistory,
                        //x.GhanaPostGps,
                        x.FatherName,
                        x.FatherPhoneNumber,
                        //x.FatherPostalAddress,
                        //x.FatherResidentialAddress,
                        //x.FatherGhanaPostGps,
                        x.MotherName,
                        x.MotherPhoneNumber,
                        //x.MotherPostalAddress,
                        //x.MotherResidentialAddress,
                        //x.MotherGhanaPostGps,
                        Uniforms = repo.GetUniforms(x.UniformIds),
                        Siblings = x.Siblings.Count(),
                        x.OutpostId,
                        Outpost = x.Outpost.Name,
                        District = x.Outpost.District.Name,
                        Region = x.Outpost.District.Region.Name,
                        Sector = x.Outpost.District.Region.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        Transfers = repo.GetTransfers(x.Id).Count(),
                        Awards = repo.GetAwards(x.Id).Count(),
                        Rank = x.IsCommander ? "Commander" : repo.GetRank(x.Age)
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get()
        {
            try
            {
                var repo = new RangerRepository(_context);
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _context.Rangers.Where(x => x.Id > 0);
                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            raw = raw.Where(x => x.Outpost.District.Region.SectorId == user.SectorId);
                            break;
                        }

                    case UserType.Regional:
                        {
                            raw = raw.Where(x => x.Outpost.District.RegionId == user.RegionId);
                            break;
                        }

                    case UserType.District:
                        {
                            raw = raw.Where(x => x.Outpost.DistrictId == user.DistrictId);
                            break;
                        }
                    case UserType.Outpost:
                        {
                            raw = raw.Where(x => x.OutpostId == user.OutpostId);
                            break;
                        }
                }

                var res = raw.Include(x => x.Outpost.District.Region.Sector).Include(x => x.Siblings).OrderBy(x => x.Name).ThenBy(x => x.Outpost.Name).ToList().Select(x => new
                {
                    x.Id,
                    x.Photo,
                    x.Code,
                    x.Name,
                    x.PhoneNumber,
                    x.Email,
                    x.DateOfBirth,
                    x.Age,
                    x.IsCommander,
                    x.Occupation,
                    Gender = x.Gender.ToString(),
                    //x.ResidentialAddress,
                    //x.PostalAddress,
                    //x.SchoolOrWork,
                    //x.Hobbies,
                    //x.Allergies,
                    //x.MedicalHistory,
                    //x.GhanaPostGps,
                    x.FatherName,
                    x.FatherPhoneNumber,
                    //x.FatherPostalAddress,
                    //x.FatherResidentialAddress,
                    //x.FatherGhanaPostGps,
                    x.MotherName,
                    x.MotherPhoneNumber,
                    //x.MotherPostalAddress,
                    //x.MotherResidentialAddress,
                    //x.MotherGhanaPostGps,
                    Uniforms = repo.GetUniforms(x.UniformIds),
                    Siblings = x.Siblings.Count(),
                    x.OutpostId,
                    Outpost = x.Outpost.Name,
                    District = x.Outpost.District.Name,
                    Region = x.Outpost.District.Region.Name,
                    Sector = x.Outpost.District.Region.Sector.Name,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    Transfers = repo.GetTransfers(x.Id).Count(),
                    Awards = repo.GetAwards(x.Id).Count(),
                    Rank = x.IsCommander ? "Commander" : repo.GetRank(x.Age)
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Get(long id)
        {
            try
            {
                var repo = new RangerRepository(_context);
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _context.Rangers.Where(x => x.Id == id);
                var raw1 = raw.Include(x => x.Outpost.District.Region.Sector).Include(x => x.Siblings).Include(x => x.Awards).ToList();
                if (!raw1.Any()) throw new Exception("Please check the id");
                var trainingIds = raw1.First().Awards.Where(x => x.Type == RangerAwardType.Training).Select(x => x.TrainingId).ToList();
                var awardsIds = raw1.First().Awards.Where(x => x.Type == RangerAwardType.Award).Select(x => x.AwardId).ToList();
                var achIds = raw1.First().Awards.Where(x => x.Type == RangerAwardType.Achievement).Select(x => x.AchievementId).ToList();
                var advIds = raw1.First().Awards.Where(x => x.Type == RangerAwardType.Advancement).Select(x => x.AdvancementId).ToList();
                var trainings = _context.Trainings.Where(x => trainingIds.Contains(x.Id)).ToList();
                var awards = _context.Awards.Where(x => awardsIds.Contains(x.Id)).ToList();
                var achs = _context.Achievements.Where(x => achIds.Contains(x.Id)).ToList();
                var advs = _context.Advancements.Where(x => advIds.Contains(x.Id)).ToList();
                var res = raw1.Select(x => new
                {
                    x.Id,
                    x.Photo,
                    x.Code,
                    x.Name,
                    x.PhoneNumber,
                    x.Email,
                    x.DateOfBirth,
                    x.Age,
                    x.IsCommander,
                    x.Occupation,
                    Gender = x.Gender.ToString(),
                    x.ResidentialAddress,
                    x.PostalAddress,
                    x.SchoolOrWork,
                    x.Hobbies,
                    x.Allergies,
                    x.MedicalHistory,
                    x.GhanaPostGps,
                    x.FatherName,
                    x.FatherPhoneNumber,
                    x.FatherPostalAddress,
                    x.FatherResidentialAddress,
                    x.FatherGhanaPostGps,
                    x.MotherName,
                    x.MotherPhoneNumber,
                    x.MotherPostalAddress,
                    x.MotherResidentialAddress,
                    x.MotherGhanaPostGps,
                    x.UniformIds,
                    Uniforms = repo.GetUniforms(x.UniformIds),
                    Siblings = x.Siblings.Select(s => new {
                        s.Id,
                        s.Name,
                        s.PhoneNumber,
                        s.DateOfBirth,
                        s.Age
                    }).ToList(),
                    x.OutpostId,
                    Outpost = x.Outpost.Name,
                    District = x.Outpost.District.Name,
                    Region = x.Outpost.District.Region.Name,
                    Sector = x.Outpost.District.Region.Sector.Name,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    Awards = x.Awards.Where(a=> a.Type == RangerAwardType.Award).Select(a => new
                    {
                        a.RangerId,
                        a.Id,
                        a.Date,
                        Type = a.Type.ToString(),
                        a.AwardId,
                        Title = awards.First(t => t.Id == a.AwardId).Name
                    }).ToList(),
                    Trainings = x.Awards.Where(a => a.Type == RangerAwardType.Training).Select(a => new
                    {
                        a.RangerId,
                        a.Id,
                        a.Date,
                        Type = a.Type.ToString(),
                        a.TrainingId,
                        Title = trainings.First(t => t.Id == a.TrainingId).Name
                    }).ToList(),
                    Advancements = x.Awards.Where(a => a.Type == RangerAwardType.Advancement).Select(a => new
                    {
                        a.RangerId,
                        a.Id,
                        a.Date,
                        Type = a.Type.ToString(),
                        a.Advancement,
                        Title = advs.First(t => t.Id == a.AdvancementId).Name
                    }).ToList(),
                    Achievements = x.Awards.Where(a => a.Type == RangerAwardType.Achievement).Select(a => new
                    {
                        a.RangerId,
                        a.Id,
                        a.Date,
                        Type = a.Type.ToString(),
                        a.Achievement,
                        Title = achs.First(t => t.Id == a.AchievementId).Name
                    }).ToList(),
                    Transfers = repo.GetTransfers(x.Id).Select(t => new
                    {
                        t.Id,
                        t.RangerId,
                        t.CreatedAt,
                        t.ModifiedAt,
                        t.CreatedBy,
                        t.ModifiedBy,
                        t.VettedBy,
                        t.VettingNotes,
                        t.VettedOn,
                        Status = t.Status.ToString(),
                        t.SourceId,
                        t.DestinationId,
                        Source = t.Source.Name,
                        Destination = t.Destination.Name
                    }).ToList(),
                    Rank = x.IsCommander ? "Commander" : repo.GetRank(x.Age)
                }).FirstOrDefault();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Post(Ranger model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.Outpost:
                        {
                            model.OutpostId = user.OutpostId.Value;
                            break;
                        }
                }
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;
                var siblings = model.Siblings?? new List<Sibling>();
                model.Siblings = null;
                _context.Rangers.Add(model);

                //add or update siblings
                var exst = _context.Siblings.Where(x => x.RangerId == model.Id);
                _context.RemoveRange(exst);
                foreach(var s in siblings)
                {
                    s.RangerId = model.Id;
                    _context.Siblings.Add(s);
                }
                await _context.SaveChangesAsync();
                return Created("CreateRanger", new { Message = "Saved Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(Ranger model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var siblings = model.Siblings ?? new List<Sibling>();
                model.Siblings = null;

                var rec = _context.Rangers.First(x => x.Id == model.Id);

                rec.ModifiedAt = DateTime.UtcNow;
                rec.ModifiedBy = user.UserName;
                rec.Photo = model.Photo;
                rec.Name = model.Name;
                rec.PhoneNumber = model.PhoneNumber;
                rec.Email = model.Email;
                rec.DateOfBirth = model.DateOfBirth;
                rec.Gender = model.Gender;
                rec.ResidentialAddress = model.ResidentialAddress;
                rec.PostalAddress = model.PostalAddress;
                rec.SchoolOrWork = model.SchoolOrWork;
                rec.Hobbies = model.Hobbies;
                rec.Allergies = model.Allergies;
                rec.MedicalHistory = model.MedicalHistory;
                rec.GhanaPostGps = model.GhanaPostGps;
                rec.FatherGhanaPostGps = model.FatherGhanaPostGps;
                rec.FatherName = model.FatherName;
                rec.FatherPhoneNumber = model.FatherPhoneNumber;
                rec.FatherPostalAddress = model.FatherPostalAddress;
                rec.FatherResidentialAddress = model.FatherResidentialAddress;
                rec.MotherGhanaPostGps = model.MotherGhanaPostGps;
                rec.MotherName = model.MotherName;
                rec.MotherPhoneNumber = model.MotherPhoneNumber;
                rec.MotherPostalAddress = model.MotherPostalAddress;
                rec.MotherResidentialAddress = model.MotherResidentialAddress;
                rec.UniformIds = model.UniformIds;
                rec.IsCommander = model.IsCommander;
                rec.Occupation = model.Occupation;

                //add or update siblings
                var exst = _context.Siblings.Where(x => x.RangerId == model.Id);
                _context.RemoveRange(exst);
                foreach (var s in siblings)
                {
                    s.RangerId = model.Id;
                    _context.Siblings.Add(s);
                }
                await _context.SaveChangesAsync();
                return Created("UpdateProgramme", new { Message = "Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("addaward")]
        public async Task<ActionResult> AddAward(RangerAwardDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                
                var obj = new RangerAward
                {
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    CreatedBy = user.UserName,
                    ModifiedBy = user.UserName,
                    Type = model.Type,
                    Date = model.Date,
                    RangerId = model.RangerId
                };
                var exst = new RangerAward();
                switch (model.Type)
                {
                    case RangerAwardType.Award:
                        //check award
                        exst = _context.RangerAwards.FirstOrDefault(x => x.RangerId == model.RangerId && (x.AwardId == model.AwardId));
                        if (exst != null) throw new Exception($"This ranger has already had this {model.Type.ToString()}");
                        obj.AwardId = model.AwardId;
                        break;
                    case RangerAwardType.Achievement:
                        //check award
                        exst = _context.RangerAwards.FirstOrDefault(x => x.RangerId == model.RangerId && (x.TrainingId == model.TrainingId));
                        if (exst != null) throw new Exception($"This ranger has already had this {model.Type.ToString()}");
                        obj.AchievementId = model.AchievementId;
                        break;
                    case RangerAwardType.Advancement:
                        //check award
                        exst = _context.RangerAwards.FirstOrDefault(x => x.RangerId == model.RangerId && (x.AdvancementId == model.AdvancementId));
                        if (exst != null) throw new Exception($"This ranger has already had this {model.Type.ToString()}");
                        obj.AdvancementId = model.AdvancementId;
                        break;
                    case RangerAwardType.Training:
                        //check award
                        exst = _context.RangerAwards.FirstOrDefault(x => x.RangerId == model.RangerId && (x.AchievementId == model.AchievementId));
                        if (exst != null) throw new Exception($"This ranger has already had this {model.Type.ToString()}");
                        obj.TrainingId = model.TrainingId;
                        break;
                    default:
                        throw new Exception("Please select a type");
                }
                db.RangerAwards.Add(obj);
                db.SaveChanges();
                return Created("AddAward", new { Message = "Added Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("removeaward")]
        public async Task<ActionResult> RemoveAward(long id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var award = db.RangerAwards.FirstOrDefault(x => x.Id == id);
                db.RangerAwards.Remove(award);
                db.SaveChanges();
                return Ok(new { Message = "Removed Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("transfer")]
        public ActionResult Transfer(RangerTransferDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                switch (user.Type)
                {
                    case UserType.Outpost:
                        {
                            model.SourceId = user.OutpostId.Value;
                            break;
                        }
                }

                var obj = new RangerTransfer
                {
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    CreatedBy = user.UserName,
                    ModifiedBy = user.UserName,
                    DestinationId = model.DestinationId,
                    SourceId = model.SourceId,
                    RangerId = model.RangerId,
                    Status = RangerTransferStatus.Pending
                };
                db.RangerTransfers.Add(obj);
                db.SaveChanges();
                //todo: send a notification or prompt to superior users
                return Created("TransferRanger", new { Message = "Transfer has been Initiated" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("canceltransfer")]
        public ActionResult CancelTransfer(RangerTransferVettingDTO model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Notes)) throw new Exception("Please provide a cancellation note.");
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var rec = _context.RangerTransfers.FirstOrDefault(x => x.Id == model.Id);
                if (rec == null) throw new Exception("Transfer cannot be found");
                if (rec.Status != RangerTransferStatus.Pending || rec.Status != RangerTransferStatus.Approved) throw new Exception($"This transfer cannot be cancelled. Transfer Status: {rec.Status.ToString()}");
                rec.Status = RangerTransferStatus.Cancelled;
                rec.VettingNotes = model.Notes;
                rec.VettedOn = DateTime.UtcNow;
                rec.VettedBy = user.UserName;
                db.SaveChanges();
                return Created("CancelTransfer", new { Message = "Transfer has been Cancelled" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("rejecttransfer")]
        public ActionResult RejectTransfer(RangerTransferVettingDTO model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Notes)) throw new Exception("Please provide a rejection note.");
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var rec = _context.RangerTransfers.FirstOrDefault(x => x.Id == model.Id);
                if (rec == null) throw new Exception("Transfer cannot be found");
                if (rec.Status != RangerTransferStatus.Pending) throw new Exception($"This transfer cannot be rejected. Transfer Status: {rec.Status.ToString()}");
                rec.Status = RangerTransferStatus.Rejected;
                rec.VettingNotes = model.Notes;
                rec.VettedOn = DateTime.UtcNow;
                rec.VettedBy = user.UserName;
                db.SaveChanges();
                return Created("RejectedTransfer", new { Message = "Transfer has been Rejected" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("approvetransfer")]
        public ActionResult ApproveTransfer(RangerTransferVettingDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var rec = _context.RangerTransfers.FirstOrDefault(x => x.Id == model.Id);
                if (rec == null) throw new Exception("Transfer cannot be found");
                if (rec.Status != RangerTransferStatus.Pending) throw new Exception($"This transfer cannot be approved. Transfer Status: {rec.Status.ToString()}");
                rec.Status = RangerTransferStatus.Approved;
                rec.VettingNotes = model.Notes;
                rec.VettedOn = DateTime.UtcNow;
                rec.VettedBy = user.UserName;
                db.SaveChanges();
                return Created("ApprovedTransfer", new { Message = "Transfer has been Approved" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("accepttransfer")]
        public ActionResult AcceptTransfer(RangerTransferVettingDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var rec = _context.RangerTransfers.FirstOrDefault(x => x.Id == model.Id);
                if (rec == null) throw new Exception("Transfer cannot be found");
                if (rec.Status != RangerTransferStatus.Approved) throw new Exception($"This transfer cannot be accepted. Transfer Status: {rec.Status.ToString()}");
                rec.Status = RangerTransferStatus.Accepted;
                rec.VettingNotes = model.Notes;
                rec.VettedOn = DateTime.UtcNow;
                rec.VettedBy = user.UserName;

                //update the ranger's outpost
                var ranger = _context.Rangers.First(x => x.Id == rec.RangerId);
                ranger.OutpostId = rec.DestinationId;

                db.SaveChanges();
                return Created("AcceptedTransfer", new { Message = "Transfer has been Accepted" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getunconfirmed")]
        public ActionResult GetUnconfirmed(long campId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var activeCamp = _context.Camps.FirstOrDefault(x=> x.Id == campId);
                if (activeCamp == null) throw new Exception($"Please check the selected camp.");
                var cnfmdIds =
                    _context.CampMembers.Where(
                        x =>
                            x.CampId == activeCamp.Id && x.Ranger.OutpostId == user.OutpostId &&
                            x.Status != CampMemberStatus.Cancelled).Select(x => x.RangerId);
                if (user.OutpostId == null || user.OutpostId <= 0) throw new Exception("You are not allowed to take this action.");
                var data =
                    _context.Rangers
                        .Where(x => x.OutpostId == user.OutpostId && !cnfmdIds.Contains(x.Id))
                        .ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Code,
                                    x.Name,
                                    x.PhoneNumber,
                                    x.Email,
                                    x.DateOfBirth,
                                    x.Age,
                                    x.IsCommander,
                                    x.Occupation,
                                    Gender = x.Gender.ToString(),
                                    x.FatherName,
                                    x.FatherPhoneNumber,
                                    x.MotherName,
                                    x.MotherPhoneNumber,
                                    x.OutpostId,
                                    Outpost = x.Outpost.Name
                                })
                        .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

    }
}
