﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class OutpostBalanceController : BaseController<OutpostBalance>
    {
        public OutpostBalanceController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            
                return BadRequest("Not Implemented");
        }

        public override async Task<ActionResult> Get(long id)
        {

            return BadRequest("Not Implemented");
        }

        public override async Task<ActionResult> Delete(long id)
        {

            return BadRequest("Not Implemented");
        }

        public override async Task<ActionResult> Post(OutpostBalance model)
        {

            return BadRequest("Not Implemented");
        }

        public override async Task<ActionResult> Put(OutpostBalance model)
        {

            return BadRequest("Not Implemented");
        }

        [HttpPost]
        [Route("admin/manualtopup")]
        public async Task<ActionResult> ManualTopup(ManualTopupDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if(user.Type == UserType.Outpost) throw new Exception("You are not authorized to do manual topups");

                var bal = db.OutpostBalances.Where(x => x.OutpostId == model.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check the selected outpost");

                db.ManualTopupTransactions.Add(new ManualTopupTransaction 
                {
                    OutpostBalanceId = bal.Id,
                    Amount = model.Amount,
                    Status = TopupTransactionStatus.Processed,
                    Notes = $"Manual Topup Transaction::  {model.Notes}",
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    ModifiedBy = user.UserName,
                    CreatedBy = user.UserName
                });

                bal.Amount = bal.Amount + model.Amount;
                db.SaveChanges();
                return Created("ManualTopup", new { Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("momotopup")]
        public async Task<ActionResult> MomoTopup(TopupTransactionDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized to do momo topups");

                var bal = db.OutpostBalances.Where(x => x.OutpostId == user.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check your account");

                db.TopupTransactions.Add(new TopupTransaction
                {
                    OutpostBalanceId = bal.Id,
                    Amount = model.Amount,
                    Status = TopupTransactionStatus.Pending,
                    Response = $"Mobile Money Topup Transaction:: ",
                    UserId = user.Id,
                    Provider = model.Provider,
                    Name = model.Name,
                    PhoneNumber = model.PhoneNumber,
                    Token = model.Token,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    ModifiedBy = user.UserName,
                    CreatedBy = user.UserName
                });
                db.SaveChanges();
                return Created("MomoTopup", new { Message = "Please authorize the transaction on your phone" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("admin/refund")]
        public async Task<ActionResult> Refund(RefundModel model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized to do refunds");

                var bal = db.OutpostBalances.Where(x => x.OutpostId == model.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check the selected outpost");
                if(model.Amount  > bal.Amount) throw new Exception("Refund is not possible. The amount is higher than the available balance.");

                db.BalanceDebits.Add(new BalanceDebit
                {
                    OutpostBalanceId = bal.Id,
                    Amount = model.Amount,
                    Date = model.Date,
                    Type = BalanceDebitType.Refund,
                    Notes = $"Refund Transaction::  {model.Notes}",
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    ModifiedBy = user.UserName,
                    CreatedBy = user.UserName
                });

                bal.Amount = bal.Amount - model.Amount;
                db.SaveChanges();
                return Created("Refund", new { Message = "Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("viewbalance")]
        public async Task<ActionResult> ViewBalance()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized to view balance");

                var bal = db.OutpostBalances.Where(x => x.OutpostId == user.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check your account");
                return Ok(bal.Amount);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("admin/viewbalances")]
        public async Task<ActionResult> AdminViewBalances(OutpostBalancesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized to view balances");

                var raw = filter.BuildQuery(_context.OutpostBalances);
                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            raw = raw.Where(x => x.Outpost.District.Region.SectorId == user.SectorId);
                            break;
                        }

                    case UserType.Regional:
                        {
                            raw = raw.Where(x => x.Outpost.District.RegionId == user.RegionId);
                            break;
                        }

                    case UserType.District:
                        {
                            raw = raw.Where(x => x.Outpost.DistrictId == user.DistrictId);
                            break;
                        }
                }
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Outpost.District.Region.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Outpost Balance Found" });

                var data = raw.Select(x=> new 
                    { 
                        x.Amount,
                    Outpost = x.Outpost.Name,
                    District = x.Outpost.District.Name,
                    Region = x.Outpost.District.Region.Name,
                    Sector = x.Outpost.District.Region.Sector.Name,
                    }).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("admin/viewstatement")]
        public async Task<ActionResult> AdminViewStatement(OutpostBalanceStatementsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized to view this statement");

                if (filter.OutpostId == 0) throw new Exception("Please select an outpost");

                var bal = db.OutpostBalances.Where(x => x.OutpostId == filter.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check the selected outpost");

                var raw = new List<BalanceStatement>();
                var mantopups = db.ManualTopupTransactions.Where(x => x.OutpostBalanceId == bal.Id && x.Status == TopupTransactionStatus.Processed).Select(x=> new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Credit",
                    Amount = x.Amount
                }).ToList();
                raw.AddRange(mantopups);
                var momotopups = db.TopupTransactions.Where(x => x.OutpostBalanceId == bal.Id && x.Status == TopupTransactionStatus.Processed).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = "Momo Topup",
                    Type = "Credit",
                    Amount = x.Amount
                }).ToList();
                raw.AddRange(momotopups);
                var debits = db.BalanceDebits.Where(x => x.OutpostBalanceId == bal.Id).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Debit",
                    Amount = x.Amount
                }).ToList();
                raw.AddRange(debits);

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x=> x.Date).Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "Data Loaded" });
                return Ok(raw);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("viewstatement")]
        public async Task<ActionResult> ViewStatement(OutpostBalanceStatementsFilterx filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized to view this statement");

                var bal = db.OutpostBalances.Where(x => x.OutpostId == user.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check your account");

                var raw = new List<BalanceStatement>();
                var mantopups = db.ManualTopupTransactions.Where(x => x.OutpostBalanceId == bal.Id && x.Status == TopupTransactionStatus.Processed).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Credit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(mantopups);
                var momotopups = db.TopupTransactions.Where(x => x.OutpostBalanceId == bal.Id && x.Status == TopupTransactionStatus.Processed).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = "Momo Topup",
                    Type = "Credit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(momotopups);
                var debits = db.BalanceDebits.Where(x => x.OutpostBalanceId == bal.Id).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Debit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(debits);

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.Date).Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "Data Loaded" });
                return Ok(raw);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("momotransactions")]
        public async Task<ActionResult> MomoTransactions(OutpostBalanceStatementsFilterx filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized to view this statement");

                var raw = db.TopupTransactions.Include(x=> x.OutpostBalance.Outpost).Where(x => x.OutpostBalance.OutpostId == user.OutpostId);

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.CreatedAt).Take(filter.Pager.Size * filter.Pager.Page);
                var data = raw.ToList().Select(x => new
                {
                    x.Reference,
                    x.Amount,
                    x.Fee,
                    x.UserId,
                    Provider = x.Provider.ToString(),
                    x.Name,
                    x.PhoneNumber,
                    Status = x.Status.ToString(),
                    x.CreatedAt
                }).ToList();
                if (!data.Any()) return Ok(new { Data = new List<object>(), Message = "Data Loaded" });
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("manualtransactions")]
        public async Task<ActionResult> ManualTransactions(OutpostBalanceStatementsFilterx filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized to view this statement");

                var raw = db.ManualTopupTransactions.Include(x => x.OutpostBalance.Outpost).Where(x => x.OutpostBalance.OutpostId == user.OutpostId);

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.CreatedAt).Take(filter.Pager.Size * filter.Pager.Page);
                var data = raw.ToList().Select(x=> new
                { 
                    x.Reference,
                    x.Amount,
                    x.Fee,
                    x.Notes,
                    Status = x.Status.ToString()
                }).ToList();
                if (!data.Any()) return Ok(new { Data = new List<object>(), Message = "Data Loaded" });
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("admin/momotransactions")]
        public async Task<ActionResult> AdminMomoTransactions(OutpostBalanceStatementsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                if (filter.OutpostId == 0) throw new Exception("Please select an outpost");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized to view this statement");

                var raw = db.TopupTransactions.Include(x => x.OutpostBalance.Outpost).Where(x => x.OutpostBalance.OutpostId == filter.OutpostId);

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.CreatedAt).Take(filter.Pager.Size * filter.Pager.Page);
                var data = raw.ToList().Select(x => new
                {
                    x.Reference,
                    x.Amount,
                    x.Fee,
                    x.UserId,
                    Provider = x.Provider.ToString(),
                    x.Name,
                    x.PhoneNumber,
                    Status = x.Status.ToString()
                }).ToList();
                if (!data.Any()) return Ok(new { Data = new List<object>(), Message = "Data Loaded" });
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("admin/manualtransactions")]
        public async Task<ActionResult> AdminManualTransactions(OutpostBalanceStatementsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                if (filter.OutpostId == 0) throw new Exception("Please select an outpost");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized to view this statement");

                var raw = db.ManualTopupTransactions.Include(x => x.OutpostBalance.Outpost).Where(x => x.OutpostBalance.OutpostId == filter.OutpostId);

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.CreatedAt).Take(filter.Pager.Size * filter.Pager.Page);
                var data = raw.ToList().Select(x => new
                {
                    x.Reference,
                    x.Amount,
                    x.Fee,
                    x.Notes,
                    Status = x.Status.ToString()
                }).ToList();
                if (!data.Any()) return Ok(new { Data = new List<object>(), Message = "Data Loaded" });
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("topuptransactions")]
        public async Task<ActionResult> TopupTransactions(OutpostBalanceStatementsFilterx filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                //todo: change this to use a privilege
                if (user.Type != UserType.Outpost) throw new Exception("You are not authorized to view these transactions");

                var bal = db.OutpostBalances.Where(x => x.OutpostId == user.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check your account");

                var raw = new List<BalanceStatement>();
                var mantopups = db.ManualTopupTransactions.Where(x => x.OutpostBalanceId == bal.Id).Select(x => new BalanceStatement
                {
                    Date = x.CreatedAt,
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Credit",
                    Amount = x.Amount
                }).ToList();
                raw.AddRange(mantopups);
                var momotopups = db.TopupTransactions.Where(x => x.OutpostBalanceId == bal.Id).Select(x => new BalanceStatement
                {
                    Date = x.CreatedAt,
                    Reference = x.Reference,
                    Notes = "Momo Topup",
                    Type = "Credit",
                    Amount = x.Amount
                }).ToList();
                raw.AddRange(momotopups);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.Date).Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "Data Loaded" });
                return Ok(raw);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("admin/topuptransactions")]
        public async Task<ActionResult> AdminTopupTransactions(OutpostBalanceStatementsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                if (filter.OutpostId == 0) throw new Exception("Please select an outpost");

                //todo: change this to use a privilege
                if (user.Type == UserType.Outpost) throw new Exception("You are not authorized to view these transactions");

                //var bal = db.OutpostBalances.Where(x => x.OutpostId == filter.OutpostId).FirstOrDefault();
                //if (bal == null) throw new Exception("Please check your account");

                var raw = new List<BalanceStatement>();
                var mantopups = db.ManualTopupTransactions.Include(x=> x.OutpostBalance.Outpost.District.Region).Where(x => x.OutpostBalanceId >0).Select(x => new BalanceStatement
                {
                    Date = x.CreatedAt,
                    CreatedBy = x.CreatedBy,
                    Outpost = x.OutpostBalance.Outpost.Name,
                    Location = $"{x.OutpostBalance.Outpost.District.Name} - {x.OutpostBalance.Outpost.District.Region.Name}", 
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Credit",
                    Amount = x.Amount,
                    Status = x.Status.ToString()
                }).ToList();
                raw.AddRange(mantopups);
                var momotopups = db.TopupTransactions.Include(x => x.OutpostBalance.Outpost.District.Region).Where(x => x.OutpostBalanceId > 0 ).Select(x => new BalanceStatement
                {
                    Date = x.CreatedAt,
                    CreatedBy = x.CreatedBy,
                    Outpost = x.OutpostBalance.Outpost.Name,
                    Location = $"{x.OutpostBalance.Outpost.District.Name} - {x.OutpostBalance.Outpost.District.Region.Name}",
                    Reference = x.Reference,
                    Notes = "Momo Topup",
                    Type = "Credit",
                    Amount = x.Amount,
                    Status = x.Status.ToString()
                }).ToList();
                raw.AddRange(momotopups);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.OrderByDescending(x => x.Date).Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "Data Loaded" });
                return Ok(raw);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
