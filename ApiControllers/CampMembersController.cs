﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using RoyalRangersAPI.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class CampMembersController : BaseController<CampMember>
    {
        public CampMembersController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            return BadRequest("Not Implemented");
        }

        public override async Task<ActionResult> Get(long id)
        {
            return BadRequest("Not Implemented");
        }

        public override async Task<ActionResult> Post(CampMember model)
        {
            return BadRequest("Not Implemented");
        }

        public override async Task<ActionResult> Put(CampMember model)
        {
            return BadRequest("Not Implemented");
        }
        public override async Task<ActionResult> Delete(long id)
        {
            return BadRequest("Not Implemented");
        }

        [HttpPost]
        [Route("confirmmember")]
        public ActionResult ConfirmMember(CampMemberModel record)
        {
            using (var db = _context)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var date = DateTime.Now;
                        var uId = User.FindFirst("Id")?.Value;
                        var user = db.Users.FirstOrDefault(x => x.Id == uId);
                        if (user == null) throw new Exception("Could not find user");
                        if (user.OutpostId == null || user.OutpostId <= 0)
                            throw new Exception("You are not allowed to make this action.");
                        var campRepo = new CampRepository(db);
                        //getactive camp
                        var camp = campRepo.GetCamp(record.CampId);
                        if (camp == null) throw new Exception("This action is not possible because there is no active camp at the moment.");
                        if (record.AmountPaid < camp.MinRegAmount) throw new Exception("The amount paid should not be less than the agreed minimum registration amount");
                        var ranger = db.Rangers.First(x => x.Id == record.RangerId);
                        if (!camp.IsActive) throw new Exception("The camp is not an active camp.");
                        var dt = DateTime.Now.Date;
                        if (camp.EndDate.Date < dt) throw new Exception("The camp has already ended.");

                        //Check if ranger is present
                        var presnt =
                            db.CampMembers.FirstOrDefault(
                                x =>
                                    x.RangerId == record.RangerId && x.CampId == camp.Id &&
                                    x.Status != CampMemberStatus.Cancelled);
                        if (presnt != null) throw new Exception("The selected ranger is already confirmed.");

                        //get amount payable
                        var pricing = campRepo.GetPricingSchedule(date, camp.Id);
                        if (pricing == null) throw new Exception("There is no pricing schedule set for the active camp");
                        if (record.AmountPaid > pricing.AmountPayable) throw new Exception("The amount entered cannot be more than the amount payable. Please check the amount.");
                        var discount = camp.Fee - pricing.AmountPayable;
                        if (discount < 0) discount = 0;
                        var balance = pricing.AmountPayable - record.AmountPaid;

                        //get rank
                        var rank = campRepo.GetRank(ranger.Age);
                        if (record.IsCommander) rank = db.Ranks.FirstOrDefault(x => x.Name.ToLower().Contains("commander"));
                        if (rank == null) throw new Exception("There is no rank setup for this rangers group.");

                        //reduce balance
                        var outpostBal = db.OutpostBalances.First(x => x.OutpostId == user.OutpostId);
                        if (outpostBal.Amount < record.AmountPaid) throw new Exception("You do not have enough credit balance to make this confirmation");
                        outpostBal.Amount = outpostBal.Amount - record.AmountPaid;

                        db.BalanceDebits.Add(new BalanceDebit
                        {
                            OutpostBalanceId = outpostBal.Id,
                            Amount = record.AmountPaid,
                            Date = DateTime.Now,
                            Type = BalanceDebitType.Refund,
                            Notes = $"Camp Confirmation for {ranger.Name}. Amount Paid: GHS{record.AmountPaid}",
                            CreatedAt = DateTime.UtcNow,
                            ModifiedAt = DateTime.UtcNow,
                            ModifiedBy = user.UserName,
                            CreatedBy = user.UserName
                        });
                        db.SaveChanges();

                        //camp member
                        var campMember = new CampMember
                        {
                            CreatedAt = DateTime.Now,
                            ModifiedAt = DateTime.Now,
                            CreatedBy = user.UserName,
                            ModifiedBy = user.UserName,
                            AmountPaid = record.AmountPaid,
                            RangerId = record.RangerId,
                            CampId = camp.Id,
                            Rank = rank.Name,
                            Discount = discount,
                            Balance = balance,
                            Status = balance > 0 ? CampMemberStatus.PartPayment : CampMemberStatus.FullPayment
                        };
                        db.CampMembers.Add(campMember);
                        db.SaveChanges();

                        //record payment
                        db.Payments.Add(new CampMemberPayment
                        {
                            CampMemberId = campMember.Id,
                            Amount = campMember.AmountPaid,
                            Date = DateTime.Now,
                            Balance = balance,
                            CreatedAt = DateTime.Now,
                            ModifiedAt = DateTime.Now,
                            CreatedBy = "System",
                            ModifiedBy = "System",
                        });
                        db.SaveChanges();

                        //Add to camp class
                        if(camp.HasClasses) 
                            campRepo.AddToCampClass(campMember.Id, rank, camp);

                        //Add to camp table
                        if (camp.HasTables)
                            campRepo.AddToCampTable(campMember.Id, ranger.Age, camp);

                        //Add to camp room
                        if (camp.HasRooms)
                            campRepo.AddToCampRoom(campMember.Id, camp);

                        transaction.Commit();
                        return Created("Confirm Camp Member", new { Message = "Confirmed Successfully" });
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return BadRequest(WebHelpers.ProcessException(ex));
                    }
                }
            }
        }

        [HttpPost]
        [Route("updatepayment")]
        public ActionResult UpdatePayment(UpdatePaymentModel record)
        {
            using (var db = _context)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var uId = User.FindFirst("Id")?.Value;
                        var user = db.Users.FirstOrDefault(x => x.Id == uId);
                        if (user == null) throw new Exception("Could not find user");
                        if (user.OutpostId == null || user.OutpostId <= 0)
                            throw new Exception("You are not allowed to make this action.");
                        //check if the person is not done paying
                        var full =
                            db.CampMembers.FirstOrDefault(
                                x => x.Id == record.CampMemberId && x.Status == CampMemberStatus.FullPayment);
                        if (full != null) throw new Exception("The selected ranger has made full payment.");
                        //get campMember
                        var member = db.CampMembers.Where(x => x.Id == record.CampMemberId).Include(x => x.Ranger).First();

                        //check if the camp is still active
                        var camp = db.Camps.First(x => x.Id == member.CampId);
                        if (!camp.IsActive) throw new Exception("The camp is not an active camp.");
                        var dt = DateTime.Now.Date;
                        if (camp.EndDate.Date < dt) throw new Exception("The camp has already ended.");

                        if (record.Amount > member.Balance) throw new Exception($"Please check the amount. It cannot be more than the outstanding balance. Balance: {member.Balance}");
                        member.AmountPaid = member.AmountPaid + record.Amount;
                        member.Balance = member.Balance - record.Amount;
                        if (member.Balance <= 0) member.Status = CampMemberStatus.FullPayment;
                        db.SaveChanges();

                        var outpostBal = db.OutpostBalances.First(x => x.OutpostId == user.OutpostId);
                        if (outpostBal.Amount < record.Amount) throw new Exception("You do not have enough credit balance to make this confirmation");
                        outpostBal.Amount = outpostBal.Amount - record.Amount;

                        db.BalanceDebits.Add(new BalanceDebit
                        {
                            OutpostBalanceId = outpostBal.Id,
                            Amount = record.Amount,
                            Date = DateTime.Now,
                            Type = BalanceDebitType.Refund,
                            Notes = $"Camp Fee Payment for {member.Ranger.Name}. Amount Paid: GHS{record.Amount}",
                            CreatedAt = DateTime.UtcNow,
                            ModifiedAt = DateTime.UtcNow,
                            ModifiedBy = user.UserName,
                            CreatedBy = user.UserName
                        });
                        db.SaveChanges();

                        //record payment
                        db.Payments.Add(new CampMemberPayment
                        {
                            CampMemberId = member.Id,
                            Amount = record.Amount,
                            Date = DateTime.Now,
                            Balance = member.Balance,
                            CreatedAt = DateTime.Now,
                            ModifiedAt = DateTime.Now,
                            CreatedBy = "System",
                            ModifiedBy = "System"
                        });
                        db.SaveChanges();

                        transaction.Commit();
                        return Ok(new { Message = "Payment Updated Successfully" });
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return BadRequest(WebHelpers.ProcessException(ex));
                    }
                }
            }
        }

        [HttpGet]
        [Route("cancelconfirmation")]
        public ActionResult CancelConfirmation(long campMemberId)
        {
            using (var db = _context)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var uId = User.FindFirst("Id")?.Value;
                        var user = db.Users.FirstOrDefault(x => x.Id == uId);
                        if (user == null) throw new Exception("Could not find user");
                        if (user.OutpostId == null || user.OutpostId <= 0)
                            throw new Exception("You are not allowed to make this action.");
                        var campRepo = new CampRepository(_context);
                        //get campMember
                        var member = db.CampMembers.First(x => x.Id == campMemberId);
                        member.Status = CampMemberStatus.Cancelled;

                        //check if the camp is still active
                        var camp = db.Camps.First(x => x.Id == member.CampId);
                        if (!camp.IsActive) throw new Exception("The camp is not an active camp.");
                        var dt = DateTime.Now.Date;
                        if (camp.EndDate.Date < dt) throw new Exception("The camp has already ended.");

                        //increase balance
                        var outpostBal = db.OutpostBalances.First(x => x.OutpostId == user.OutpostId);
                        outpostBal.Amount = outpostBal.Amount + member.AmountPaid;

                        db.BalanceDebits.Add(new BalanceDebit
                        {
                            OutpostBalanceId = outpostBal.Id,
                            Amount = (member.AmountPaid * -1),
                            Date = DateTime.Now,
                            Type = BalanceDebitType.Refund,
                            Notes = $"Camp Confirmation Cancellation for {member.Ranger.Name}. Amount Paid: GHS{member.AmountPaid}",
                            CreatedAt = DateTime.UtcNow,
                            ModifiedAt = DateTime.UtcNow,
                            ModifiedBy = user.UserName,
                            CreatedBy = user.UserName
                        });
                        db.SaveChanges();

                        //reverses payment
                        db.Payments.Add(new CampMemberPayment
                        {
                            CampMemberId = member.Id,
                            Amount = member.AmountPaid * -1,
                            Date = DateTime.Now,
                            Balance = 0,
                            CreatedAt = DateTime.Now,
                            ModifiedAt = DateTime.Now,
                            CreatedBy = "System",
                            ModifiedBy = "System",
                        });
                        db.SaveChanges();

                        //remove from table
                        if (camp.HasTables)
                            campRepo.RemoveFromCampTable(campMemberId,member.CampId);
                        //remove from class
                        if (camp.HasClasses)
                            campRepo.RemoveFromCampClass(campMemberId, member.CampId);
                        //remove from room
                        if (camp.HasRooms)
                            campRepo.RemoveFromCampRoom(campMemberId, member.CampId);
                        transaction.Commit();
                        return Created("Cancel Camp Member", new { Message = "Cancel Successfully" });
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return BadRequest(WebHelpers.ProcessException(ex));
                    }
                }
            }
        }

        [HttpPost]
        [Route("replacecampmember")]
        public ActionResult ReplaceCampMember(CampMemberReplacementModel record)
        {
            using (var db = _context)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var uId = User.FindFirst("Id")?.Value;
                        var user = db.Users.FirstOrDefault(x => x.Id == uId);
                        if (user == null) throw new Exception("Could not find user");
                        if (user.OutpostId == null || user.OutpostId <= 0)
                            throw new Exception("You are not allowed to make this action.");

                        //check if the person is a camp member
                        var campMember =
                            db.CampMembers.FirstOrDefault(
                                x => x.Id == record.CampMemberId);
                        if (campMember == null) throw new Exception("Please check the selected camp member");

                        //check if the camp is still active
                        var camp = db.Camps.First(x => x.Id == campMember.CampId);
                        if (!camp.IsActive) throw new Exception("The camp is not an active camp.");
                        var dt = DateTime.Now.Date;
                        if(camp.EndDate.Date < dt) throw new Exception("The camp has already ended.");

                        var ranger = db.Rangers.FirstOrDefault(x => x.Id == record.RangerId);
                        if (ranger == null) throw new Exception("Please check the selected ranger");

                        var rank = new CampRepository(db).GetRank(ranger.Age);
                        if (record.IsCommander) rank = db.Ranks.FirstOrDefault(x => x.Name.ToLower().Contains("commander"));
                        if (rank == null) throw new Exception("There is no rank setup for this rangers group.");

                        campMember.RangerId = ranger.Id;
                        campMember.Rank = rank.Name;
                        campMember.ModifiedAt = DateTime.Now;
                        campMember.ModifiedBy = user.UserName;
                        db.SaveChanges();

                        transaction.Commit();
                        return Ok(new { Message = "Replacement Successful" });
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return BadRequest(WebHelpers.ProcessException(ex));
                    }
                }
            }
        }

        [HttpPost]
        [Route("admin/updatecampmemberrank")]
        public ActionResult UpdateCampMemberRank(CampMemberRankModel record)
        {
            using (var db = _context)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Check if ranger is present
                        var member =
                            db.CampMembers.FirstOrDefault(
                                x =>
                                    x.Id == record.CampMemberId);
                        if (member == null) throw new Exception("There is no camp member with the selected Id");
                        var rank = db.Ranks.FirstOrDefault(x => x.Id == record.RankId);
                        if (rank == null) throw new Exception("There is no rank with the selected Id");

                        member.Rank = rank.Name;
                        db.SaveChanges();

                        transaction.Commit();
                        return Ok(new { Message = "Rank has been updated successfully" });
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return BadRequest(WebHelpers.ProcessException(ex));
                    }
                }
            }
        }

        [HttpGet]
        [Route("getbycamp")]
        public ActionResult GetbyCamp(long campId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (user.OutpostId == null || user.OutpostId <= 0) throw new Exception("You are not allowed to take this action.");
                var campRepo = new CampRepository(_context);
                var data =
                    _context.CampMembers
                        .Where(x => x.CampId == campId && x.Ranger.OutpostId == user.OutpostId)
                        .ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Balance,
                                    x.Discount,
                                    x.AmountPaid,
                                    Status = x.Status.ToString(),
                                    x.RangerId,
                                    Ranger = x.Ranger.Name,
                                    x.Rank,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.CampId,
                                    Camp = x.Camp.Name,
                                    OutPost = x.Ranger.Outpost.Name,
                                    District = x.Ranger.Outpost.District.Name,
                                    Class = campRepo.GetCampClass(x.Id)?.Code,
                                    Room = campRepo.GetCampRoom(x.Id)?.Code,
                                    Table = campRepo.GetCampTable(x.Id)?.Code,
                                    Patrol = campRepo.GetCampPatrol(x.Id)?.Code
                                })
                        .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getcampmember")]
        public ActionResult GetCampMember(long campMemberId)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var campRepo = new CampRepository(_context);
                var data =
                    _context.CampMembers
                        .Where(x => x.Id == campMemberId)
                        .ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Balance,
                                    x.Discount,
                                    x.AmountPaid,
                                    Status = x.Status.ToString(),
                                    x.RangerId,
                                    Ranger = x.Ranger.Name,
                                    x.Rank,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.CampId,
                                    Camp = x.Camp.Name,
                                    OutPost = x.Ranger.Outpost.Name,
                                    District = x.Ranger.Outpost.District.Name,
                                    Class = campRepo.GetCampClass(x.Id)?.Code,
                                    Room = campRepo.GetCampRoom(x.Id)?.Code,
                                    Table = campRepo.GetCampTable(x.Id)?.Code,
                                    Patrol = campRepo.GetCampPatrol(x.Id)?.Code
                                })
                        .FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public ActionResult Query(CampMembersFilter filter)
        {
            try
            {
                if (filter.CampId == 0) throw new Exception("Please select a camp");
                var campRepo = new CampRepository(_context);
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (user.OutpostId == null || user.OutpostId <= 0) throw new Exception("You are not allowed to take this action.");
                
                var activeCamp = campRepo.GetCamp(filter.CampId);
                var data = filter.BuildQuery(_context.CampMembers.Where(x => x.CampId == activeCamp.Id)).Where(x => x.Ranger.OutpostId == user.OutpostId).ToList().OrderBy(x => x.Ranger.Name).ToList();
                var total = data.Count();
                if (filter.Pager.Page > 0)
                    data = data.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                var res = data.Select(x => new
                {
                    x.Id,
                    x.Balance,
                    x.Discount,
                    x.AmountPaid,
                    Status = x.Status.ToString(),
                    x.RangerId,
                    Ranger = x.Ranger.Name,
                    Outpost = x.Ranger.Outpost.Name,
                    District = x.Ranger.Outpost.District.Name,
                    x.Rank,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.CampId,
                    Camp = x.Camp.Name,
                    Class = campRepo.GetCampClass(x.Id)?.Code,
                    Room = campRepo.GetCampRoom(x.Id)?.Code,
                    Table = campRepo.GetCampTable(x.Id)?.Code,
                    Patrol = campRepo.GetCampPatrol(x.Id)?.Code
                }).ToList();

                return Ok(new
                {
                    data = res,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("admin/getbycamp")]
        public ActionResult AdminGetbyCamp(AdminCampMembersFilter filter)
        {
            try
            {
                var campRepo = new CampRepository(_context);
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var activeCamp = campRepo.Get(filter.CampId);
                var data = _context.CampMembers.Where(x => x.CampId == activeCamp.Id).Include(x => x.Ranger.Outpost.District.Region).ToList();
                var total = data.Count();
                if (filter.Pager.Page > 0)
                    data = data.OrderBy(x=> x.CreatedAt).Take(filter.Pager.Size * filter.Pager.Page).ToList();
                var res = data.ToList().Select(
                    x =>
                        new
                        {
                            x.Id,
                            x.Balance,
                            x.Discount,
                            x.AmountPaid,
                            Status = x.Status.ToString(),
                            x.RangerId,
                            Ranger = x.Ranger.Name,
                            x.Rank,
                            x.CreatedAt,
                            x.ModifiedAt,
                            x.CreatedBy,
                            x.ModifiedBy,
                            x.CampId,
                            Camp = x.Camp.Name,
                            District = x.Ranger.Outpost.District.Name,
                            x.Ranger.Outpost.DistrictId,
                            Region = x.Ranger.Outpost.District.Region.Name,
                            x.Ranger.Outpost.District.RegionId,
                            Outpost = x.Ranger.Outpost.Name,
                            Class = campRepo.GetCampClass(x.Id)?.Code,
                            Room = campRepo.GetCampRoom(x.Id)?.Code,
                            Table = campRepo.GetCampTable(x.Id)?.Code,
                            Patrol = campRepo.GetCampPatrol(x.Id)?.Code
                        })
                    .ToList();
                return Ok(new
                {
                    data = res,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpGet]
        [Route("admin/getslimbycamp")]
        public ActionResult AdminGetSlimbyCamp(long campId)
        {
            ResultObj results;
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var campRepo = new CampRepository(_context);
                var activeCamp = campRepo.Get(campId);
                var data = _context.CampMembers.Where(x => x.CampId == activeCamp.Id && x.Status != CampMemberStatus.Cancelled).Include(x => x.Ranger.Outpost.District.Region).ToList().Select(
                    x =>
                        new
                        {
                            x.Id,
                            x.RangerId,
                            Ranger = x.Ranger.Name,
                            x.Rank,
                            District = x.Ranger.Outpost.District.Name,
                            x.Ranger.Outpost.DistrictId,
                            Region = x.Ranger.Outpost.District.Region.Name,
                            x.Ranger.Outpost.District.RegionId,
                            OutPost = x.Ranger.Outpost.Name
                        })
                    .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpPost]
        [Route("adminquery")]
        public ActionResult AdminQuery(CampMembersFilter filter)
        {
            try
            {
                if (filter.CampId == 0) throw new Exception("Please select a camp");
                var campRepo = new CampRepository(_context);
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var activeCamp = campRepo.GetCamp(filter.CampId);
                var data = filter.BuildQuery(_context.CampMembers.Where(x => x.CampId == activeCamp.Id)).ToList().OrderBy(x => x.Ranger.Name).ToList();
                var total = data.Count();
                if (filter.Pager.Page > 0)
                    data = data.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                var res = data.Select(x => new
                {
                    x.Id,
                    x.Balance,
                    x.Discount,
                    x.AmountPaid,
                    Status = x.Status.ToString(),
                    x.RangerId,
                    Ranger = x.Ranger.Name,
                    x.Rank,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.CampId,
                    Camp = x.Camp.Name,
                    District = x.Ranger.Outpost.District.Name,
                    x.Ranger.Outpost.DistrictId,
                    Region = x.Ranger.Outpost.District.Region.Name,
                    x.Ranger.Outpost.District.RegionId,
                    Outpost = x.Ranger.Outpost.Name,
                }).ToList();
                return Ok(new
                {
                    data = res,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
