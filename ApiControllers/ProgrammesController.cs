﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class ProgrammesController : BaseController<Programme>
    {
        public ProgrammesController(ApplicationDbContext context) : base(context)
        {
        }

        [HttpPost]
        [Route("querynational")]
        public async Task<ActionResult> QueryNational(ProgrammesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Type == ProgrammeType.National);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Programme Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Title,
                        x.Notes,
                        x.Email,
                        x.File,
                        Type = x.Type.ToString(),
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("querysector")]
        public async Task<ActionResult> QuerySector(ProgrammesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Type == ProgrammeType.Sector && x.SectorId == user.SectorId).Include(x => x.Sector);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Programme Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Title,
                        x.Notes,
                        x.Email,
                        x.File,
                        Type = x.Type.ToString(),
                        x.SectorId,
                        Sector = x.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("queryregional")]
        public async Task<ActionResult> QueryRegional(ProgrammesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Type == ProgrammeType.Regional && x.RegionId == user.RegionId).Include(x => x.Region.Sector);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Region.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Programme Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Title,
                        x.Notes,
                        x.Email,
                        x.File,
                        Type = x.Type.ToString(),
                        x.RegionId,
                        Region = x.Region.Name,
                        Sector = x.Region.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("querydistrict")]
        public async Task<ActionResult> QueryDistrict(ProgrammesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Type == ProgrammeType.District && x.DistrictId == user.DistrictId).Include(x => x.District.Region.Sector);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.District.Region.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Programme Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Title,
                        x.Notes,
                        x.Email,
                        x.File,
                        Type = x.Type.ToString(),
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        Sector = x.District.Region.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("queryoutpost")]
        public async Task<ActionResult> QueryOutpost(ProgrammesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Type == ProgrammeType.Outpost && x.OutpostId == user.OutpostId).Include(x => x.Outpost.District.Region.Sector);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Outpost.District.Region.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Programme Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Title,
                        x.Notes,
                        x.Email,
                        x.File,
                        Type = x.Type.ToString(),
                        x.OutpostId,
                        Outpost = x.Outpost.Name,
                        District = x.Outpost.District.Name,
                        Region = x.Outpost.District.Region.Name,
                        Sector = x.Outpost.District.Region.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getbytype")]
        public async Task<ActionResult> GetByType(ProgrammeType type)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _context.Programmes.Where(x => x.Type == type);
                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            raw = raw.Where(x => x.SectorId == user.SectorId);
                            break;
                        }

                    case UserType.Regional:
                        {
                            raw = raw.Where(x => x.RegionId == user.RegionId);
                            break;
                        }

                    case UserType.District:
                        {
                            raw = raw.Where(x => x.DistrictId == user.DistrictId);
                            break;
                        }
                    case UserType.Outpost:
                        {
                            raw = raw.Where(x => x.OutpostId == user.OutpostId);
                            break;
                        }
                }

                var res = raw.ToList().Select(x => new
                {
                    x.Id,
                    x.StartDate,
                    x.EndDate,
                    x.Title,
                    x.Notes,
                    x.Email,
                    x.File,
                    Type = x.Type.ToString(),
                    x.OutpostId,
                    x.DistrictId,
                    x.RegionId,
                    x.SectorId,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Post(Programme model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.System:
                        model.Type = ProgrammeType.National;
                        break;
                    case UserType.Sector:
                        {
                            model.SectorId = user.SectorId;
                            model.Type = ProgrammeType.Sector;
                            break;
                        }
                    case UserType.Regional:
                        {
                            model.RegionId = user.RegionId;
                            model.Type = ProgrammeType.Regional;
                            break;
                        }
                    case UserType.District:
                        {
                            model.DistrictId = user.DistrictId;
                            model.Type = ProgrammeType.District;
                            break;
                        }
                    case UserType.Outpost:
                        {
                            model.OutpostId = user.OutpostId;
                            model.Type = ProgrammeType.Outpost;
                            break;
                        }
                }
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;

                _repository.Insert(model);
                return Created("CreateProgramme", new { Message = "Saved Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(Programme model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                model.ModifiedAt = DateTime.UtcNow;
                model.ModifiedBy = user.UserName;

                _repository.Update(model);
                return Created("UpdateProgramme", new { Message = "Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

    }
}
