﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class ExecutivesController : BaseController<Executive>
    {
        public ExecutivesController(ApplicationDbContext context) : base(context)
        {
        }        

        [HttpPost]
        [Route("querynational")]
        public async Task<ActionResult> QueryNational(ExecutivesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x=> x.Position.Type == ExecutiveType.National).Include(x => x.Position);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Position);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Executive Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Name,
                        x.PhoneNumber,
                        x.Email,
                        x.Church,
                        x.PositionId,
                        x.Order,
                        Position = x.Position.Name,
                        PositionType = x.Position.Type.ToString(),                        
                        Status = x.Status.ToString(),
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("querysector")]
        public async Task<ActionResult> QuerySector(ExecutivesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Position.Type == ExecutiveType.Sector && x.SectorId == user.SectorId).Include(x=> x.Position).Include(x=> x.Sector);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Position).Include(x => x.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Executive Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Name,
                        x.PhoneNumber,
                        x.Email,
                        x.Church,
                        x.PositionId,
                        x.Order,
                        Position = x.Position.Name,
                        PositionType = x.Position.Type.ToString(),
                        Status = x.Status.ToString(),
                        x.SectorId,
                        Sector = x.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("queryregional")]
        public async Task<ActionResult> QueryRegional(ExecutivesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Position.Type == ExecutiveType.Regional && x.RegionId == user.RegionId).Include(x => x.Position).Include(x => x.Region.Sector);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Position).Include(x => x.Region.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Executive Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Name,
                        x.PhoneNumber,
                        x.Email,
                        x.Church,
                        x.PositionId,
                        x.Order,
                        Position = x.Position.Name,
                        PositionType = x.Position.Type.ToString(),
                        Status = x.Status.ToString(),
                        x.RegionId,
                        Region = x.Region.Name,
                        Sector = x.Region.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("querydistrict")]
        public async Task<ActionResult> QueryDistrict(ExecutivesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Position.Type == ExecutiveType.District && x.DistrictId == user.DistrictId).Include(x => x.Position).Include(x => x.District.Region.Sector);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Position).Include(x => x.District.Region.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Executive Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Name,
                        x.PhoneNumber,
                        x.Email,
                        x.Church,
                        x.PositionId,
                        x.Order,
                        Position = x.Position.Name,
                        PositionType = x.Position.Type.ToString(),
                        Status = x.Status.ToString(),
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        Sector = x.District.Region.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("queryoutpost")]
        public async Task<ActionResult> QueryOutpost(ExecutivesFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _repository.Query(filter).Where(x => x.Position.Type == ExecutiveType.Outpost && x.OutpostId == user.OutpostId).Include(x => x.Position).Include(x => x.Outpost.District.Region.Sector);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page).Include(x => x.Position).Include(x => x.Outpost.District.Region.Sector);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Executive Found" });
                var data = raw.OrderByDescending(x => x.Id)
                    .Select(x => new
                    {
                        x.Id,
                        x.StartDate,
                        x.EndDate,
                        x.Name,
                        x.PhoneNumber,
                        x.Email,
                        x.Church,
                        x.PositionId,
                        x.Order,
                        Position = x.Position.Name,
                        PositionType = x.Position.Type.ToString(),
                        Status = x.Status.ToString(),
                        x.OutpostId,
                        Outpost = x.Outpost.Name,
                        District = x.Outpost.District.Name,
                        Region = x.Outpost.District.Region.Name,
                        Sector = x.Outpost.District.Region.Sector.Name,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getbytype")]
        public async Task<ActionResult> GetByType(ExecutiveType type)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var raw = _context.Executives.Where(x => x.Position.Type == type);
                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            raw = raw.Where(x => x.SectorId == user.SectorId);
                            break;
                        }

                    case UserType.Regional:
                        {
                            raw = raw.Where(x => x.RegionId == user.RegionId);
                            break;
                        }

                    case UserType.District:
                        {
                            raw = raw.Where(x => x.DistrictId == user.DistrictId);
                            break;
                        }
                    case UserType.Outpost:
                        {
                            raw = raw.Where(x => x.OutpostId == user.OutpostId);
                            break;
                        }
                }

                var res = raw.Include(x => x.Position).ToList().Select(x => new
                {
                    x.Id,
                    x.StartDate,
                    x.EndDate,
                    x.Name,
                    x.PhoneNumber,
                    x.Email,
                    x.Church,
                    x.PositionId,
                    x.Order,
                    Position = x.Position.Name,
                    PositionType = x.Position.Type.ToString(),
                    x.OutpostId,
                    x.DistrictId,
                    x.RegionId,
                    x.SectorId,
                    Status = x.Status.ToString(),
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        
        public override async Task<ActionResult> Post(Executive model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            model.SectorId = user.SectorId;
                            break;
                        }

                    case UserType.Regional:
                        {
                            model.RegionId = user.RegionId;
                            break;
                        }

                    case UserType.District:
                        {
                            model.DistrictId = user.DistrictId;
                            break;
                        }
                    case UserType.Outpost:
                        {
                            model.OutpostId = user.OutpostId;
                            break;
                        }
                }
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;

                _repository.Insert(model);
                return Created("CreateExecutive", new { Message = "Saved Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(Executive model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    
                    case UserType.Sector:
                        {
                            model.SectorId = user.SectorId;
                            break;
                        }

                    case UserType.Regional:
                        {
                            model.RegionId = user.RegionId;
                            break;
                        }

                    case UserType.District:
                        {
                            model.DistrictId = user.DistrictId;
                            break;
                        }
                    case UserType.Outpost:
                        {
                            model.OutpostId = user.OutpostId;
                            break;
                        }
                }
                model.ModifiedAt = DateTime.UtcNow;
                model.ModifiedBy = user.UserName;

                _repository.Update(model);
                return Created("UpdateExecutive", new { Message = "Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

    }

}
