﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class OutpostsController : BaseController<Outpost>
    {
        public OutpostsController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.Where(x => x.Id == uId).Include(x => x.District.Region.Sector).Include(x => x.Sector).Include(x => x.Region.Sector).Include(x => x.Outpost.District.Region.Sector).FirstOrDefault();
                if (user == null) throw new Exception("Could not find user");

                var raw = _context.Outposts.Include(x => x.District.Region.Sector).Include(x => x.Materials).ToList();
                if (user.Type == UserType.Outpost)
                {
                    raw = raw.Where(x => x.Id == user.OutpostId).ToList();
                }
                else
                {
                    if (user.Type == UserType.District) raw = raw.Where(x => x.DistrictId == user.DistrictId).ToList();
                    else if (user.Type == UserType.Regional) raw = raw.Where(x => x.District.RegionId == user.RegionId).ToList();
                    else if (user.Type == UserType.Sector) raw = raw.Where(x => x.District.Region.SectorId == user.SectorId).ToList();
                    else raw = raw.Where(x => x.Id > 0).ToList();
                }
                var res = raw.Select(x => new
                {
                    x.Id,
                    x.Code,
                    x.Name,
                    x.Phone,
                    x.Email,
                    x.DistrictId,
                    District = x.District.Name,
                    Region = x.District.Region.Name,
                    Sector = x.District.Region.Sector.Name,                    
                    Status = x.Status.ToString(),
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.ChurchPhoneNumber,
                    x.ChurchAddress,
                    x.StartDate,
                    MaterialIds = x.Materials.Select(m => m.MaterialId).ToList(),
                    Materials = string.Join(",", x.Materials.Select(m => m.Material?.Name).ToList())
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Post(Outpost model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.District:
                        {
                            model.DistrictId = user.DistrictId.Value;
                            break;
                        }
                }
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;
                var materialIds = model.MaterialIds ?? new List<long>();
                model.MaterialIds = null;
                model.Materials = null;
                db.Outposts.Add(model);

                foreach (var matId in materialIds)
                {
                    db.OutpostMaterials.Add(new OutpostMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        OutpostId = model.Id,
                        MaterialId = matId
                    });
                }

                // create the outpost balance
                db.Add(new OutpostBalance { OutpostId = model.Id });
                db.SaveChanges();

                return Created("CreateOutpost", new { Message = "Saved Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(Outpost model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var materialIds = model.MaterialIds ?? new List<long>();
                model.MaterialIds = null;
                model.Materials = null;

                var existing = db.Outposts.First(x => x.Id == model.Id);

                existing.ModifiedAt = DateTime.UtcNow;
                existing.ModifiedBy = user.UserName;
                existing.Code = model.Code;
                existing.Name = model.Name;
                existing.Notes = model.Notes;
                existing.Phone = model.Phone;
                existing.Email = model.Email;
                existing.Status = model.Status;
                existing.StartDate = model.StartDate;
                existing.ChurchAddress = model.ChurchAddress;
                existing.ChurchPhoneNumber = model.ChurchPhoneNumber;

                var mats = db.OutpostMaterials.Where(x => x.OutpostId == model.Id);
                db.OutpostMaterials.RemoveRange(mats);
                foreach (var matId in materialIds)
                {
                    db.OutpostMaterials.Add(new OutpostMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        OutpostId = model.Id,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();
                return Created("UpdateOutpost", new { Message = "Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(OutpostsFilter filter)
        {
            try
            {
                var raw = _repository.Query(filter);
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.Where(x => x.Id == uId).Include(x => x.District.Region.Sector).Include(x => x.Sector).Include(x => x.Region.Sector).Include(x => x.Outpost.District.Region.Sector).FirstOrDefault();
                if (user == null) throw new Exception("Could not find user");
                var res = raw.ToList();
                if (user.Type == UserType.Outpost)
                {
                    res = res.Where(x => x.Id == user.OutpostId).ToList();
                }
                else
                {
                    if (user.Type == UserType.District) res = res.Where(x => x.DistrictId == user.DistrictId).ToList();
                    else if (user.Type == UserType.Regional) res = res.Where(x => x.District.RegionId == user.RegionId).ToList();
                    else if (user.Type == UserType.Sector) res = res.Where(x => x.District.Region.SectorId == user.SectorId).ToList();
                    else res = res.Where(x => x.Id > 0).ToList();
                }

                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Outpost Found" });
                var data = raw.Include(x => x.Materials).OrderByDescending(x => x.Id).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Code,
                        x.Name,
                        x.Phone,
                        x.Email,
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        Sector = x.District.Region.Sector.Name,
                        Status = x.Status.ToString(),
                        x.Notes,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.ChurchPhoneNumber,
                        x.ChurchAddress,
                        x.StartDate,
                        MaterialIds = x.Materials.Select(m => m.MaterialId).ToList(),
                        Materials = string.Join(",", x.Materials.Select(m => m.Material?.Name).ToList())
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("addorupdatematerials")]
        public async Task<ActionResult> AddOrUpdateMaterials(OutpostMaterialDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.Outpost:
                        {
                            model.OutpostId = user.OutpostId.Value;
                            break;
                        }
                }
                //clear existing
                var existing = db.OutpostMaterials.Where(x => x.OutpostId == model.OutpostId);
                db.OutpostMaterials.RemoveRange(existing);
                foreach (var matId in model.MaterialId)
                {
                    db.OutpostMaterials.Add(new OutpostMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        OutpostId = model.OutpostId,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();
                return Created("AddOrUpdateMaterials", new { Message = "Added Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getmaterials")]
        public async Task<ActionResult> GetMaterials(long outpostId = 0)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (outpostId == 0)
                {
                    switch (user.Type)
                    {
                        case UserType.Outpost:
                            {
                                outpostId = user.OutpostId.Value;
                                break;
                            }
                    }
                }
                var data = db.OutpostMaterials.Where(x => x.OutpostId == outpostId).Include(x => x.Material).Include(x => x.Outpost.District.Region).ToList().Select(x => new {
                    x.Id,
                    x.OutpostId,
                    Outpost = x.Outpost.Name,
                    x.MaterialId,
                    x.Material.Name
                }).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }

}
