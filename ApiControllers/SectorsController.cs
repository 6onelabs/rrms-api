﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class SectorsController : BaseController<Sector>
    {
        public SectorsController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var res = _repository.Get().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.Email,
                    x.PhoneNumber,
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("addorupdatematerials")]
        public async Task<ActionResult> AddOrUpdateMaterials(SectorMaterialDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            model.SectorId = user.SectorId.Value;
                            break;
                        }
                }
                //clear existing
                var existing = db.SectorMaterials.Where(x => x.SectorId == model.SectorId);
                db.SectorMaterials.RemoveRange(existing);
                foreach (var matId in model.MaterialId)
                {
                    db.SectorMaterials.Add(new SectorMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        SectorId = model.SectorId,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();
                return Created("AddOrUpdateMaterials", new { Message = "Added Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getmaterials")]
        public async Task<ActionResult> GetMaterials(long sectorId = 0)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (sectorId == 0)
                {
                    switch (user.Type)
                    {
                        case UserType.Sector:
                            {
                                sectorId = user.SectorId.Value;
                                break;
                            }
                    }
                }
                var data = db.SectorMaterials.Where(x => x.SectorId == sectorId).Include(x => x.Material).Include(x => x.Sector).ToList().Select(x => new {
                    x.Id,
                    x.SectorId,
                    Sector = x.Sector.Name,
                    x.MaterialId,
                    x.Material.Name
                }).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }

}
