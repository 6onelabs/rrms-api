﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using RoyalRangersAPI.Repositories;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<User> signInManager;
        private readonly IConfiguration configuration;

        public AccountController(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            userManager = serviceProvider.GetService<UserManager<User>>();
            roleManager = serviceProvider.GetService<RoleManager<Role>>();
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            signInManager = serviceProvider.GetService<SignInManager<User>>();
            this.configuration = configuration;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<ActionResult> Login(LoginModel model)
        {
            try
            {
                var user = await userManager.FindByNameAsync(model.Username);

                if (user != null)
                {
                    var result = await signInManager.CheckPasswordSignInAsync(user, model.Password, model.RememberMe);

                    if (result.Succeeded)
                    {
                        user = _context.Users.Where(x => x.Id == user.Id).Include(x => x.UserRoles).Include(x => x.Claims).First();
                        var role = _context.Roles.Where(x => x.Id == user.UserRoles.First().RoleId).Include(x => x.RoleClaims).First();
                        var roleClaims = role.RoleClaims.Select(x => x.ClaimValue).Distinct().ToList();

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var nowUtc = DateTime.UtcNow.ToUniversalTime();
                        var expires = nowUtc.AddDays(90).ToUniversalTime();

                        var claims = new List<Claim> { new Claim("Id", user.Id), new Claim("UserName", user.UserName) };
                        var token = new JwtSecurityToken(
                        configuration["Tokens:Issuer"],
                        configuration["Tokens:Audience"],
                        claims,
                        expires: expires,
                        signingCredentials: creds);
                        var tokenResponse = new JwtSecurityTokenHandler().WriteToken(token);

                        var data = new
                        {
                            user.Id,
                            Username = user.UserName,
                            user.Name,
                            user.Email,
                            user.PhoneNumber,
                            Role = role.Name,
                            Claims = roleClaims,
                            Token = tokenResponse,
                            Type = user.Type.ToString()
                        };

                        return Ok(new
                        {
                            data,
                            message = "Login Successful"
                        });
                    }

                    return BadRequest("Invalid Username or Password");
                }

                return BadRequest("Invalid Username or Password");
            }
            catch (Exception e)
            {
                return BadRequest(WebHelpers.ProcessException(e));
            }
        }

        [HttpGet]
        [Route("Logout")]
        public async Task<ActionResult> Logout()
        {
            try
            {
                await signInManager.SignOutAsync();
                return Ok(new { Message = "User Logged Out" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpDelete]
        [Route("DeleteUser")]
        public ActionResult DeleteUser(string id)
        {
            try
            {
                new UserRepository(_context).Delete(id);
                return Ok(new { Message = "User Deleted Successfully." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("system/createuser")]
        public async Task<ActionResult> CreateSystemUser(User model)
        {
            try
            {
                var roleRepo = new RoleRepository(_context);
                var role = roleRepo.GetByName(model.Role);
                //todo: do validations
                model.Id = Guid.NewGuid().ToString();
                model.Locked = false;
                model.Type = UserType.System;
                var result = await userManager.CreateAsync(model, model.Password).ConfigureAwait(true);

                if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.UserName).Result;
                    //Add to role
                    roleRepo.AddToRole(user.Id, role.Name);
                }
                else
                    return BadRequest(WebHelpers.ProcessException(result));

                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("system/updateuser")]
        public ActionResult UpdateSystemUser(User model)
        {
            try
            {
                var roleRepo = new RoleRepository(_context);
                var user = new UserRepository(_context).Get(model.UserName);
                var role = roleRepo.GetByName(model.Role);

                if (user == null) return NotFound("Updating user not found. Please update an existing user");

                user.Name = model.Name;
                user.UpdatedAt = DateTime.UtcNow.ToUniversalTime();
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;
                //user.Locked = false;
                new UserRepository(_context).Update(user);

                //Remove old role
                roleRepo.RemoveFromAllRoles(user.Id);

                //Add to role
                roleRepo.AddToRole(user.Id, role.Name);

                return Created("UpdateUser", new { user.Id, Message = "User has been updated successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("system/GetUsers")]
        public ActionResult GetSystemUsers()
        {
            try
            {
                var res = _context.Users.Where(x => x.Type == UserType.System).Include(x => x.UserRoles).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("system/QueryUsers")]
        public ActionResult QuerySystemUsers(UserFilter filter)
        {
            try
            {
                var res = filter.BuildQuery(_context.Users).Where(x => x.Type == UserType.System).Include(x => x.UserRoles).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No User Found" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.First().RoleId)?.Name,
                }).ToList();
                return Ok(new { Data = data, Total = total});
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("system/GetUser")]
        public ActionResult GetSystemUser(string id)
        {
            try
            {
                var res = _context.Users.Where(x => x.Id == id).Include(x => x.UserRoles).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("sector/createuser")]
        public async Task<ActionResult> CreateSectorUser(User model)
        {
            try
            {
                if (model.SectorId == null || model.SectorId < 1) throw new Exception("Please select the sector");
                var roleRepo = new RoleRepository(_context);
                var role = roleRepo.GetByName(model.Role);
                //todo: do validations
                model.Id = Guid.NewGuid().ToString();
                model.Locked = false;
                model.Sector = null;
                model.Type = UserType.Sector;
                var result = await userManager.CreateAsync(model, model.Password).ConfigureAwait(true);

                if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.UserName).Result;
                    //Add to role
                    roleRepo.AddToRole(user.Id, role.Name);
                }
                else
                    return BadRequest(WebHelpers.ProcessException(result));

                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("sector/updateuser")]
        public ActionResult UpdateSectorUser(User model)
        {
            try
            {
                if (model.SectorId == null || model.SectorId < 1) throw new Exception("Please select the sector");
                var roleRepo = new RoleRepository(_context);
                var user = new UserRepository(_context).Get(model.UserName);
                var role = roleRepo.GetByName(model.Role);

                if (user == null) return NotFound("Updating user not found. Please update an existing user");

                user.Name = model.Name;
                user.UpdatedAt = DateTime.UtcNow.ToUniversalTime();
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;
                user.SectorId = model.SectorId;
                //user.Locked = false;
                new UserRepository(_context).Update(user);

                //Remove old role
                roleRepo.RemoveFromAllRoles(user.Id);

                //Add to role
                roleRepo.AddToRole(user.Id, role.Name);

                return Created("UpdateUser", new { user.Id, Message = "User has been updated successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("sector/GetUsers")]
        public ActionResult GetSectorUsers()
        {
            try
            {
                var res = _context.Users.Where(x => x.Type == UserType.Sector).Include(x => x.UserRoles).Include(x=> x.Sector).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Type = x.Type.ToString(),
                    Sector = x.Sector.Name,
                    x.SectorId,
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("sector/QueryUsers")]
        public ActionResult QuerySectorUsers(UserFilter filter)
        {
            try
            {
                var res = filter.BuildQuery(_context.Users).Where(x => x.Type == UserType.Sector).Include(x => x.UserRoles).Include(x=> x.Sector).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "Could not find any users" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Type = x.Type.ToString(),
                    Sector = x.Sector.Name,
                    x.SectorId,
                    Role = new RoleRepository(_context).GetById(x.UserRoles.First().RoleId)?.Name,
                }).ToList();
                return Ok(new { Data = data, Total = total });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("sector/GetUser")]
        public ActionResult GetSectorUser(string id)
        {
            try
            {
                var res = _context.Users.Where(x => x.Id == id).Include(x => x.UserRoles).Include(x => x.Sector).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.Sector.Name,
                    x.SectorId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("region/createuser")]
        public async Task<ActionResult> CreateRegionUser(User model)
        {
            try
            {
                if (model.RegionId == null || model.RegionId < 1) throw new Exception("Please select the region");
                var roleRepo = new RoleRepository(_context);
                var role = roleRepo.GetByName(model.Role);
                //todo: do validations
                model.Id = Guid.NewGuid().ToString();
                model.Locked = false;
                model.Region = null;
                model.Type = UserType.Regional;
                var result = await userManager.CreateAsync(model, model.Password).ConfigureAwait(true);

                if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.UserName).Result;
                    //Add to role
                    roleRepo.AddToRole(user.Id, role.Name);
                }
                else
                    return BadRequest(WebHelpers.ProcessException(result));

                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("region/updateuser")]
        public ActionResult UpdateRegionUser(User model)
        {
            try
            {
                if (model.RegionId == null || model.RegionId < 1) throw new Exception("Please select the region");
                var roleRepo = new RoleRepository(_context);
                var user = new UserRepository(_context).Get(model.UserName);
                var role = roleRepo.GetByName(model.Role);

                if (user == null) return NotFound("Updating user not found. Please update an existing user");

                user.Name = model.Name;
                user.UpdatedAt = DateTime.UtcNow.ToUniversalTime();
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;
                user.RegionId = model.RegionId;
                //user.Locked = false;
                new UserRepository(_context).Update(user);

                //Remove old role
                roleRepo.RemoveFromAllRoles(user.Id);

                //Add to role
                roleRepo.AddToRole(user.Id, role.Name);

                return Created("UpdateUser", new { user.Id, Message = "User has been updated successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("region/GetUsers")]
        public ActionResult GetRegionUsers()
        {
            try
            {
                var res = _context.Users.Where(x => x.Type == UserType.Regional).Include(x => x.UserRoles).Include(x => x.Region.Sector).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.Region.Sector.Name,
                    x.Region.SectorId,
                    Region = x.Region.Name,
                    x.RegionId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("region/QueryUsers")]
        public ActionResult QueryRegionUsers(UserFilter filter)
        {
            try
            {
                var res = filter.BuildQuery(_context.Users).Where(x => x.Type == UserType.Regional).Include(x => x.UserRoles).Include(x => x.Region.Sector).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No User Found" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.Region.Sector.Name,
                    x.Region.SectorId,
                    Region = x.Region.Name,
                    x.RegionId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.First().RoleId)?.Name,
                }).ToList();
                return Ok(new { Data = data, Total = total });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("region/GetUser")]
        public ActionResult GetRegionUser(string id)
        {
            try
            {
                var res = _context.Users.Where(x => x.Id == id).Include(x => x.UserRoles).Include(x => x.Region.Sector).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.Region.Sector.Name,
                    x.Region.SectorId,
                    Region = x.Region.Name,
                    x.RegionId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("district/createuser")]
        public async Task<ActionResult> CreateDistrictUser(User model)
        {
            try
            {
                if (model.DistrictId == null || model.DistrictId < 1) throw new Exception("Please select the district");
                var roleRepo = new RoleRepository(_context);
                var role = roleRepo.GetByName(model.Role);
                //todo: do validations
                model.Id = Guid.NewGuid().ToString();
                model.Locked = false;
                model.Region = null;
                model.Type = UserType.District;
                var result = await userManager.CreateAsync(model, model.Password).ConfigureAwait(true);

                if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.UserName).Result;
                    //Add to role
                    roleRepo.AddToRole(user.Id, role.Name);
                }
                else
                    return BadRequest(WebHelpers.ProcessException(result));

                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("district/updateuser")]
        public ActionResult UpdateDistrictUser(User model)
        {
            try
            {
                if (model.DistrictId == null || model.DistrictId < 1) throw new Exception("Please select the district");
                var roleRepo = new RoleRepository(_context);
                var user = new UserRepository(_context).Get(model.UserName);
                var role = roleRepo.GetByName(model.Role);

                if (user == null) return NotFound("Updating user not found. Please update an existing user");

                user.Name = model.Name;
                user.UpdatedAt = DateTime.UtcNow.ToUniversalTime();
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;
                user.DistrictId = model.DistrictId;
                //user.Locked = false;
                new UserRepository(_context).Update(user);

                //Remove old role
                roleRepo.RemoveFromAllRoles(user.Id);

                //Add to role
                roleRepo.AddToRole(user.Id, role.Name);

                return Created("UpdateUser", new { user.Id, Message = "User has been updated successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("district/GetUsers")]
        public ActionResult GetDistrictUsers()
        {
            try
            {
                var res = _context.Users.Where(x => x.Type == UserType.District).Include(x => x.UserRoles).Include(x => x.District.Region.Sector).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.District.Region.Sector.Name,
                    x.District.Region.SectorId,
                    Region = x.District.Region.Name,
                    x.District.RegionId,
                    District = x.District.Name,
                    x.DistrictId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("district/QueryUsers")]
        public ActionResult QueryDistrictUsers(UserFilter filter)
        {
            try
            {
                var res = filter.BuildQuery(_context.Users).Where(x => x.Type == UserType.District).Include(x => x.UserRoles).Include(x => x.District.Region.Sector).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No User Found" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.District.Region.Sector.Name,
                    x.District.Region.SectorId,
                    Region = x.District.Region.Name,
                    x.District.RegionId,
                    District = x.District.Name,
                    x.DistrictId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.First().RoleId)?.Name,
                }).ToList();
                return Ok(new { Data = data, Total = total });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("district/GetUser")]
        public ActionResult GetDistrictUser(string id)
        {
            try
            {
                var res = _context.Users.Where(x => x.Id == id).Include(x => x.UserRoles).Include(x => x.District.Region.Sector).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.District.Region.Sector.Name,
                    x.District.Region.SectorId,
                    Region = x.District.Region.Name,
                    x.District.RegionId,
                    District = x.District.Name,
                    x.DistrictId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("outpost/createuser")]
        public async Task<ActionResult> CreateOutpostUser(User model)
        {
            try
            {
                if (model.OutpostId == null || model.OutpostId < 1) throw new Exception("Please select the outpost");
                var roleRepo = new RoleRepository(_context);
                var role = roleRepo.GetByName(model.Role);
                //todo: do validations
                model.Id = Guid.NewGuid().ToString();
                model.Locked = false;
                model.Region = null;
                model.Type = UserType.Outpost;
                var result = await userManager.CreateAsync(model, model.Password).ConfigureAwait(true);

                if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.UserName).Result;
                    //Add to role
                    roleRepo.AddToRole(user.Id, role.Name);
                }
                else
                    return BadRequest(WebHelpers.ProcessException(result));

                return Created("CreateUser", new { model.Id, Message = "User has been created Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("outpost/updateuser")]
        public ActionResult UpdateOutpostUser(User model)
        {
            try
            {
                if (model.OutpostId == null || model.OutpostId < 1) throw new Exception("Please select the outpost");
                var roleRepo = new RoleRepository(_context);
                var user = new UserRepository(_context).Get(model.UserName);
                var role = roleRepo.GetByName(model.Role);

                if (user == null) return NotFound("Updating user not found. Please update an existing user");

                user.Name = model.Name;
                user.UpdatedAt = DateTime.UtcNow.ToUniversalTime();
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;
                user.OutpostId = model.OutpostId;
                //user.Locked = false;
                new UserRepository(_context).Update(user);

                //Remove old role
                roleRepo.RemoveFromAllRoles(user.Id);

                //Add to role
                roleRepo.AddToRole(user.Id, role.Name);

                return Created("UpdateUser", new { user.Id, Message = "User has been updated successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("outpost/GetUsers")]
        public ActionResult GetOutpostUsers()
        {
            try
            {
                var res = _context.Users.Where(x => x.Type == UserType.Outpost).Include(x => x.UserRoles).Include(x => x.Outpost.District.Region.Sector).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.Outpost.District.Region.Sector.Name,
                    x.Outpost.District.Region.SectorId,
                    Region = x.Outpost.District.Region.Name,
                    x.Outpost.District.RegionId,
                    District = x.Outpost.District.Name,
                    x.Outpost.DistrictId,
                    Outpost = x.Outpost.Name,
                    x.OutpostId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("outpost/QueryUsers")]
        public ActionResult QueryOutpostUsers(UserFilter filter)
        {
            try
            {
                var res = filter.BuildQuery(_context.Users).Where(x => x.Type == UserType.Outpost).Include(x => x.UserRoles).Include(x => x.Outpost.District.Region.Sector).ToList();
                var total = res.Count();
                if (filter.Pager.Page > 0)
                    res = res.Take(filter.Pager.Size * filter.Pager.Page).ToList();
                if (!res.Any()) return Ok(new { Data = new List<object>(), Message = "No User Found" });
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.Outpost.District.Region.Sector.Name,
                    x.Outpost.District.Region.SectorId,
                    Region = x.Outpost.District.Region.Name,
                    x.Outpost.District.RegionId,
                    District = x.Outpost.District.Name,
                    x.Outpost.DistrictId,
                    Outpost = x.Outpost.Name,
                    x.OutpostId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.First().RoleId)?.Name,
                }).ToList();
                return Ok(new { Data = data, Total = total });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("outpost/GetUser")]
        public ActionResult GetOutpostUser(string id)
        {
            try
            {
                var res = _context.Users.Where(x=> x.Id == id).Include(x => x.UserRoles).Include(x => x.Outpost.District.Region.Sector).ToList();
                var data = res.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Email,
                    x.PhoneNumber,
                    x.UserName,
                    Sector = x.Outpost.District.Region.Sector.Name,
                    x.Outpost.District.Region.SectorId,
                    Region = x.Outpost.District.Region.Name,
                    x.Outpost.District.RegionId,
                    District = x.Outpost.District.Name,
                    x.Outpost.DistrictId,
                    Outpost = x.Outpost.Name,
                    x.OutpostId,
                    Type = x.Type.ToString(),
                    Role = new RoleRepository(_context).GetById(x.UserRoles.FirstOrDefault()?.RoleId)?.Name,
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }


        [HttpGet]
        [Route("GetClaims")]
        [AllowAnonymous]
        public ActionResult GetClaims()
        {
            try
            {
                var data = _context.RoleClaims.Select(x => x.ClaimValue).Distinct().ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetRoles")]
        [AllowAnonymous]
        public ActionResult GetRoles()
        {
            try
            {
                var data = _context.Roles.Where(x => !x.IsDeleted).Include(x => x.RoleClaims).ToList().Select(x => new Role
                {
                    Id = x.Id,
                    Name = x.Name,
                    Notes = x.Notes,
                    NormalizedName = x.NormalizedName,
                    Claims = x.RoleClaims.Select(c => c.ClaimValue).ToList()
                }).OrderBy(x => x.Name).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("GetRole/{id}")]
        public ActionResult GetRole(string id)
        {
            try
            {
                var data = _context.Roles.Where(x => x.Id == id).Include(x => x.RoleClaims).ToList().Select(x => new Role
                {
                    Id = x.Id,
                    Name = x.Name,
                    Notes = x.Notes,
                    NormalizedName = x.NormalizedName,
                    Claims = x.RoleClaims.Select(c => c.ClaimValue).ToList()
                }).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("CreateRole")]
        public ActionResult CreateRole(Role model)
        {
            try
            {
                model.NormalizedName = model.Name;
                var claims = model.Claims;
                var existingRole = _context.Roles.FirstOrDefault(x => x.Name == model.Name);
                if (existingRole == null)
                {
                    var res = roleManager.CreateAsync(model);
                    if (res.Result.Succeeded)
                    {
                        //var role = _context.Roles.First(x => x.Name == model.Name);
                        foreach (var c in claims)
                        {
                            roleManager.AddClaimAsync(model,
                        new Claim(GenericProperties.Privilege, c)).Wait();
                        }
                    }
                }
                else
                {
                    return BadRequest("There is an existing role with the same name. Consider updating the role.");
                }
                _context.SaveChanges();
                return Created("CreateRole", new { model.Id, Message = "Role has been created successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPut]
        [Route("UpdateRole")]
        public ActionResult UpdateRole(Role model)
        {
            try
            {
                //_context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                var claims = model.Claims;
                var role = _context.Roles.FirstOrDefault(x => x.Id == model.Id);
                if (role != null)
                {
                    role.Name = model.Name;
                    role.NormalizedName = model.Name;
                    role.Notes = model.Notes;
                    role.UpdatedAt = DateTime.UtcNow.ToUniversalTime();

                    var roleClaims = _context.RoleClaims.Where(x => x.RoleId == model.Id);
                    _context.RoleClaims.RemoveRange(roleClaims);
                    _context.SaveChanges();


                    foreach (var c in claims)
                    {
                        _context.RoleClaims.Add(new RoleClaim
                        {
                            ClaimType = GenericProperties.Privilege,
                            ClaimValue = c,
                            RoleId = model.Id
                        });
                    }

                    
                    _context.SaveChanges();
                }
                else
                {
                    return NotFound("Please check the role id.");
                }
                _context.SaveChanges();
                return Created("UpdateRole", new { model.Id, Message = "Role has been updated successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpDelete]
        [Route("DeleteRole")]
        public ActionResult DeleteRole(string id)
        {
            try
            {
                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                var role = _context.Roles.FirstOrDefault(x => x.Id == id);
                if (role != null)
                {
                    var roleClaims = _context.RoleClaims.Where(x => x.RoleId == id);
                    _context.RoleClaims.RemoveRange(roleClaims);

                    _context.Roles.Remove(role);
                }
                else
                {
                    return NotFound("Please check the role id.");
                }
                _context.SaveChanges();
                return Ok(new { Message = "Role Deleted Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Authorize]
        [Route("UpdateProfile")]
        public ActionResult UpdateProfile(User model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                user.Name = model.Name;
                user.UpdatedAt = DateTime.UtcNow;
                user.PhoneNumber = model.PhoneNumber;
                user.Email = model.Email;
                db.SaveChanges();

                return Created("UpdateProfile", new { model.Id, Message = "Profile has been updated successfully" });

            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [Authorize]
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.AsNoTracking().FirstOrDefault(x => x.Id == uId);
                var result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

                if (!result.Succeeded) return BadRequest(WebHelpers.ProcessException(result));

                return Ok(new { Message = "Password changed sucessfully." });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("resetpassword")]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            try
            {
                var us = _context.Users.FirstOrDefault(x => x.UserName == model.UserName && !x.Hidden && !x.IsDeleted);
                if (us == null) return NotFound("Unknown Username");
                var result = await userManager.RemovePasswordAsync(us);
                if (result.Succeeded)
                {
                    var res = await userManager.AddPasswordAsync(us, model.Password);
                    if (!res.Succeeded) return BadRequest(WebHelpers.ProcessException(res));
                }
                else return BadRequest(WebHelpers.ProcessException(result));

                _context.SaveChanges();
                return Ok(new { Message = "Password Reset Successful" });
            }
            catch (Exception e)
            {
                return BadRequest(WebHelpers.ProcessException(e));
            }
        }
    }
}
