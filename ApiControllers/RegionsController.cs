﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class RegionsController : BaseController<Region>
    {
        public RegionsController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var res = _context.Regions.Include(x => x.Sector).Include(x => x.Districts).Include(x => x.Materials).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    x.SectorId,
                    x.Email,
                    x.PhoneNumber,
                    Sector = x.Sector.Name,
                    Districts= x.Districts.Select(q => new { q.Id, q.Name }).ToList(),
                    MaterialIds = x.Materials.Select(m => m.MaterialId).ToList(),
                    Materials = string.Join(",", x.Materials.Select(m => m.Material?.Name).ToList())
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Post(Region model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.Sector:
                        {
                            model.SectorId = user.SectorId.Value;
                            break;
                        }
                }
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;
                var materialIds = model.MaterialIds?? new List<long>();
                model.MaterialIds = null;
                model.Materials = null;
                db.Regions.Add(model);

                foreach (var matId in materialIds)
                {
                    db.RegionMaterials.Add(new RegionMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        RegionId = model.Id,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();

                return Created("CreateRegion", new { Message = "Saved Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(Region model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var materialIds = model.MaterialIds ?? new List<long>();
                model.MaterialIds = null;
                model.Materials = null;

                var existing = db.Regions.First(x => x.Id == model.Id);

                existing.ModifiedAt = DateTime.UtcNow;
                existing.ModifiedBy = user.UserName;
                existing.Name = model.Name;
                existing.Notes = model.Notes;
                existing.PhoneNumber = model.PhoneNumber;
                existing.Email = model.Email;

                var mats = db.RegionMaterials.Where(x => x.RegionId == model.Id);
                db.RegionMaterials.RemoveRange(mats);
                foreach (var matId in materialIds)
                {
                    db.RegionMaterials.Add(new RegionMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        RegionId = model.Id,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();
                return Created("UpdateRegion", new { Message = "Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(RegionsFilter filter)
        {
            try
            {
                var raw = _repository.Query(filter);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Region Found" });
                var data = raw.Include(x => x.Materials).OrderByDescending(x => x.Id).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.Notes,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.SectorId,
                        x.Email,
                        x.PhoneNumber,
                        Sector = x.Sector.Name,
                        Districts = x.Districts.Select(q => new { q.Id, q.Name }).ToList(),
                        MaterialIds = x.Materials.Select(m => m.MaterialId).ToList(),
                        Materials = string.Join(",", x.Materials.Select(m => m.Material?.Name).ToList())
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("addorupdatematerials")]
        public async Task<ActionResult> AddOrUpdateMaterials(RegionMaterialDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.Regional:
                        {
                            model.RegionId = user.RegionId.Value;
                            break;
                        }
                }
                //clear existing
                var existing = db.RegionMaterials.Where(x => x.RegionId == model.RegionId);
                db.RegionMaterials.RemoveRange(existing);
                foreach (var matId in model.MaterialId)
                {
                    db.RegionMaterials.Add(new RegionMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        RegionId = model.RegionId,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();
                return Created("AddOrUpdateMaterials", new { Message = "Added Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getmaterials")]
        public async Task<ActionResult> GetMaterials(long regionId = 0)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (regionId == 0)
                {
                    switch (user.Type)
                    {
                        case UserType.Regional:
                            {
                                regionId = user.RegionId.Value;
                                break;
                            }
                    }
                }
                var data = db.RegionMaterials.Where(x => x.RegionId == regionId).Include(x => x.Material).Include(x => x.Region).ToList().Select(x => new {
                    x.Id,
                    x.RegionId,
                    Region = x.Region.Name,
                    x.MaterialId,
                    x.Material.Name
                }).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
