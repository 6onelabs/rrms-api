﻿using Microsoft.AspNetCore.Mvc;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class RanksController : BaseController<Rank>
    {
        public RanksController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var res = _repository.Get().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.MinAge,
                    x.MaxAge,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }

}
