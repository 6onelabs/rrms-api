﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class DistrictsController : BaseController<District>
    {
        public DistrictsController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var res = _context.Districts.Include(x => x.Region.Sector).Include(x=> x.Materials).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.RegionId,
                    x.Notes,
                    x.Email,
                    x.PhoneNumber,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy,
                    Region = x.Region.Name,
                    Sector = x.Region.Sector.Name,
                    MaterialIds = x.Materials.Select(m=> m.MaterialId).ToList(),
                    Materials = string.Join(",", x.Materials.Select(m => m.Material?.Name).ToList())
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Post(District model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.Regional:
                        {
                            model.RegionId = user.RegionId.Value;
                            break;
                        }
                }
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;
                var materialIds = model.MaterialIds ?? new List<long>();
                model.MaterialIds = null;
                model.Materials = null;
                db.Districts.Add(model);

                foreach (var matId in materialIds)
                {
                    db.DistrictMaterials.Add(new DistrictMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        DistrictId = model.Id,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();

                return Created("CreateDistrict", new { Message = "Saved Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(District model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                var materialIds = model.MaterialIds ?? new List<long>();
                model.MaterialIds = null;
                model.Materials = null;

                var existing = db.Districts.First(x => x.Id == model.Id);

                existing.ModifiedAt = DateTime.UtcNow;
                existing.ModifiedBy = user.UserName;
                existing.Name = model.Name;
                existing.Notes = model.Notes;
                existing.PhoneNumber = model.PhoneNumber;
                existing.Email = model.Email;
                existing.RegionId = model.RegionId;

                var mats = db.DistrictMaterials.Where(x => x.DistrictId == model.Id);
                db.DistrictMaterials.RemoveRange(mats);
                foreach (var matId in materialIds)
                {
                    db.DistrictMaterials.Add(new DistrictMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        DistrictId = model.Id,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();
                return Created("UpdateDistrict", new { Message = "Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(DistrictsFilter filter)
        {
            try
            {
                var raw = _repository.Query(filter);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No District Found" });
                var data = raw.Include(x => x.Materials).OrderByDescending(x => x.Id).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.RegionId,
                        x.Notes,
                        x.Email,
                        x.PhoneNumber,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        Region = x.Region.Name,
                        Sector = x.Region.Sector.Name,
                        MaterialIds = x.Materials.Select(m => m.MaterialId).ToList(),
                        Materials = string.Join(",", x.Materials.Select(m => m.Material?.Name).ToList())
                    }).ToList();
                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("addorupdatematerials")]
        public async Task<ActionResult> AddOrUpdateMaterials(DistrictMaterialDTO model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");

                switch (user.Type)
                {
                    case UserType.District:
                        {
                            model.DistrictId = user.DistrictId.Value;
                            break;
                        }
                }
                //clear existing
                var existing = db.DistrictMaterials.Where(x => x.DistrictId == model.DistrictId);
                db.DistrictMaterials.RemoveRange(existing);
                foreach(var matId in model.MaterialId)
                {
                    db.DistrictMaterials.Add(new DistrictMaterial
                    {
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = user.UserName,
                        ModifiedBy = user.UserName,
                        DistrictId = model.DistrictId,
                        MaterialId = matId
                    });
                }
                db.SaveChanges();
                return Created("AddOrUpdateMaterials", new { Message = "Added Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getmaterials")]
        public async Task<ActionResult> GetMaterials(long districtId = 0)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if(districtId == 0)
                {
                    switch (user.Type)
                    {
                        case UserType.District:
                            {
                                districtId = user.DistrictId.Value;
                                break;
                            }
                    }
                }
                var data = db.DistrictMaterials.Where(x => x.DistrictId == districtId).Include(x => x.Material).Include(x => x.District).ToList().Select(x => new {
                    x.Id,
                    x.DistrictId,
                    District = x.District.Name,
                    x.MaterialId,
                    x.Material.Name
                }).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }

}
