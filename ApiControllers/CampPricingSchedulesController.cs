﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class CampPricingSchedulesController : BaseController<CampPricingSchedule>
    {
        public CampPricingSchedulesController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var res = _context.CampPricingSchedules.ToList().Select(x => new
                {
                    x.Id,
                    x.EndDate,
                    x.StartDate,
                    x.AmountPayable,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.CampId,
                    Camp = x.Camp.Name
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("getbycamp")]
        public ActionResult GetbyCamp(long campId)
        {
            try
            {
                var data =
                    _context.CampPricingSchedules.Where(x => x.CampId == campId)
                        .ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.EndDate,
                                    x.StartDate,
                                    x.AmountPayable,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.CampId,
                                    Camp = x.Camp.Name
                                })
                        .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Post(CampPricingSchedule model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                model.CreatedAt = DateTime.UtcNow;
                model.ModifiedAt = DateTime.UtcNow;
                model.CreatedBy = user.UserName;
                model.ModifiedBy = user.UserName;
                db.CampPricingSchedules.Add(model);
                db.SaveChanges();

                return Created("CreateSchedule", new { Message = "Saved Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(CampPricingSchedule model)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var db = _context;
                var user = db.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var existing = db.CampPricingSchedules.First(x => x.Id == model.Id);

                existing.ModifiedAt = DateTime.UtcNow;
                existing.ModifiedBy = user.UserName;
                existing.CampId = model.CampId;
                existing.StartDate = model.StartDate;
                existing.EndDate = model.EndDate;
                existing.AmountPayable = model.AmountPayable;
                db.SaveChanges();
                return Created("UpdateSchedule", new { Message = "Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
