﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Filters;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using RoyalRangersAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class CampsController : BaseController<Camp>
    {
        public CampsController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.Where(x => x.Id == uId).Include(x => x.District.Region.Sector).Include(x => x.Sector).Include(x => x.Region.Sector).Include(x=> x.Outpost.District.Region.Sector).FirstOrDefault();
                if (user == null) throw new Exception("Could not find user");
                var campType = new CampRepository(_context).GetCampType(user.Type.ToString());
                var raw = _context.Camps.Where(x => x.Id > 0);
                if (user.Type == UserType.Outpost)
                {
                    raw = raw.Where(x => x.DistrictId == user.Outpost.DistrictId || x.RegionId == user.Outpost.District.RegionId || x.SectorId == user.Outpost.District.Region.SectorId || x.Type == CampType.National);
                }
                else
                {
                    if (campType == CampType.District) raw = raw.Where(x => x.DistrictId == user.DistrictId || x.RegionId == user.District.RegionId || x.SectorId == user.District.Region.SectorId || x.Type == CampType.National);
                    else if (campType == CampType.Regional) raw = raw.Where(x => x.RegionId == user.RegionId || x.SectorId == user.Region.SectorId || x.Type == CampType.National);
                    else if (campType == CampType.Sector) raw = raw.Where(x => x.SectorId == user.SectorId || x.Type == CampType.National);
                    else raw = raw.Where(x => x.Type == CampType.National);
                }

                var data = raw.Include(x => x.District).Include(x => x.Sector).Include(x => x.Region).ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Name,
                                    x.Code,
                                    x.Location,
                                    x.Theme,
                                    x.StartDate,
                                    x.EndDate,
                                    x.Logo,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.IsActive,
                                    x.Fee,
                                    x.MinRegAmount,
                                    x.MaxClassSize,
                                    x.MaxRoomSize,
                                    x.MaxTableSize,
                                    x.MaxNumberOfPatrols,
                                    Type = x.Type.ToString(),
                                    x.DistrictId,
                                    x.SectorId,
                                    x.RegionId,
                                    District = x.District?.Name,
                                    Sector = x.Sector?.Name,
                                    Region = x.Region?.Name
                                })
                        .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpGet]
        [Route("getactive")]
        public async Task<ActionResult> GetActive()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.Where(x => x.Id == uId).Include(x => x.District.Region.Sector).Include(x => x.Sector).Include(x => x.Region.Sector).Include(x => x.Outpost.District.Region.Sector).FirstOrDefault();
                if (user == null) throw new Exception("Could not find user");
                var campType = new CampRepository(_context).GetCampType(user.Type.ToString());
                var raw = _context.Camps.Where(x => x.IsActive);

                if (user.Type == UserType.Outpost)
                {
                    raw = raw.Where(x => x.DistrictId == user.Outpost.DistrictId || x.RegionId == user.Outpost.District.RegionId || x.SectorId == user.Outpost.District.Region.SectorId || x.Type == CampType.National);
                }
                else
                {
                    if (campType == CampType.District) raw = raw.Where(x => x.DistrictId == user.DistrictId || x.RegionId == user.District.RegionId || x.SectorId == user.District.Region.SectorId || x.Type == CampType.National);
                    else if (campType == CampType.Regional) raw = raw.Where(x => x.RegionId == user.RegionId || x.SectorId == user.Region.SectorId || x.Type == CampType.National);
                    else if (campType == CampType.Sector) raw = raw.Where(x => x.SectorId == user.SectorId || x.Type == CampType.National);
                    else raw = raw.Where(x => x.Type == CampType.National);
                }
                var data = raw.Include(x => x.District).Include(x => x.Sector).Include(x => x.Region).ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Name,
                                    x.Code,
                                    x.Location,
                                    x.Theme,
                                    x.StartDate,
                                    x.EndDate,
                                    x.Logo,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.IsActive,
                                    x.Fee,
                                    x.MinRegAmount,
                                    x.MaxClassSize,
                                    x.MaxRoomSize,
                                    x.MaxTableSize,
                                    x.MaxNumberOfPatrols,
                                    Type = x.Type.ToString(),
                                    x.DistrictId,
                                    x.SectorId,
                                    x.RegionId,
                                    District = x.District?.Name,
                                    Sector = x.Sector?.Name,
                                    Region = x.Region?.Name
                                })
                        .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Post(Camp record)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var campType = new CampRepository(_context).GetCampType(user.Type.ToString());
                record.IsActive = false;
                record.Type = campType;
                record.Code = StringGenerators.GenerateRandomNumber(15);
                if(campType == CampType.District)
                {
                    record.DistrictId = user.DistrictId;
                    record.RegionId = null;
                    record.SectorId = null;
                }
                else if (campType == CampType.Regional)
                {
                    record.DistrictId = null;
                    record.RegionId = user.RegionId;
                    record.SectorId = null;
                }
                else if (campType == CampType.Sector)
                {
                    record.DistrictId = null;
                    record.RegionId = null;
                    record.SectorId = user.SectorId;
                }
                _repository.Insert(SetAudit(record, true));
                new CampRepository(_context).UpdateCampCode(record.Id);
                return Created("CreateCamp", new { Message = "Saved Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        public override async Task<ActionResult> Put(Camp record)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var camp = _context.Camps.First(x => x.Id == record.Id);
                camp.Name = record.Name;
                camp.StartDate = record.StartDate;
                camp.EndDate = record.EndDate;
                camp.Location = record.Location;
                camp.Theme = record.Theme;
                camp.Logo = record.Logo;
                camp.IsActive = record.IsActive;
                camp.Fee = record.Fee;
                camp.MinRegAmount = record.MinRegAmount;
                camp.HasClasses = record.HasClasses;
                camp.MaxClassSize = record.MaxClassSize;
                camp.HasTables = record.HasTables;
                camp.MaxTableSize = record.MaxTableSize;
                camp.HasRooms = record.HasRooms;
                camp.MaxRoomSize = record.MaxRoomSize;
                camp.HasPatrols = record.HasPatrols;
                camp.MaxNumberOfPatrols = record.MaxNumberOfPatrols;
                _context.SaveChanges();
                return Created("UpdateCamp", new { Message = "Updated Successful" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
        [HttpGet]
        [Route("activate")]
        public async Task<ActionResult> Activate(long id)
        {
            try
            {
                var repo = new CampRepository(_context);
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var campType = repo.GetCampType(user.Type.ToString());
                repo.Activate(id, campType);
                return Ok(new { Message = "Camp has been activated Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("deactivate")]
        public async Task<ActionResult> Deactivate(long id)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                var campType = new CampRepository(_context).GetCampType(user.Type.ToString());
                new CampRepository(_context).Deactivate(id, campType);
                return Ok(new { Message = "Camp has been deactivated Successfully" });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("query")]
        public async Task<ActionResult> Query(CampsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.Where(x => x.Id == uId).Include(x => x.District.Region.Sector).Include(x => x.Sector).Include(x => x.Region.Sector).Include(x => x.Outpost.District.Region.Sector).FirstOrDefault();
                if (user == null) throw new Exception("Could not find user");
                var campType = new CampRepository(_context).GetCampType(user.Type.ToString());

                var raw = _context.Camps.Where(x => x.Type == campType);
                if (user.Type == UserType.Outpost)
                {
                    raw = raw.Where(x => x.DistrictId == user.Outpost.DistrictId || x.RegionId == user.Outpost.District.RegionId || x.SectorId == user.Outpost.District.Region.SectorId || x.Type == CampType.National);
                }
                else
                {
                    if (campType == CampType.District) raw = raw.Where(x => x.DistrictId == user.DistrictId || x.RegionId == user.District.RegionId || x.SectorId == user.District.Region.SectorId || x.Type == CampType.National);
                    else if (campType == CampType.Regional) raw = raw.Where(x => x.RegionId == user.RegionId || x.SectorId == user.Region.SectorId || x.Type == CampType.National);
                    else if (campType == CampType.Sector) raw = raw.Where(x => x.SectorId == user.SectorId || x.Type == CampType.National);
                    else raw = raw.Where(x => x.Type == CampType.National);
                }
                raw = filter.BuildQuery(raw);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Camp Found" });
                var data = raw.OrderBy(x => x.Name).ThenBy(x => x.StartDate).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    x.Location,
                    x.Theme,
                    x.StartDate,
                    x.EndDate,
                    x.Logo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.IsActive,
                    x.Fee,
                    x.MinRegAmount,
                    x.MaxNumberOfPatrols,
                    x.MaxClassSize,
                    x.MaxRoomSize,
                    x.MaxTableSize,
                    Type = x.Type.ToString(),
                    x.DistrictId,
                    x.SectorId,
                    x.RegionId,
                    District = x.District?.Name,
                    Sector = x.Sector?.Name,
                    Region = x.Region?.Name
                }).ToList();

                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("adminget")]
        public async Task<ActionResult> AdminGet()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.Where(x => x.Id == uId).Include(x => x.District.Region.Sector).Include(x => x.Sector).Include(x => x.Region.Sector).Include(x => x.Outpost.District.Region.Sector).FirstOrDefault();
                if (user == null) throw new Exception("Could not find user");
                var campType = new CampRepository(_context).GetCampType(user.Type.ToString());
                var raw = _context.Camps.Where(x => x.Id > 0);
                if (user.Type == UserType.Outpost)
                {
                    raw = raw.Where(x => x.Id == 0);
                }
                else
                {
                    if (user.Type == UserType.District) raw = raw.Where(x => x.DistrictId == user.DistrictId && x.Type == CampType.District);
                    else if (user.Type == UserType.Regional) raw = raw.Where(x => x.RegionId == user.RegionId && x.Type == CampType.Regional);
                    else if (user.Type == UserType.Sector) raw = raw.Where(x => x.SectorId == user.SectorId && x.Type == CampType.Sector);
                    else raw = raw.Where(x => x.Type == CampType.National);
                }

                var data = raw.Include(x => x.District).Include(x => x.Sector).Include(x => x.Region).ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Name,
                                    x.Code,
                                    x.Location,
                                    x.Theme,
                                    x.StartDate,
                                    x.EndDate,
                                    x.Logo,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.IsActive,
                                    x.Fee,
                                    x.MinRegAmount,
                                    x.MaxClassSize,
                                    x.MaxRoomSize,
                                    x.MaxTableSize,
                                    x.MaxNumberOfPatrols,
                                    Type = x.Type.ToString(),
                                    x.DistrictId,
                                    x.SectorId,
                                    x.RegionId,
                                    District = x.District?.Name,
                                    Sector = x.Sector?.Name,
                                    Region = x.Region?.Name
                                })
                        .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpPost]
        [Route("adminquery")]
        public async Task<ActionResult> AdminQuery(CampsFilter filter)
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.Where(x => x.Id == uId).Include(x => x.District.Region.Sector).Include(x => x.Sector).Include(x => x.Region.Sector).Include(x => x.Outpost.District.Region.Sector).FirstOrDefault();
                if (user == null) throw new Exception("Could not find user");
                var campType = new CampRepository(_context).GetCampType(user.Type.ToString());

                var raw = _context.Camps.Where(x => x.Type == campType);
                if (user.Type == UserType.Outpost)
                {
                    raw = raw.Where(x => x.Id == 0);
                }
                else
                {
                    if (user.Type == UserType.District) raw = raw.Where(x => x.DistrictId == user.DistrictId && x.Type == CampType.District);
                    else if (user.Type == UserType.Regional) raw = raw.Where(x => x.RegionId == user.RegionId && x.Type == CampType.Regional);
                    else if (user.Type == UserType.Sector) raw = raw.Where(x => x.SectorId == user.SectorId && x.Type == CampType.Sector);
                    else raw = raw.Where(x => x.Type == CampType.National);
                }
                raw = filter.BuildQuery(raw);
                var total = raw.Count();
                if (filter.Pager.Page > 0)
                    raw = raw.Take(filter.Pager.Size * filter.Pager.Page);
                if (!raw.Any()) return Ok(new { Data = new List<object>(), Message = "No Camp Found" });
                var data = raw.OrderBy(x => x.Name).ThenBy(x => x.StartDate).ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Code,
                    x.Location,
                    x.Theme,
                    x.StartDate,
                    x.EndDate,
                    x.Logo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.IsActive,
                    x.Fee,
                    x.MinRegAmount,
                    x.MaxNumberOfPatrols,
                    x.MaxClassSize,
                    x.MaxRoomSize,
                    x.MaxTableSize,
                    Type = x.Type.ToString(),
                    x.DistrictId,
                    x.SectorId,
                    x.RegionId,
                    District = x.District?.Name,
                    Sector = x.Sector?.Name,
                    Region = x.Region?.Name
                }).ToList();

                return Ok(new
                {
                    data,
                    total,
                    message = "Loaded Successfully"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("admingetactive")]
        public async Task<ActionResult> AdminGetActive()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.Where(x => x.Id == uId).Include(x => x.District.Region.Sector).Include(x => x.Sector).Include(x => x.Region.Sector).Include(x => x.Outpost.District.Region.Sector).FirstOrDefault();
                if (user == null) throw new Exception("Could not find user");
                var campType = new CampRepository(_context).GetCampType(user.Type.ToString());
                var raw = _context.Camps.Where(x => x.IsActive);

                if (user.Type == UserType.Outpost)
                {
                    raw = raw.Where(x => x.Id == 0);
                }
                else
                {
                    if (user.Type == UserType.District) raw = raw.Where(x => x.DistrictId == user.DistrictId && x.Type == CampType.District);
                    else if (user.Type == UserType.Regional) raw = raw.Where(x => x.RegionId == user.RegionId && x.Type == CampType.Regional);
                    else if (user.Type == UserType.Sector) raw = raw.Where(x => x.SectorId == user.SectorId && x.Type == CampType.Sector);
                    else raw = raw.Where(x => x.Type == CampType.National);
                }
                var data = raw.Include(x => x.District).Include(x => x.Sector).Include(x => x.Region).ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Name,
                                    x.Code,
                                    x.Location,
                                    x.Theme,
                                    x.StartDate,
                                    x.EndDate,
                                    x.Logo,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.IsActive,
                                    x.Fee,
                                    x.MinRegAmount,
                                    x.MaxClassSize,
                                    x.MaxRoomSize,
                                    x.MaxTableSize,
                                    x.MaxNumberOfPatrols,
                                    Type = x.Type.ToString(),
                                    x.DistrictId,
                                    x.SectorId,
                                    x.RegionId,
                                    District = x.District?.Name,
                                    Sector = x.Sector?.Name,
                                    Region = x.Region?.Name
                                })
                        .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }

}
