﻿using Microsoft.AspNetCore.Mvc;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    public class AchievementsController : BaseController<Achievement>
    {
        public AchievementsController(ApplicationDbContext context) : base(context)
        {
        }
        public override async Task<ActionResult> Get()
        {
            try
            {
                var res = _context.Achievements.ToList().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Notes,
                    x.CreatedAt,
                    x.CreatedBy,
                    x.ModifiedAt,
                    x.ModifiedBy
                }).ToList();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
