﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using RoyalRangersAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DashboardController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration configuration;

        public DashboardController(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            this.configuration = configuration;
        }

        [HttpGet]
        [Route("outpost/getstats")]
        public async Task<ActionResult> GetOutpostStats()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (user.OutpostId == null || user.OutpostId <= 0 || user.Type != UserType.Outpost)
                    throw new Exception("You are not allowed to make this request.");

                var rangers = _context.Rangers.Count(r => r.OutpostId == user.OutpostId);
                var boys = _context.Rangers.Count(r => r.OutpostId == user.OutpostId && r.Gender == Gender.Male);
                var girls = _context.Rangers.Count(r => r.OutpostId == user.OutpostId && r.Gender == Gender.Female);
                var bal = _context.OutpostBalances.FirstOrDefault(x => x.OutpostId == user.OutpostId).Amount;

                var res = new
                {
                    TotalRangers = rangers,
                    MaleRangers = boys,
                    FemaleRangers = girls,
                    Balance = bal
                };
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("outpost/getsummaries")]
        public async Task<ActionResult> GetOutpostSummaries()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (user.OutpostId == null || user.OutpostId <= 0 || user.Type != UserType.Outpost)
                    throw new Exception("You are not allowed to make this request.");
                var ranksList = new List<RanksSummaryModel>();
                var rangers = _context.Rangers.Where(r => r.OutpostId == user.OutpostId).ToList();
                var campRepo = new CampRepository(_context);
                var rangs = rangers.Select(x => new RangerSummaryModel
                {
                    Rank = campRepo.GetRank(x.Age)?.Name,
                    Gender = x.Gender
                }).ToList();

                var ranks = rangs.Select(x => x.Rank).Distinct();
                foreach(var r in ranks)
                {
                    var cnt = rangs.Count(x => x.Rank == r);
                    ranksList.Add(new RanksSummaryModel()
                    {
                        Rank = r,
                        Count = cnt
                    });
                }

                var bal = _context.OutpostBalances.Where(x => x.OutpostId == user.OutpostId).FirstOrDefault();
                if (bal == null) throw new Exception("Please check your account");

                var raw = new List<BalanceStatement>();
                var mantopups = _context.ManualTopupTransactions.Where(x => x.OutpostBalanceId == bal.Id && x.Status == TopupTransactionStatus.Processed).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Credit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(mantopups);
                var momotopups = _context.TopupTransactions.Where(x => x.OutpostBalanceId == bal.Id && x.Status == TopupTransactionStatus.Processed).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = "Momo Topup",
                    Type = "Credit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(momotopups);
                var debits = _context.BalanceDebits.Where(x => x.OutpostBalanceId == bal.Id).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Debit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(debits);

                var trans = raw.OrderByDescending(x => x.Date).Take(5).ToList();

                var res = new
                {
                    Ranks = ranksList,
                    Transactions = trans
                };
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("outpost/getcampstats")]
        public async Task<ActionResult> GetOutpostCampStats(long campId)
        {
            try
            {
                if (campId <= 0) throw new Exception("Please select a camp");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (user.OutpostId == null || user.OutpostId <= 0 || user.Type != UserType.Outpost)
                    throw new Exception("You are not allowed to make this request.");

                var members = _context.CampMembers.Where(r => r.CampId == campId && r.Ranger.OutpostId == user.OutpostId && r.Status != CampMemberStatus.Cancelled).Include(x=> x.Ranger.Outpost).Include(x=> x.Camp).ToList();

                var memberIds = members.Select(x => x.Id).ToList();
                var payments = _context.Payments.Where(x => memberIds.Contains(x.CampMemberId)).Select(x => x.Amount).ToList();
                var ranksList = new List<RanksSummaryModel>();
                var campRepo = new CampRepository(_context);
                var rangers = members.Select(x => new RangerSummaryModel
                {
                    Rank = campRepo.GetRank(x.Ranger.Age)?.Name
                }).ToList();
                
                var ranks = rangers.Select(x => x.Rank).Distinct();
                foreach (var r in ranks)
                {
                    var cnt = rangers.Count(x => x.Rank == r);
                    ranksList.Add(new RanksSummaryModel()
                    {
                        Rank = r,
                        Count = cnt
                    });
                }

                var res = new
                {
                    TotalCampers = members.Count(),
                    MaleCampers = members.Count(x=> x.Ranger.Gender == Gender.Male),
                    FemaleCampers = members.Count(x => x.Ranger.Gender == Gender.Female),
                    TotalPayments = payments.Sum(),
                    Ranks = ranksList
                };
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("admin/getstats")]
        public async Task<ActionResult> GetAdminStats()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (user.Type == UserType.Outpost)
                    throw new Exception("You are not allowed to make this request.");

                var outposts = _context.Outposts.Where(x => x.Id > 0).Include(x=> x.District.Region.Sector);
                if (user.Type == UserType.District) outposts = outposts.Where(x => x.DistrictId == user.DistrictId).Include(x => x.District.Region.Sector);
                else if (user.Type == UserType.Regional) outposts = outposts.Where(x => x.District.RegionId == user.RegionId).Include(x => x.District.Region.Sector);
                else if (user.Type == UserType.Sector) outposts = outposts.Where(x => x.District.Region.SectorId == user.SectorId).Include(x => x.District.Region.Sector);

                var outpostIds = outposts.Select(x => x.Id).ToList();

                var rangers = _context.Rangers.Where(r => outpostIds.Contains(r.OutpostId));
                var boys = rangers.Count(r => r.Gender == Gender.Male);
                var girls = rangers.Count(r => r.Gender == Gender.Female);
                var bal = _context.OutpostBalances.Where(x => outpostIds.Contains(x.OutpostId)).Sum(x=> x.Amount);

                var res = new
                {
                    TotalRangers = rangers.Count(),
                    MaleRangers = boys,
                    FemaleRangers = girls,
                    TotalOutpostBalances = bal
                };
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("admin/getsummaries")]
        public async Task<ActionResult> GetAdminSummaries()
        {
            try
            {
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user == null) throw new Exception("Could not find user");
                if (user.Type == UserType.Outpost)
                    throw new Exception("You are not allowed to make this request.");

                var outposts = _context.Outposts.Where(x => x.Id > 0).Include(x => x.District.Region.Sector);
                if (user.Type == UserType.District) outposts = outposts.Where(x => x.DistrictId == user.DistrictId).Include(x => x.District.Region.Sector);
                else if (user.Type == UserType.Regional) outposts = outposts.Where(x => x.District.RegionId == user.RegionId).Include(x => x.District.Region.Sector);
                else if (user.Type == UserType.Sector) outposts = outposts.Where(x => x.District.Region.SectorId == user.SectorId).Include(x => x.District.Region.Sector);
                var outpostIds = outposts.Select(x => x.Id).ToList();

                var ranksList = new List<RanksSummaryModel>();
                var rangers = _context.Rangers.Where(r => outpostIds.Contains(r.OutpostId)).ToList();
                var campRepo = new CampRepository(_context);
                var rangs = rangers.Select(x => new RangerSummaryModel
                {
                    Rank = campRepo.GetRank(x.Age)?.Name,
                    Gender = x.Gender
                }).ToList();

                var ranks = rangs.Select(x => x.Rank).Distinct();
                foreach (var r in ranks)
                {
                    var cnt = rangs.Count(x => x.Rank == r);
                    ranksList.Add(new RanksSummaryModel()
                    {
                        Rank = r,
                        Count = cnt
                    });
                }

                var bals = _context.OutpostBalances.Where(x => outpostIds.Contains(x.OutpostId)).ToList();
                var balsIds = bals.Select(x => x.Id).ToList();

                var raw = new List<BalanceStatement>();
                var mantopups = _context.ManualTopupTransactions.Where(x => balsIds.Contains(x.OutpostBalanceId) && x.Status == TopupTransactionStatus.Processed).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Credit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(mantopups);
                var momotopups = _context.TopupTransactions.Where(x => balsIds.Contains(x.OutpostBalanceId) && x.Status == TopupTransactionStatus.Processed).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = "Momo Topup",
                    Type = "Credit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(momotopups);
                var debits = _context.BalanceDebits.Where(x => balsIds.Contains(x.OutpostBalanceId)).Select(x => new BalanceStatement
                {
                    Reference = x.Reference,
                    Notes = x.Notes,
                    Type = "Debit",
                    Amount = x.Amount,
                    Date = x.CreatedAt
                }).ToList();
                raw.AddRange(debits);

                var trans = raw.OrderByDescending(x => x.Date).Take(20).ToList();

                var res = new
                {
                    Ranks = ranksList,
                    Transactions = trans
                };
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }

        [HttpGet]
        [Route("admin/getcampstats")]
        public async Task<ActionResult> GetAdminCampStats(long campId)
        {
            try
            {
                if (campId <= 0) throw new Exception("Please select a camp");
                var uId = User.FindFirst("Id")?.Value;
                var user = _context.Users.FirstOrDefault(x => x.Id == uId);
                if (user.Type == UserType.Outpost)
                    throw new Exception("You are not allowed to make this request.");

                var outposts = _context.Outposts.Where(x => x.Id > 0).Include(x => x.District.Region.Sector).ToList();
                if (user.Type == UserType.District) outposts = outposts.Where(x => x.DistrictId == user.DistrictId).ToList();
                else if (user.Type == UserType.Regional) outposts = outposts.Where(x => x.District.RegionId == user.RegionId).ToList();
                else if (user.Type == UserType.Sector) outposts = outposts.Where(x => x.District.Region.SectorId == user.SectorId).ToList();
                var outpostIds = outposts.Select(x => x.Id).ToList();

                var members = _context.CampMembers.Where(r => r.CampId == campId && outpostIds.Contains(r.Ranger.OutpostId) && r.Status != CampMemberStatus.Cancelled).Include(x => x.Ranger).ToList();

                var memberIds = members.Select(x => x.Id).ToList();
                var payments = _context.Payments.Where(x => memberIds.Contains(x.CampMemberId)).Select(x => x.Amount).ToList();
                var ranksList = new List<RanksSummaryModel>();
                var campRepo = new CampRepository(_context);
                var rangers = members.Select(x => new RangerSummaryModel
                {
                    Rank = campRepo.GetRank(x.Ranger.Age)?.Name
                }).ToList();

                var ranks = rangers.Select(x => x.Rank).Distinct();
                foreach (var r in ranks)
                {
                    var cnt = rangers.Count(x => x.Rank == r);
                    ranksList.Add(new RanksSummaryModel()
                    {
                        Rank = r,
                        Count = cnt
                    });
                }

                var res = new
                {
                    TotalCampers = members.Count(),
                    MaleCampers = members.Count(x => x.Ranger.Gender == Gender.Male),
                    FemaleCampers = members.Count(x => x.Ranger.Gender == Gender.Female),
                    TotalPayments = payments.Sum(),
                    TotalExpenses = 0,
                    Ranks = ranksList
                };
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(WebHelpers.ProcessException(ex));
            }
        }
    }
}
