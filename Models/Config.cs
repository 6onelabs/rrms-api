﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.Models
{
    public static class Config
    {
        public static Setting Setting { get; set; }
    }

    public class Setting
    {
        public AppSettings AppSettings { get; set; }
    }

    public class AppSettings
    {
        public double CharteringFee { get; set; } = 20.0;
        public int CharteringStartMonth { get; set; } = 1;
        public int CharteringEndMonth { get; set; } = 12;
    }
}
