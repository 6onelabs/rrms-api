﻿namespace RoyalRangersAPI.Models
{
    public class Privileges
    {
        public const string CanViewDashboard = "CanViewDashboard";
        public const string CanViewAdministration = "CanViewAdministration";
        public const string CanViewMembership = "CanViewMembership";
        public const string CanViewChartering = "CanViewChartering";

        //Reports
        public const string CanViewReports = "CanViewReports";
        public const string CanViewOutpostReports = "CanViewOutpostReports";
        public const string CanViewDistrictReports = "CanViewDistrictReports";
        public const string CanViewRegionalReports = "CanViewRegionalReports";
        public const string CanViewSectorReports = "CanViewSectorReports";
        public const string CanViewSystemReports = "CanViewSystemReports";
        
        //Roles
        public const string CanCreateRoles = "CanCreateRoles";
        public const string CanViewRoles = "CanViewRoles";
        public const string CanUpdateRoles = "CanUpdateRoles";
        public const string CanDeleteRoles = "CanDeleteRoles";

        //System Users
        public const string IsSystemUser = "IsSystemUser";
        public const string CanCreateSystemUsers = "CanCreateSystemUsers";
        public const string CanViewSystemUsers = "CanViewSystemUsers";
        public const string CanUpdateSystemUsers = "CanUpdateSystemUsers";
        public const string CanDeleteSystemUsers = "CanDeleteSystemUsers";
        public const string CanActivateSystemUsers = "CanActivateSystemUsers";
        public const string CanDeactivateSystemUsers = "CanDeactivateSystemUsers";

        //Sector Users
        public const string IsSectorUser = "IsSectorUser";
        public const string CanCreateSectorUsers = "CanCreateSectorUsers";
        public const string CanViewSectorUsers = "CanViewSectorUsers";
        public const string CanUpdateSectorUsers = "CanUpdateSectorUsers";
        public const string CanDeleteSectorUsers = "CanDeleteSectorUsers";
        public const string CanActivateSectorUsers = "CanActivateSectorUsers";
        public const string CanDeactivateSectorUsers = "CanDeactivateSectorUsers";

        //Regional Users
        public const string IsRegionalUser = "IsRegionalUser";
        public const string CanCreateRegionalUsers = "CanCreateRegionalUsers";
        public const string CanViewRegionalUsers = "CanViewRegionalUsers";
        public const string CanUpdateRegionalUsers = "CanUpdateRegionalUsers";
        public const string CanDeleteRegionalUsers = "CanDeleteRegionalUsers";
        public const string CanActivateRegionalUsers = "CanActivateRegionalUsers";
        public const string CanDeactivateRegionalUsers = "CanDeactivateRegionalUsers";

        //District Users
        public const string IsDistrictUser = "IsDistrictUser";
        public const string CanCreateDistrictUsers = "CanCreateDistrictUsers";
        public const string CanViewDistrictUsers = "CanViewDistrictUsers";
        public const string CanUpdateDistrictUsers = "CanUpdateDistrictUsers";
        public const string CanDeleteDistrictUsers = "CanDeleteDistrictUsers";
        public const string CanActivateDistrictUsers = "CanActivateDistrictUsers";
        public const string CanDeactivateDistrictUsers = "CanDeactivateDistrictUsers";

        //Outpost Users
        public const string IsOutpostUser = "IsOutpostUser";
        public const string CanCreateOutpostUsers = "CanCreateOutpostUsers";
        public const string CanViewOutpostUsers = "CanViewOutpostUsers";
        public const string CanUpdateOutpostUsers = "CanUpdateOutpostUsers";
        public const string CanDeleteOutpostUsers = "CanDeleteOutpostUsers";
        public const string CanActivateOutpostUsers = "CanActivateOutpostUsers";
        public const string CanDeactivateOutpostUsers = "CanDeactivateOutpostUsers";

        //Executives
        public const string CanCreateExecutives = "CanCreateExecutives";
        public const string CanViewExecutives = "CanViewExecutives";
        public const string CanUpdateExecutives = "CanUpdateExecutives";
        public const string CanDeleteExecutives = "CanDeleteExecutives";

        //Rangers
        public const string CanCreateRangers = "CanCreateRangers";
        public const string CanViewRangers = "CanViewRangers";
        public const string CanUpdateRangers = "CanUpdateRangers";
        public const string CanDeleteRangers = "CanDeleteRangers";
        public const string CanTransferRangers = "CanTransferRangers";
        public const string CanCancelRangerTransfer = "CanCancelRangerTransfer";
        public const string CanAcceptRangerTransfer = "CanAcceptRangerTransfer";
        public const string CanApproveRangerTransfer = "CanApproveRangerTransfer";
        public const string CanRejectRangerTransfer = "CanRejectRangerTransfer";

        //Material
        public const string CanCreateOrUpdateMaterials = "CanCreateOrUpdateMaterials";
        public const string CanRemoveMaterials = "CanRemoveMaterials";

        //Settings
        public const string CanCreateSettings = "CanCreateSettings";
        public const string CanViewSettings = "CanViewSettings";
        public const string CanUpdateSettings = "CanUpdateSettings";
        public const string CanDeleteSettings = "CanDeleteSettings";


        

    }

    public class GenericProperties
    {
        public const string Administrator = "Administrator";
        public const string Privilege = "Privilege";
        public const string CreatedBy = "CreatedBy";
        public const string CreatedAt = "CreatedAt";
        public const string ModifiedAt = "ModifiedAt";
        public const string ModifiedBy = "ModifiedBy";
        public const string Locked = "Locked";
        public const string IsDeleted = "IsDeleted";
    }

    public class ExceptionMessage
    {
        public const string RecordLocked = "Record is locked and can't be deleted.";
        public const string NotFound = "Record not found.";
    }

    public class ConfigKeys
    {
        public static string EmailAccountName = "EmailAccountName";
        public static string EmailApiKey = "EmailApiKey";
        public static string EmailSender = "EmailSender";
        public static string SmsApiKey = "SmsApiKey";
        public static string SmsSender = "SmsSender";
        public static string AppTitle = "AppTitle";
        public static string Logo = "Logo";
        public static string KeepAlive = "KeepAlive";
    }

    public class ResultObj
    {
        public long Total { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
