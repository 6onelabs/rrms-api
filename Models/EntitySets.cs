﻿using Microsoft.AspNetCore.Identity;
using RoyalRangersAPI.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RoyalRangersAPI.Models
{
    public class HasId
    {
        public long Id { get; set; }
    }

    public class AuditFields : HasId
    {
        //[Required]
        public string CreatedBy { get; set; }
        //[Required]
        public string ModifiedBy { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow.ToUniversalTime();
        public DateTime ModifiedAt { get; set; } = DateTime.UtcNow.ToUniversalTime();
        public bool Locked { get; set; } = false;
        public bool Hidden { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
    }

    public class LookUp : AuditFields
    {
        [MaxLength(512), Required]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
    }

    public class Role : IdentityRole
    {
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow.ToUniversalTime();
        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow.ToUniversalTime();
        public bool IsDeleted { get; set; } = false;
        public string Notes { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<RoleClaim> RoleClaims { get; set; }
        [NotMapped]
        public List<string> Claims { get; set; }
    }

    public class RoleClaim : IdentityRoleClaim<string>
    {
        public virtual Role Role { get; set; }
    }

    public class User : IdentityUser
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow.ToUniversalTime();
        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow.ToUniversalTime();
        public bool IsDeleted { get; set; } = false;
        public bool Locked { get; set; } = true;
        public bool Hidden { get; set; } = false;
        public virtual ICollection<UserClaim> Claims { get; set; }
        public virtual ICollection<UserLogin> Logins { get; set; }
        public virtual ICollection<UserToken> Tokens { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        [NotMapped]
        public string Password { get; set; }
        [NotMapped]
        public string Role { get; set; }
        public virtual Sector Sector { get; set; }
        public long? SectorId { get; set; }
        public virtual Region Region { get; set; }
        public long? RegionId { get; set; }
        public virtual District District { get; set; }
        public long? DistrictId { get; set; }
        public virtual Outpost Outpost { get; set; }
        public long? OutpostId { get; set; }
        public UserType Type { get; set; } = UserType.System;
    }

    public enum UserType
    {
        System,
        Sector,
        Regional,
        District,
        Outpost
    }

    public class UserClaim : IdentityUserClaim<string>
    {
        public virtual User User { get; set; }
    }

    public class UserLogin : IdentityUserLogin<string>
    {
        public virtual User User { get; set; }
    }

    public class UserRole : IdentityUserRole<string>
    {
        public virtual User User { get; set; }
        public virtual Role Role { get; set; }
    }

    public class UserToken : IdentityUserToken<string>
    {
        public virtual User User { get; set; }
    }

    public class ResetRequest : HasId
    {
        public string Ip { get; set; } = "127.0.0.1";
        public DateTime Date { get; set; } = DateTime.UtcNow;
        public string Email { get; set; }
        public string Token { get; set; }
        public bool IsActive { get; set; } = false;
    }

    public class ResetModel
    {
        public string Token { get; set; }
        public string Password { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }
    public class Message : HasId
    {
        [MaxLength(256), Required]
        public string Recipient { get; set; }
        [MaxLength(256)]
        public string Name { get; set; }
        [MaxLength(128)]
        public string Subject { get; set; }
        [Required]
        public string Text { get; set; }
        public MessageStatus Status { get; set; }
        public MessageType Type { get; set; }
        [MaxLength(5000)]
        public string Response { get; set; }
        public DateTime TimeStamp { get; set; }
        [NotMapped]
        public string Attachment { get; set; }
    }

    public enum MessageType
    {
        Sms,
        Email
    }

    public enum MessageStatus
    {
        Pending,
        Sent,
        Received,
        Failed
    }

    public enum RangerTransferStatus
    {
        Pending,
        Approved,
        Accepted,
        Rejected,
        Cancelled
    }

    public enum OutpostStatus
    {
        Active,
        InActive,
        Chartered,
        NotChartered
    }

    public class Sector : LookUp
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public virtual List<Region> Regions { get; set; }
    }

    public class Rank : LookUp
    {
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
    }

    public class Region : LookUp
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public virtual Sector Sector { get; set; }
        public long SectorId { get; set; }
        public virtual List<District> Districts { get; set; }
        public virtual List<RegionMaterial> Materials { get; set; }
        [NotMapped]
        public List<long> MaterialIds { get; set; }
    }

    public class District : LookUp
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public virtual Region Region { get; set; }
        public long RegionId { get; set; }
        public virtual List<Outpost> Outposts { get; set; }
        public virtual List<DistrictMaterial> Materials { get; set; }
        [NotMapped]
        public List<long> MaterialIds { get; set; }
    }

    public class Outpost : AuditFields
    {
        [MaxLength(32)]
        public string Code { get; set; } = StringGenerators.GenerateRandomNumber(4);
        [MaxLength(256), Required]
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public long DistrictId { get; set; }
        public virtual District District { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
        public OutpostStatus Status { get; set; } = OutpostStatus.NotChartered;
        public virtual List<Ranger> Rangers { get; set; }
        //public List<RangerTransfer> Transfers { get; set; }
        public virtual List<OutpostMaterial> Materials { get; set; }
        [NotMapped]
        public List<long> MaterialIds { get; set; }
        public string ChurchAddress { get; set; }
        public string ChurchPhoneNumber { get; set; }
        public DateTime? StartDate { get; set; }
    }

    public class Ranger : AuditFields
    {
        public long OutpostId { get; set; }
        public virtual Outpost Outpost { get; set; }
        public string Photo { get; set; }
        [MaxLength(32), Required]
        public string Code { get; set; } = StringGenerators.GenerateRandomNumber(5);
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        [NotMapped]
        public int Age => DateHelpers.GetAge(DateOfBirth);
        public Gender Gender { get; set; }
        public string ResidentialAddress { get; set; }
        public string PostalAddress { get; set; }
        public string SchoolOrWork { get; set; }
        public string Occupation { get; set; }
        public string Hobbies { get; set; }
        public string Allergies { get; set; }
        public string MedicalHistory { get; set; }
        public string GhanaPostGps { get; set; }
        public string FatherName { get; set; }
        public string FatherPhoneNumber { get; set; }
        public string FatherResidentialAddress { get; set; }
        public string FatherPostalAddress { get; set; }
        public string FatherGhanaPostGps { get; set; }
        public string MotherName { get; set; }
        public string MotherPhoneNumber { get; set; }
        public string MotherResidentialAddress { get; set; }
        public string MotherPostalAddress { get; set; }
        public string MotherGhanaPostGps { get; set; }
        public virtual List<RangerTransfer> Transfers { get; set; }
        public virtual List<Sibling> Siblings { get; set; }
        public RangerStatus Status { get; set; } = RangerStatus.Active;
        public string Uniforms { get; set; }
        public string UniformIds { get; set; }
        public virtual List<RangerAward> Awards { get; set; }
        public bool IsCommander { get; set; } = false;
    }

    public enum RangerStatus
    {
        Active,
        InActive
    }

    public class Sibling : HasId
    {
        public long RangerId { get; set; }
        public virtual Ranger Ranger { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        [NotMapped]
        public int Age => DateHelpers.GetAge(DateOfBirth??DateTime.UtcNow);
    }

    public class RangerTransfer : AuditFields
    {
        public long RangerId { get; set; }
        public virtual Ranger Ranger { get; set; }
        public long SourceId { get; set; }
        public virtual Outpost Source { get; set; }
        public RangerTransferStatus Status { get; set; } = RangerTransferStatus.Pending;
        public long DestinationId { get; set; }
        public virtual Outpost Destination { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
        public string VettedBy { get; set; }
        public string VettingNotes { get; set; }
        public DateTime? VettedOn { get; set; }
        public string AcceptedBy { get; set; }
        public DateTime? AcceptedOn { get; set; }
    }

    public class RangerTransferDTO
    {
        public long RangerId { get; set; }
        public long SourceId { get; set; }
        public long DestinationId { get; set; }
    }

    public class RangerTransferVettingDTO
    {
        public long Id { get; set; }
        public string Notes { get; set; }
    }

    public class RangerAward : AuditFields
    {
        public long RangerId { get; set; }
        public virtual Ranger Ranger { get; set; }
        public DateTime Date { get; set; }
        public RangerAwardType Type { get; set; }
        public long? AwardId { get; set; }
        public virtual Award Award { get; set; }
        public long? AdvancementId { get; set; }
        public virtual Advancement Advancement { get; set; }
        public long? AchievementId { get; set; }
        public virtual Achievement Achievement { get; set; }
        public long? TrainingId { get; set; }
        public virtual Training Training { get; set; }
    }

    public class RangerAwardDTO
    {
        public long RangerId { get; set; }
        public DateTime Date { get; set; }
        public RangerAwardType Type { get; set; }
        public long AwardId { get; set; }
        public long AdvancementId { get; set; }
        public long AchievementId { get; set; }
        public long TrainingId { get; set; }
    }

    public enum RangerAwardType
    {
        Award,
        Advancement,
        Achievement,
        Training
    }

    public class Executive : AuditFields
    {
        public int Order { get; set; } = 0;
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Church { get; set; }
        public long PositionId { get; set; }
        public virtual ExecutivePosition Position { get; set; }
        public long? OutpostId { get; set; }
        public virtual Outpost Outpost { get; set; }
        public long? DistrictId { get; set; }
        public virtual District District { get; set; }
        public long? RegionId { get; set; }
        public virtual Region Region { get; set; }
        public long? SectorId { get; set; }
        public virtual Sector Sector { get; set; }
        public ExecutiveStatus Status { get; set; }
    }

    public enum ExecutiveStatus
    {
        Active,
        InActive
    }

    public class ExecutivePosition : LookUp
    {
        public ExecutiveType Type { get; set; }
    }

    public enum ExecutiveType
    {
        National,
        Sector,
        Regional,
        District,
        Outpost
    }

    public class Programme : AuditFields
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string File { get; set; }
        public string Title { get; set; }
        public string Notes { get; set; }
        public string Email { get; set; }
        public long? OutpostId { get; set; }
        public virtual Outpost Outpost { get; set; }
        public long? DistrictId { get; set; }
        public virtual District District { get; set; }
        public long? RegionId { get; set; }
        public virtual Region Region { get; set; }
        public long? SectorId { get; set; }
        public virtual Sector Sector { get; set; }
        public ProgrammeType Type { get; set; }
    }

    public enum ProgrammeType
    {
        National,
        Sector,
        Regional,
        District,
        Outpost
    }
    public class AdvancementCategory : LookUp
    {
    }
    public class Advancement : LookUp
    {        public long? CategoryId { get; set; }
        public virtual AdvancementCategory Category { get; set; }
    }
    public class AwardCategory : LookUp
    {
    }
    public class Award : LookUp
    {
        public long? CategoryId { get; set; }
        public virtual AwardCategory Category { get; set; }
    }

    public class Achievement : LookUp
    {
    }

    public class Material : LookUp
    {
    }
    public class TrainingCategory : LookUp
    {
    }
    public class Training : LookUp
    {
        public long? CategoryId { get; set; }
        public virtual TrainingCategory Category { get; set; }
    }

    public class Uniform : LookUp
    {
    }

    public class OutpostMaterial : AuditFields
    {
        public long MaterialId { get; set; }
        public virtual Material Material { get; set; }
        public long OutpostId { get; set; }
        public virtual Outpost Outpost { get; set; }
    }

    public class OutpostMaterialDTO
    {
        public List<long> MaterialId { get; set; }
        public long OutpostId { get; set; }
    }

    public class DistrictMaterial : AuditFields
    {
        public long MaterialId { get; set; }
        public virtual Material Material { get; set; }
        public long DistrictId { get; set; }
        public virtual District District { get; set; }
    }

    public class DistrictMaterialDTO
    {
        public List<long> MaterialId { get; set; }
        public long DistrictId { get; set; }
    }

    public class RegionMaterial : AuditFields
    {
        public long MaterialId { get; set; }
        public virtual Material Material { get; set; }
        public long RegionId { get; set; }
        public virtual Region Region { get; set; }
    }

    public class RegionMaterialDTO
    {
        public List<long> MaterialId { get; set; }
        public long RegionId { get; set; }
    }

    public class RangerUniformDTO
    {
        public List<long> UniformId { get; set; }
        public long RangerId { get; set; }
    }

    public class SectorMaterial : AuditFields
    {
        public long MaterialId { get; set; }
        public virtual Material Material { get; set; }
        public long SectorId { get; set; }
        public virtual Sector Sector { get; set; }
    }

    public class SectorMaterialDTO
    {
        public List<long> MaterialId { get; set; }
        public long SectorId { get; set; }
    }

    public class NationalMaterial : AuditFields
    {
        public long MaterialId { get; set; }
        public virtual Material Material { get; set; }
    }

    public class NationalMaterialDTO
    {
        public List<long> MaterialId { get; set; }
    }

    public class Chartering : AuditFields
    {
        public long OutpostId { get; set; }
        public virtual Outpost Outpost { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Fee { get; set; }
        public long CommanderId { get; set; }
        public virtual Executive Commander { get; set; }
        public long PastorId { get; set; }
        public virtual Executive Pastor { get; set; }
        public long MmPresidentId { get; set; }
        public virtual Executive MmPresident { get; set; }
        public int TotalRangers { get; set; }
        public int NumberOfRagerKids { get; set; }
        public int NumberOfDiscoveryRangers { get; set; }
        public int NumberOfAdventureRangers { get; set; }
        public int NumberOfExpeditionRangers { get; set; }
        public int NumberOfChallengerRangers { get; set; }
        public int NumberOfCommanders { get; set; }
        public string VettedBy { get; set; }
        public DateTime VettedOn { get; set; }
        public CharteringStatus Status { get; set; } = CharteringStatus.Pending;
    }

    public class CharteringDTO
    {
        public long CommanderId { get; set; }
        public long PastorId { get; set; }
        public long MmPresidentId { get; set; }
    }

    public enum CharteringStatus
    {
        Pending,
        Approved,
        Rejected,
        Cancelled,
        Expired
    }

    public class Camp : AuditFields
    {
        [MaxLength(32), Required]
        public string Code { get; set; } = StringGenerators.GenerateCode(32);
        [MaxLength(256), Required]
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Location { get; set; }
        public string Theme { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; } = false;
        public double Fee { get; set; }
        public double MinRegAmount { get; set; }
        public bool HasClasses { get; set; }
        public int MaxClassSize { get; set; }
        public bool HasTables { get; set; }
        public int MaxTableSize { get; set; }
        public bool HasRooms { get; set; }
        public int MaxRoomSize { get; set; }
        public bool HasPatrols { get; set; }
        public int MaxNumberOfPatrols { get; set; }
        public CampType Type { get; set; } = CampType.National;
        public long? DistrictId { get; set; }
        public virtual District District { get; set; }
        public long? RegionId { get; set; }
        public virtual Region Region { get; set; }
        public long? SectorId { get; set; }
        public virtual Sector Sector { get; set; }
    }

    public enum CampType
    {
        National,
        Sector,
        Regional,
        District
    }

    public class CampPricingSchedule : AuditFields
    {
        public long CampId { get; set; }
        public virtual Camp Camp { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double AmountPayable { get; set; }
    }

    public class CampMember : AuditFields
    {
        public long CampId { get; set; }
        public virtual Camp Camp { get; set; }
        public long RangerId { get; set; }
        public virtual Ranger Ranger { get; set; }
        public string Rank { get; set; }
        public CampMemberStatus Status { get; set; }
        public double AmountPaid { get; set; }
        public double Balance { get; set; }
        public double Discount { get; set; }
    }
    public enum CampMemberStatus
    {
        PartPayment,
        FullPayment,
        Cancelled,
        Finalized
    }
    public class CampMemberModel
    {
        public long CampId { get; set; }
        public long RangerId { get; set; }
        public double AmountPaid { get; set; }
        public bool IsCommander { get; set; }
    }

    public class CampMemberReplacementModel
    {
        public long CampMemberId { get; set; }
        public long RangerId { get; set; }
        public bool IsCommander { get; set; }
    }

    public class CampMemberRankModel
    {
        public long CampMemberId { get; set; }
        public long RankId { get; set; }
    }

    public class UpdatePaymentModel
    {
        public long CampMemberId { get; set; }
        public double Amount { get; set; }
    }
    public class CampMemberPayment : AuditFields
    {
        public long CampMemberId { get; set; }
        public virtual CampMember CampMember { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public double Balance { get; set; }
    }
    public class CampClass : AuditFields
    {
        public long CampId { get; set; }
        public virtual Camp Camp { get; set; }
        [MaxLength(64), Required]
        public string Code { get; set; }
        public int Count { get; set; } = 0;
        public int Order { get; set; }
    }

    public class CampMemberClass : HasId
    {
        public long MemberId { get; set; }
        public virtual CampMember Member { get; set; }
        public long CampClassId { get; set; }
        public virtual CampClass CampClass { get; set; }
    }

    public class CampTable : AuditFields
    {
        public long CampId { get; set; }
        public virtual Camp Camp { get; set; }
        [MaxLength(32), Required]
        public string Code { get; set; }
        public int Count { get; set; } = 0;
        public int Order { get; set; }
    }

    public class CampMemberTable : HasId
    {
        public long MemberId { get; set; }
        public virtual CampMember Member { get; set; }
        public long CampTableId { get; set; }
        public virtual CampTable CampTable { get; set; }
    }

    public class CampRoom : AuditFields
    {
        public long CampId { get; set; }
        public virtual Camp Camp { get; set; }
        [MaxLength(64), Required]
        public string Code { get; set; }
        public int Count { get; set; } = 0;
        public int Order { get; set; }

    }

    public class CampPatrol : AuditFields
    {
        public long CampId { get; set; }
        public virtual Camp Camp { get; set; }
        [MaxLength(64), Required]
        public string Code { get; set; }
        public int Count { get; set; } = 0;
        public int Order { get; set; }

    }

    public class CampMemberPatrol : HasId
    {
        public long MemberId { get; set; }
        public virtual CampMember Member { get; set; }
        public long CampPatrolId { get; set; }
        public virtual CampPatrol CampPatrol { get; set; }
    }

    public class CampRoomModel
    {
        public long Id { get; set; }
        public string Code { get; set; }
    }

    public class CampTableModel
    {
        public long Id { get; set; }
        public string Code { get; set; }
    }

    public class CampClassModel
    {
        public long Id { get; set; }
        public string Code { get; set; }
    }

    public class CampMemberRoom : HasId
    {
        public long MemberId { get; set; }
        public virtual CampMember Member { get; set; }
        public long CampRoomId { get; set; }
        public virtual CampRoom CampRoom { get; set; }
    }

    public class ChangeCampMemberRoom
    {
        public long MemberId { get; set; }
        public virtual CampMember Member { get; set; }
        public long CampRoomId { get; set; }
        public virtual CampRoom CampRoom { get; set; }
        public long NewRoomId { get; set; }
        public virtual CampRoom NewRoom { get; set; }
    }

    public class ChangeCampMemberTable
    {
        public long MemberId { get; set; }
        public virtual CampMember Member { get; set; }
        public long CampTableId { get; set; }
        public virtual CampRoom CampTable { get; set; }
        public long NewTableId { get; set; }
        public virtual CampRoom NewTable { get; set; }
    }

    public class ChangeCampMemberClass
    {
        public long MemberId { get; set; }
        public virtual CampMember Member { get; set; }
        public long CampClassId { get; set; }
        public virtual CampRoom CampClass { get; set; }
        public long NewClassId { get; set; }
        public virtual CampRoom NewClass { get; set; }
    }

    public enum ReconciliationType
    {
        Rooms,
        Classes,
        Tables,
        Patrols
    }
    public enum ReconciliationState
    {
        Full,
        Partial
    }
    public enum ReconciliationStatus
    {
        Pending,
        Completed
    }
    public class Reconciliation : AuditFields
    {
        public ReconciliationType Type { get; set; }
        public long CampId { get; set; }
        public virtual Camp Camp { get; set; }
        public ReconciliationStatus Status { get; set; } = ReconciliationStatus.Pending;
        public virtual List<ReconciliationDetails> Details { get; set; }
    }

    public class ReconciliationDetails : HasId
    {
        public long CampMemberId { get; set; }
        public virtual CampMember CampMember { get; set; }
        public long ReconciliationId { get; set; }
        public virtual Reconciliation Reconciliation { get; set; }
    }

    public enum TopupTransactionStatus
    {
        Pending,
        Sent,
        Processed,
        Failed,
        Cancelled
    }

    public enum TopupRequestProvider
    {
        Mtn,
        Vodafone,
        Tigo,
        Airtel
    }

    public enum BalanceDebitType
    {
        Chartering,
        CampRegistration,
        CampFeePayment,
        Refund
    }

    public class OutpostBalance : HasId
    {
        public long OutpostId { get; set; }
        public virtual Outpost Outpost { get; set; }
        [MaxLength(32), Required]
        public string Reference { get; set; } = StringGenerators.GenerateRandomNumber(16);
        public double Amount { get; set; } = 0;
    }

    public class TopupTransaction : AuditFields
    {
        public long OutpostBalanceId { get; set; }
        public virtual OutpostBalance OutpostBalance { get; set; }
        [MaxLength(32), Required]
        public string Reference { get; set; } = StringGenerators.GenerateRandomNumber(12);
        public double Amount { get; set; }
        public double Fee => Amount * 0.02;
        public string UserId { get; set; }
        public TopupRequestProvider Provider { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Token { get; set; }
        public TopupTransactionStatus Status { get; set; } = TopupTransactionStatus.Pending;
        public string ExternalTransactionId { get; set; }
        public string Response { get; set; }
    }

    public class TopupTransactionDTO
    {
        public double Amount { get; set; }
        public TopupRequestProvider Provider { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Token { get; set; }
    }


    public class ManualTopupTransaction : AuditFields
    {
        public long OutpostBalanceId { get; set; }
        public virtual OutpostBalance OutpostBalance { get; set; }
        [MaxLength(32), Required]
        public string Reference { get; set; } = StringGenerators.GenerateRandomNumber(16);
        public double Amount { get; set; }
        public double Fee { get; set; } = 0;
        public TopupTransactionStatus Status { get; set; } = TopupTransactionStatus.Processed;
        public string Notes { get; set; }
    }

    public class ManualTopupDTO
    {
        public long OutpostId { get; set; }
        public double Amount { get; set; }
        public string Notes { get; set; }
    }

    public class BalanceDebit : AuditFields
    {
        public long OutpostBalanceId { get; set; }
        public virtual OutpostBalance OutpostBalance { get; set; }
        [MaxLength(32), Required]
        public string Reference { get; set; } = StringGenerators.GenerateRandomNumber(16);
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public BalanceDebitType Type { get; set; }
    }

    public class RefundModel
    {
        public long OutpostId { get; set; }
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
    }

    public class BalanceStatement
    {
        public string Reference { get; set; } = StringGenerators.GenerateRandomNumber(16);
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public string Type { get; set; }
        public string Outpost { get; set; }
        public string CreatedBy { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
    }

    public class RangerSummaryModel
    {
        public string Rank { get; set; }
        public Gender Gender { get; set; }
    }

    public class RanksSummaryModel
    {
        public string Rank { get; set; }
        public long Count { get; set; }
    }
}
