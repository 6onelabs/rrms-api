﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace RoyalRangersAPI.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetService<UserManager<User>>();
            var roleManager = serviceProvider.GetService<RoleManager<Role>>();
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {

                #region Roles
                var roles = new List<string>
                        {
                            Privileges.CanViewDashboard,
                            Privileges.CanViewAdministration,
                            Privileges.CanViewChartering,
                            Privileges.CanViewMembership,
                            Privileges.CanViewOutpostReports,
                            Privileges.CanViewDistrictReports,
                            Privileges.CanViewRegionalReports,
                            Privileges.CanViewSectorReports,
                            Privileges.CanViewSystemReports,
                            Privileges.CanViewRoles,
                            Privileges.CanCreateRoles,
                            Privileges.CanUpdateRoles,
                            Privileges.CanDeleteRoles,
                            Privileges.IsSystemUser,
                            Privileges.CanCreateSystemUsers,
                            Privileges.CanViewSystemUsers,
                            Privileges.CanUpdateSystemUsers,
                            Privileges.CanDeleteSystemUsers,
                            Privileges.CanDeactivateSystemUsers,
                            Privileges.CanActivateSystemUsers,
                            Privileges.IsSectorUser,
                            Privileges.CanCreateSectorUsers,
                            Privileges.CanViewSectorUsers,
                            Privileges.CanUpdateSectorUsers,
                            Privileges.CanDeleteSectorUsers,
                            Privileges.CanDeactivateSectorUsers,
                            Privileges.CanActivateSectorUsers,
                            Privileges.IsRegionalUser,
                            Privileges.CanCreateRegionalUsers,
                            Privileges.CanViewRegionalUsers,
                            Privileges.CanUpdateRegionalUsers,
                            Privileges.CanDeleteRegionalUsers,
                            Privileges.CanDeactivateRegionalUsers,
                            Privileges.CanActivateRegionalUsers,
                            Privileges.IsDistrictUser,
                            Privileges.CanCreateDistrictUsers,
                            Privileges.CanViewDistrictUsers,
                            Privileges.CanUpdateDistrictUsers,
                            Privileges.CanDeleteDistrictUsers,
                            Privileges.CanDeactivateDistrictUsers,
                            Privileges.CanActivateDistrictUsers,
                            Privileges.IsOutpostUser,
                            Privileges.CanCreateOutpostUsers,
                            Privileges.CanViewOutpostUsers,
                            Privileges.CanUpdateOutpostUsers,
                            Privileges.CanDeleteOutpostUsers,
                            Privileges.CanDeactivateOutpostUsers,
                            Privileges.CanActivateOutpostUsers,
                            Privileges.CanCreateExecutives,
                            Privileges.CanDeleteExecutives,
                            Privileges.CanUpdateExecutives,
                            Privileges.CanViewExecutives,
                            Privileges.CanCreateOrUpdateMaterials,
                            Privileges.CanRemoveMaterials,
                            Privileges.CanCreateRangers,
                            Privileges.CanDeleteRangers,
                            Privileges.CanRejectRangerTransfer,
                            Privileges.CanApproveRangerTransfer,
                            Privileges.CanCancelRangerTransfer,
                            Privileges.CanTransferRangers,
                            Privileges.CanUpdateRangers,
                            Privileges.CanViewRangers,
                            Privileges.CanAcceptRangerTransfer,
                            Privileges.CanCreateSettings,
                            Privileges.CanViewSettings,
                            Privileges.CanUpdateSettings,
                            Privileges.CanDeleteSettings
                };
                var adminRole = new Role { Name = "Administrator", NormalizedName = "Administrator" };
                var existingRole = context.Roles.FirstOrDefault(x => x.Name == adminRole.Name);
                if (existingRole == null)
                {
                    var res = roleManager.CreateAsync(adminRole);
                    if (res.Result.Succeeded)
                    {
                        roles.Distinct().ToList().ForEach(r => roleManager.AddClaimAsync(adminRole,
                        new Claim(GenericProperties.Privilege, r)).Wait());
                    }
                }
                else
                {
                    foreach(var r in roles)
                    {
                        var exst = context.RoleClaims.FirstOrDefault(x => x.RoleId == existingRole.Id && r == x.ClaimValue);
                        if(exst == null)
                        {
                            var newClaim = new RoleClaim { RoleId = existingRole.Id, ClaimValue = r, ClaimType = GenericProperties.Privilege };
                            //    roleManager.AddClaimAsync(existingRole,
                            //new Claim(GenericProperties.Privilege, r)).Wait();
                            context.RoleClaims.Add(newClaim);
                            context.SaveChanges();
                            var a = newClaim;
                        }
                    }
                }
                #endregion

                #region Users
                var adminUser = new User
                {
                     Name = "System Administrator",
                    UserName = "Admin",

                };
                var existingUser = userManager.FindByNameAsync("Admin").Result;
                //Admin User
                if (existingUser == null)
                {
                    var res = userManager.CreateAsync(adminUser, "Admin@app1");
                    if (res.Result.Succeeded)
                    {
                        var user = userManager.FindByNameAsync("Admin").Result;
                        userManager.AddToRoleAsync(user, adminRole.Name).Wait();
                    }
                }
                #endregion
                context.SaveChanges();
            }
        }

    }
}