﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace RoyalRangersAPI.Models
{
    public class ApplicationDbContext : IdentityDbContext<
        User, Role, string,
        UserClaim, UserRole, UserLogin,
        RoleClaim, UserToken>
    {
        //private readonly IConfiguration _configuration;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Sector> Sectors { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Outpost> Outposts { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<Ranger> Rangers { get; set; }
        public DbSet<Sibling> Siblings { get; set; }
        public DbSet<RangerTransfer> RangerTransfers { get; set; }
        public DbSet<ExecutivePosition> ExecutivePositions { get; set; }
        public DbSet<Executive> Executives { get; set; }
        public DbSet<Programme> Programmes { get; set; }
        public DbSet<Advancement> Advancements { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Uniform> Uniforms { get; set; }
        public DbSet<OutpostMaterial> OutpostMaterials { get; set; }
        public DbSet<DistrictMaterial> DistrictMaterials { get; set; }
        public DbSet<RegionMaterial> RegionMaterials { get; set; }
        public DbSet<RangerAward> RangerAwards { get; set; }
        public DbSet<SectorMaterial> SectorMaterials { get; set; }
        public DbSet<NationalMaterial> NationalMaterials { get; set; }
        public DbSet<TrainingCategory> TrainingCategories { get; set; }
        public DbSet<AwardCategory> AwardCategories { get; set; }
        public DbSet<AdvancementCategory> AdvancementCategories { get; set; }
        public DbSet<OutpostBalance> OutpostBalances { get; set; }
        public DbSet<TopupTransaction> TopupTransactions { get; set; }
        public DbSet<ManualTopupTransaction> ManualTopupTransactions { get; set; }
        public DbSet<BalanceDebit> BalanceDebits { get; set; }
        public DbSet<Chartering> Charterings { get; set; }
        public DbSet<Camp> Camps { get; set; }
        public DbSet<CampPricingSchedule> CampPricingSchedules { get; set; }
        public DbSet<CampClass> CampClasses { get; set; }
        public DbSet<CampMember> CampMembers { get; set; }
        public DbSet<CampMemberClass> CampMemberClasses { get; set; }
        public DbSet<CampMemberPayment> Payments { get; set; }
        public DbSet<CampTable> CampTables { get; set; }
        public DbSet<CampMemberTable> CampMemberTables { get; set; }
        public DbSet<CampRoom> CampRooms { get; set; }
        public DbSet<CampMemberRoom> CampMemberRooms { get; set; }
        public DbSet<CampPatrol> CampPatrols { get; set; }
        public DbSet<CampMemberPatrol> CampMemberPatrols { get; set; }
        public DbSet<Reconciliation> Reconciliations { get; set; }
        public DbSet<ReconciliationDetails> ReconciliationDetails { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<User>(b =>
            {
                // Each User can have many UserClaims
                b.HasMany(e => e.Claims)
                    .WithOne(e => e.User)
                    .HasForeignKey(uc => uc.UserId)
                    .IsRequired();

                // Each User can have many UserLogins
                b.HasMany(e => e.Logins)
                    .WithOne(e => e.User)
                    .HasForeignKey(ul => ul.UserId)
                    .IsRequired();

                // Each User can have many UserTokens
                b.HasMany(e => e.Tokens)
                    .WithOne(e => e.User)
                    .HasForeignKey(ut => ut.UserId)
                    .IsRequired();

                // Each User can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.User)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            builder.Entity<Role>(b =>
            {
                // Each Role can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.Role)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                // Each Role can have many associated RoleClaims
                b.HasMany(e => e.RoleClaims)
                    .WithOne(e => e.Role)
                    .HasForeignKey(rc => rc.RoleId)
                    .IsRequired();
            });

            builder.Entity<Ranger>()
            .HasIndex(b => b.Code)
            .IsUnique();

            builder.Entity<Region>()
            .HasIndex(b => b.Name)
            .IsUnique();

            builder.Entity<District>()
            .HasIndex(b => b.Name)
            .IsUnique();

            builder.Entity<Outpost>()
                .HasIndex(b => b.Code)
                .IsUnique();

            builder.Entity<Ranger>()
                .HasMany(c => c.Transfers)
                .WithOne(e => e.Ranger)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}