﻿using RoyalRangersAPI.Models;

namespace RoyalRangersAPI.Repositories
{
    public class OutpostRepository : BaseRepository<Outpost>
    {
        public OutpostRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
