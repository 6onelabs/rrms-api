﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace RoyalRangersAPI.Repositories
{
    public class RangerRepository : BaseRepository<Ranger>
    {
        public RangerRepository(ApplicationDbContext context) : base(context)
        {
        }

        public string GetUniforms(string ids)
        {
            if (string.IsNullOrEmpty(ids)) return "";
            var idList = ids.Split(',').ToList();
            var res = _context.Uniforms.Where(x => idList.Contains(x.Id.ToString())).Select(x => x.Name).ToList();
            return string.Join(",", res);
        }

        public List<RangerAward> GetAwards(long rangerId)
        {
            var res = _context.RangerAwards.Where(x => x.RangerId == rangerId).Include(x => x.Award).Include(x => x.Advancement).Include(x => x.Achievement).Include(x => x.Training).ToList();
            return res;
        }

        public string GetTitle(RangerAward obj)
        {
            switch (obj.Type)
            {
                case RangerAwardType.Award:
                    return obj.Award?.Name;
                case RangerAwardType.Achievement:
                    return obj.Achievement?.Name;
                case RangerAwardType.Advancement:
                    return obj.Advancement?.Name;
                case RangerAwardType.Training:
                    return obj.Training?.Name;
                default:
                    return string.Empty;
            }

        }

        public string GetRank(int age)
        {
            var rec = _context.Ranks.Where(x => age >= x.MinAge && age <= x.MaxAge).FirstOrDefault();
            if (rec == null) return "";
            else return rec.Name;
        }

        public List<RangerTransfer> GetTransfers(long rangerId)
        {
            var res = _context.RangerTransfers.Where(x => x.RangerId == rangerId).Include(x => x.Source).Include(x => x.Destination).ToList();
            return res;
        }
    }
}
