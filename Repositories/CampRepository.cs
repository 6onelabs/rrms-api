﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Helpers;
using RoyalRangersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RoyalRangersAPI.Repositories
{
    public class CampRepository : BaseRepository<Camp>
    {
        public CampRepository(ApplicationDbContext context) : base(context)
        {
        }

        public void Activate(long campId, CampType campType)
        {
            var rec = _context.Camps.FirstOrDefault(x => x.Id == campId && x.Type == campType);
            if (rec == null) throw new Exception("Please check the selected Camp");
            if(rec.EndDate.Date <= DateTime.Now.Date) throw new Exception("You can not activate this camp because it has already ended.");
            var recs = _context.Camps.Where(x => x.Id != campId && x.Type == campType).ToList();
            foreach(var r in recs)
            {
                r.IsActive = false;
            }
            _context.SaveChanges();
            
            rec.IsActive = true;
            _context.SaveChanges();
        }

        public void Deactivate(long campId, CampType campType)
        {
            var rec = _context.Camps.FirstOrDefault(x => x.Id == campId && x.Type == campType);
            if (rec == null) throw new Exception("Please check the selected Camp");
            rec.IsActive = false;
            SaveChanges();
        }

        public void UpdateOutpostCode(long outpostId)
        {
            var rec = _context.Outposts.Include(x => x.District.Region).Where(x => x.Id == outpostId).First();
            var reg = rec.District.Region.Name.Substring(0, 4).ToUpper();
            var id = $"{rec.Id:D5}";
            rec.Code = $"OTP/{reg}/{id}";
            SaveChanges();
        }

        public void UpdateCampCode(long campId)
        {
            var rec = _context.Camps.First(x => x.Id == campId);
            var id = $"{rec.Id:D5}";
            rec.Code = $"CAMP/{DateTime.Now.Year}/{id}";
            SaveChanges();
        }

        public Camp GetActiveCamp(CampType type, User user)
        {
            var recs = _context.Camps.Where(x => x.IsActive && x.Type == type);
            switch (type)
            {
                case CampType.Sector:
                    {

                        recs = recs.Where(x => x.SectorId == user.SectorId);
                        break;
                    }

                case CampType.Regional:
                    {

                        recs = recs.Where(x => x.RegionId == user.RegionId);
                        break;
                    }

                case CampType.District:
                    {

                        recs = recs.Where(x => x.DistrictId == user.DistrictId);
                        break;
                    }
            }
            return recs.FirstOrDefault();
        }

        public Camp GetCamp(long campId)
        {
            return _context.Camps.Where(x => x.Id == campId).FirstOrDefault();
        }

        public CampPricingSchedule GetPricingSchedule(DateTime date, long campId)
        {
            date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            return _context.CampPricingSchedules.FirstOrDefault(x => date >= x.StartDate && date <= x.EndDate && x.CampId == campId);
        }

        public Rank GetRank(int age)
        {
            return _context.Ranks.FirstOrDefault(x => age >= x.MinAge && age <= x.MaxAge);
        }

        public CampType GetCampType(string userType)
        {
            if (userType == UserType.Sector.ToString()) return CampType.Sector;
            else if (userType == UserType.Regional.ToString()) return CampType.Regional;
            else if (userType == UserType.District.ToString()) return CampType.District;
            else return CampType.National;

        }

        public void AddToCampClass(long campMemId, Rank rank, Camp camp)
        {
            var campClass =
                _context.CampClasses.Where(
                    x =>
                        x.Count < camp.MaxClassSize &&
                        x.Code.ToLower().Contains(rank.Name.ToLower())).OrderByDescending(x => x.Code).FirstOrDefault();
            if (campClass == null)
            {
                var order = 1;
                //add a new class
                var lastClass =
                    _context.CampClasses.Where(x => x.Code.ToLower().Contains(rank.Name.ToLower()))
                        .OrderByDescending(x => x.Code)
                        .FirstOrDefault();
                if (lastClass != null) order = lastClass.Order + 1;
                var newClass = new CampClass
                {
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedBy = "System",
                    ModifiedBy = "System",
                    Count = 0,
                    Order = order,
                    Code = $"{rank.Name} Class {order}",
                    CampId = camp.Id
                };

                _context.CampClasses.Add(newClass);
                _context.SaveChanges();
                campClass = newClass;
            }

            //add member to class
            var cClass = _context.CampClasses.First(x => x.Id == campClass.Id);
            cClass.Count = cClass.Count + 1;
            _context.SaveChanges();
            _context.CampMemberClasses.Add(new CampMemberClass
            {
                MemberId = campMemId,
                CampClassId = campClass.Id
            });
            _context.SaveChanges();
        }

        public void AddToCampTable(long campMemId, int age, Camp camp)
        {
            var campTable =
                _context.CampTables.Where(
                    x =>
                        x.Count < (camp.MaxTableSize - 1) &&
                        x.Code.ToLower().Contains(age.ToString())).OrderByDescending(x => x.Code).FirstOrDefault();
            if (campTable == null)
            {
                var order = 1;
                //add a new table
                var lastTable =
                    _context.CampTables.Where(x => x.Code.ToLower().Contains(age.ToString()))
                        .OrderByDescending(x => x.Code)
                        .FirstOrDefault();
                if (lastTable != null) order = lastTable.Order + 1;
                var newTable = new CampTable
                {
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedBy = "System",
                    ModifiedBy = "System",
                    Count = 0,
                    Order = order,
                    Code = $"{age} Table {order}",
                    CampId = camp.Id
                };

                _context.CampTables.Add(newTable);
                _context.SaveChanges();
                campTable = newTable;
            }

            //add member to table
            var table = _context.CampTables.First(x => x.Id == campTable.Id);
            table.Count = table.Count + 1;
            _context.SaveChanges();
            _context.CampMemberTables.Add(new CampMemberTable
            {
                MemberId = campMemId,
                CampTableId = campTable.Id
            });
            _context.SaveChanges();
        }

        public void AddToCampRoom(long campMemId, Camp camp)
        {
            var member = _context.CampMembers.Where(x => x.Id == campMemId).Include(x => x.Camp).Include(x => x.Ranger.Outpost.District).First();
            var campRoom =
                _context.CampRooms.Where(
                    x =>
                        x.Count < camp.MaxRoomSize &&
                        x.Code.ToLower().Contains(member.Ranger.Outpost.District.Name.ToLower())).OrderByDescending(x => x.Code).FirstOrDefault();
            if (campRoom == null)
            {
                var order = 1;
                //add a new room
                var lastRoom =
                    _context.CampRooms.Where(x => x.Code.ToLower().Contains(member.Ranger.Outpost.District.Name.ToLower()))
                        .OrderByDescending(x => x.Code)
                        .FirstOrDefault();
                if (lastRoom != null) order = lastRoom.Order + 1;
                var newRoom = new CampRoom
                {
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedBy = "System",
                    ModifiedBy = "System",
                    Count = 0,
                    Order = order,
                    Code = $"{member.Ranger.Outpost.District.Name} Room {order}",
                    CampId = camp.Id
                };

                _context.CampRooms.Add(newRoom);
                _context.SaveChanges();
                campRoom = newRoom;
            }

            //add member to room
            var room = _context.CampRooms.First(x => x.Id == campRoom.Id);
            room.Count = room.Count + 1;
            _context.SaveChanges();
            _context.CampMemberRooms.Add(new CampMemberRoom
            {
                MemberId = campMemId,
                CampRoomId = campRoom.Id
            });
            _context.SaveChanges();
        }

        public void RemoveFromCampClass(long campMemId, long campId)
        {
            var rec =
                _context.CampMemberClasses.Where(
                    x =>
                        x.MemberId == campMemId && x.CampClass.CampId == campId).Include(x => x.CampClass).FirstOrDefault();
            if (rec == null) return;

            var campClass = _context.CampClasses.FirstOrDefault(x => x.CampId == campId && x.Id == rec.CampClassId);
            if (campClass == null) return;
            campClass.Count = campClass.Count - 1;
            _context.CampMemberClasses.Remove(rec);
            _context.SaveChanges();
        }

        public void RemoveFromCampTable(long campMemId, long campId)
        {
            var rec =
                _context.CampMemberTables.Where(
                    x =>
                        x.MemberId == campMemId && x.CampTable.CampId == campId).Include(x => x.CampTable).FirstOrDefault();
            if (rec == null) return;

            var campTable = _context.CampTables.FirstOrDefault(x => x.CampId == campId && x.Id == rec.CampTableId);
            if (campTable == null) return;
            campTable.Count = campTable.Count - 1;
            _context.CampMemberTables.Remove(rec);
            _context.SaveChanges();
        }

        public void RemoveFromCampRoom(long campMemId, long campId)
        {
            var rec =
                _context.CampMemberRooms.Where(
                    x =>
                        x.MemberId == campMemId && x.CampRoom.CampId == campId).Include(x => x.CampRoom).FirstOrDefault();
            if (rec == null) return;

            var campRoom = _context.CampRooms.FirstOrDefault(x => x.CampId == campId && x.Id == rec.CampRoomId);
            if (campRoom == null) return;
            campRoom.Count = campRoom.Count - 1;
            _context.CampMemberRooms.Remove(rec);
            _context.SaveChanges();
        }

        public CampClass GetCampClass(long campMemberId)
        {
            var res = _context.CampMemberClasses.FirstOrDefault(x => x.MemberId == campMemberId)?.CampClass;

            return res;
        }

        public List<CampClass> GetCampClassx(long campMemberId)
        {
            var res = _context.CampMemberClasses.Where(x => x.MemberId == campMemberId).Select(x => x.CampClass).ToList();
            return res;
        }

        public CampRoom GetCampRoom(long campMemberId)
        {
            var res = _context.CampMemberRooms.FirstOrDefault(x => x.MemberId == campMemberId)?.CampRoom;
            return res;
        }

        public List<CampRoom> GetCampRoomx(long campMemberId)
        {
            var res = _context.CampMemberRooms.Where(x => x.MemberId == campMemberId).Select(x => x.CampRoom).ToList();
            return res;
        }

        public CampTable GetCampTable(long campMemberId)
        {
            var res = _context.CampMemberTables.FirstOrDefault(x => x.MemberId == campMemberId)?.CampTable;
            return res;
        }

        public List<CampTable> GetCampTablex(long campMemberId)
        {
            var res = _context.CampMemberTables.Where(x => x.MemberId == campMemberId).Select(x => x.CampTable).ToList();
            return res;
        }

        public CampPatrol GetCampPatrol(long campMemberId)
        {
            var res = _context.CampMemberPatrols.FirstOrDefault(x => x.MemberId == campMemberId)?.CampPatrol;
            return res;
        }

        public List<CampPatrol> GetCampPatrolx(long campMemberId)
        {
            var res = _context.CampMemberPatrols.Where(x => x.MemberId == campMemberId).Select(x => x.CampPatrol).ToList();
            return res;
        }

        public List<CampPricingSchedule> GetCampPricingSchedules(long campId)
        {
            var res = _context.CampPricingSchedules.Where(x => x.CampId == campId).ToList();
            return res;
        }

        public void ReconcileCampTables(long recId)
        {
            using (var db = _context)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //var campRepo = new CampsRepository();
                        var recon = db.Reconciliations.First(x => x.Id == recId);
                        var camp = db.Camps.First(x => x.Id == recon.CampId);

                        //remove details
                        var det =
                            db.ReconciliationDetails.Where(
                                x => x.Reconciliation.CampId == recon.CampId && x.Reconciliation.Type == recon.Type);
                        db.ReconciliationDetails.RemoveRange(det);

                        //delete all tables
                        var tableMembers = db.CampMemberTables.Where(x => x.CampTable.CampId == camp.Id);
                        db.CampMemberTables.RemoveRange(tableMembers);

                        var tables = db.CampTables.Where(x => x.CampId == camp.Id);
                        db.CampTables.RemoveRange(tables);
                        db.SaveChanges();

                        var members = db.CampMembers.Where(x => x.Status == CampMemberStatus.FullPayment)
                            .Include(x => x.Ranger.Outpost.District.Region)
                            .ToList().OrderBy(x => x.Ranger.Age).ToList();
                        var count = (members.Count() / camp.MaxTableSize) + 5;
                        for (var i = 0; i < count; i++)
                        {
                            var newTable = new CampTable
                            {
                                CreatedAt = DateTime.Now,
                                ModifiedAt = DateTime.Now,
                                CreatedBy = "System",
                                ModifiedBy = "System",
                                Count = 0,
                                Order = i,
                                Code = $"Camp Table {i}",
                                CampId = camp.Id
                            };
                            db.CampTables.Add(newTable);
                            db.SaveChanges();
                        }
                        foreach (var m in members)
                        {
                            var campTable =
                                db.CampTables
                                    .Where(x => x.Count < (camp.MaxTableSize)).OrderByDescending(x => x.Count).First();
                            db.CampMemberTables.Add(new CampMemberTable
                            {
                                MemberId = m.Id,
                                CampTableId = campTable.Id
                            });
                            db.SaveChanges();
                            db.ReconciliationDetails.Add(new ReconciliationDetails
                            {
                                CampMemberId = m.Id,
                                ReconciliationId = recon.Id
                            });
                            db.SaveChanges();
                            var cr = db.CampTables.First(x => x.Id == campTable.Id);
                            cr.Count = cr.Count + 1;
                            db.SaveChanges();
                        }
                        recon.Status = ReconciliationStatus.Completed;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        WebHelpers.ProcessException(ex);
                    }
                }
            }
        }

        public void ReconcileCampPatrols(long recId)
        {
            using (var db = _context)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //var campRepo = new CampsRepository();
                        var recon = db.Reconciliations.First(x => x.Id == recId);
                        var camp = db.Camps.First(x => x.Id == recon.CampId);

                        //remove details
                        var det =
                            db.ReconciliationDetails.Where(
                                x => x.Reconciliation.CampId == recon.CampId && x.Reconciliation.Type == recon.Type);
                        db.ReconciliationDetails.RemoveRange(det);

                        //delete all patrols
                        var patrolMembers = db.CampMemberPatrols.Where(x => x.CampPatrol.CampId == camp.Id);
                        db.CampMemberPatrols.RemoveRange(patrolMembers);

                        var patrols = db.CampPatrols.Where(x => x.CampId == camp.Id);
                        db.CampPatrols.RemoveRange(patrols);
                        db.SaveChanges();

                        for (var i = 0; i < camp.MaxNumberOfPatrols; i++)
                        {
                            db.CampPatrols.Add(new CampPatrol
                            {
                                CreatedAt = DateTime.Now,
                                ModifiedAt = DateTime.Now,
                                CreatedBy = "System",
                                ModifiedBy = "System",
                                Count = 0,
                                Order = i,
                                Code = $"Camp Patrol {i}",
                                CampId = camp.Id
                            });
                            db.SaveChanges();
                        }
                        db.SaveChanges();
                        var members = db.CampMembers.Where(x => x.Status == CampMemberStatus.FullPayment)
                            .Include(x => x.Ranger.Outpost.District.Region)
                            .ToList().OrderBy(x => x.Ranger.Age).ThenBy(x => x.Ranger.Gender).ThenBy(x => x.Ranger.Outpost.Name).ToList();
                        foreach (var m in members)
                        {
                            var index = members.IndexOf(m);
                            var orderId = index % (camp.MaxNumberOfPatrols);
                            var cp = db.CampPatrols.First(x => x.Order == orderId);
                            cp.Count = cp.Count + 1;
                            db.CampMemberPatrols.Add(new CampMemberPatrol
                            {
                                MemberId = m.Id,
                                CampPatrolId = cp.Id
                            });
                            db.SaveChanges();
                            db.ReconciliationDetails.Add(new ReconciliationDetails
                            {
                                CampMemberId = m.Id,
                                ReconciliationId = recon.Id
                            });
                            db.SaveChanges();
                        }
                        recon.Status = ReconciliationStatus.Completed;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        WebHelpers.ProcessException(ex);
                    }
                }
            }
        }

        public void ReconcileCampRooms(long recId)
        {
            using (var db = _context)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //var campRepo = new CampsRepository();
                        var recon = db.Reconciliations.First(x => x.Id == recId);
                        var camp = db.Camps.First(x => x.Id == recon.CampId);

                        //remove details
                        var det =
                            db.ReconciliationDetails.Where(
                                x => x.Reconciliation.CampId == recon.CampId && x.Reconciliation.Type == recon.Type);
                        db.ReconciliationDetails.RemoveRange(det);

                        //delete all rooms
                        var roomMembers = db.CampMemberRooms.Where(x => x.CampRoom.CampId == camp.Id);
                        db.CampMemberRooms.RemoveRange(roomMembers);

                        var rooms = db.CampRooms.Where(x => x.CampId == camp.Id);
                        db.CampRooms.RemoveRange(rooms);
                        db.SaveChanges();

                        var members = db.CampMembers.Where(x => x.Status == CampMemberStatus.FullPayment)
                            .Include(x => x.Ranger.Outpost.District.Region).OrderBy(x => x.Ranger.Outpost.District.RegionId).ThenBy(x => x.Ranger.Outpost.DistrictId).ThenBy(x => x.Ranger.OutpostId).ToList();
                        var guys = members.Where(x => x.Ranger.Gender == Gender.Male).ToList();
                        var gals = members.Where(x => x.Ranger.Gender == Gender.Female).ToList();
                        var numGuysRooms = (guys.Count / camp.MaxRoomSize) + 5;
                        var numGalsRooms = (gals.Count / camp.MaxRoomSize) + 5;
                        //create rooms
                        for (var i = 1; i <= numGuysRooms; i++)
                        {
                            var newRoom = new CampRoom
                            {
                                CreatedAt = DateTime.Now,
                                ModifiedAt = DateTime.Now,
                                CreatedBy = "System",
                                ModifiedBy = "System",
                                Count = 0,
                                Order = i,
                                Code = $"Boys Room {i}",
                                CampId = camp.Id
                            };
                            db.CampRooms.Add(newRoom);
                            db.SaveChanges();
                        }
                        for (var i = 1; i <= numGalsRooms; i++)
                        {
                            var newRoom = new CampRoom
                            {
                                CreatedAt = DateTime.Now,
                                ModifiedAt = DateTime.Now,
                                CreatedBy = "System",
                                ModifiedBy = "System",
                                Count = 0,
                                Order = i,
                                Code = $"Girls Room {i}",
                                CampId = camp.Id
                            };
                            db.CampRooms.Add(newRoom);
                            db.SaveChanges();
                        }
                        foreach (var m in guys)
                        {
                            var campRoom =
                                db.CampRooms.Where(
                                    x =>
                                        x.Count < (camp.MaxRoomSize) &&
                                        x.Code.ToLower().Contains("Boys Room"))
                                    .OrderBy(x => x.Code)
                                    .FirstOrDefault();

                            if (campRoom == null)
                            {
                                numGuysRooms++;
                                campRoom = new CampRoom
                                {
                                    CreatedAt = DateTime.Now,
                                    ModifiedAt = DateTime.Now,
                                    CreatedBy = "System",
                                    ModifiedBy = "System",
                                    Count = 0,
                                    Order = numGuysRooms,
                                    Code = $"Boys Room {numGuysRooms}",
                                    CampId = camp.Id
                                };

                                db.CampRooms.Add(campRoom);
                                db.SaveChanges();
                            }

                            db.CampMemberRooms.Add(new CampMemberRoom
                            {
                                MemberId = m.Id,
                                CampRoomId = campRoom.Id
                            });
                            db.SaveChanges();
                            db.ReconciliationDetails.Add(new ReconciliationDetails
                            {
                                CampMemberId = m.Id,
                                ReconciliationId = recon.Id
                            });
                            db.SaveChanges();

                            var cr = db.CampRooms.First(x => x.Id == campRoom.Id);
                            cr.Count = cr.Count + 1;
                            db.SaveChanges();

                        }
                        foreach (var m in gals)
                        {
                            var campRoom =
                                db.CampRooms.Where(
                                    x =>
                                        x.Count < (camp.MaxRoomSize) &&
                                        x.Code.ToLower().Contains("Girls Room"))
                                    .OrderBy(x => x.Code)
                                    .FirstOrDefault();

                            if (campRoom == null)
                            {
                                numGalsRooms++;
                                campRoom = new CampRoom
                                {
                                    CreatedAt = DateTime.Now,
                                    ModifiedAt = DateTime.Now,
                                    CreatedBy = "System",
                                    ModifiedBy = "System",
                                    Count = 0,
                                    Order = numGalsRooms,
                                    Code = $"Girls Room {numGalsRooms}",
                                    CampId = camp.Id
                                };

                                db.CampRooms.Add(campRoom);
                                db.SaveChanges();
                            }

                            db.CampMemberRooms.Add(new CampMemberRoom
                            {
                                MemberId = m.Id,
                                CampRoomId = campRoom.Id
                            });
                            db.SaveChanges();
                            db.ReconciliationDetails.Add(new ReconciliationDetails
                            {
                                CampMemberId = m.Id,
                                ReconciliationId = recon.Id
                            });
                            db.SaveChanges();

                            var cr = db.CampRooms.First(x => x.Id == campRoom.Id);
                            cr.Count = cr.Count + 1;
                            db.SaveChanges();

                        }
                        recon.Status = ReconciliationStatus.Completed;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        WebHelpers.ProcessException(ex);
                    }
                }
            }
        }
        public void ReconcileCampClasses(long recId)
        {
        }

    }
}
