﻿using Quartz;
using System;
using System.Threading.Tasks;

namespace RoyalRangersAPI.Services
{
    [DisallowConcurrentExecution]
    public class OutPostProcessService : IJob
    {
        private readonly IServiceProvider _serviceProvider;
        public OutPostProcessService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            await new OutpostProcessRepository(_serviceProvider).CreateOutpostBalances();
        }
    }
}
