﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RoyalRangersAPI.Models;
using Quartz;
using System;
using System.Threading.Tasks;

namespace RoyalRangersAPI.Services
{
    public class MessageProcessor
    {
        private readonly ApplicationDbContext _context;
        public MessageProcessor(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }
        public async Task Send()
        {
            try
            {

            }
            catch (Exception) { }
        }

    }

    [DisallowConcurrentExecution]
    public class MessageProcessService : IJob
    {
        private readonly IServiceProvider _serviceProvider;
        public MessageProcessService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            await new MessageProcessor(_serviceProvider).Send();
        }
    }
}
