﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RoyalRangersAPI.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RoyalRangersAPI.Services
{
    public class OutpostProcessRepository
    {
        private readonly ApplicationDbContext _context;
        public OutpostProcessRepository(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }

        public async Task CreateOutpostBalances()
        {
            var outpostIds = _context.OutpostBalances.Where(x => x.Id > 0).Select(x => x.OutpostId).ToList();
            var outposts = _context.Outposts.Where(x => !outpostIds.Contains(x.Id)).Select(x => x.Id).ToList();
            foreach(var o in outposts)
            {
                _context.Add(new OutpostBalance { OutpostId = o});
            }
            _context.SaveChanges();
        }

        public async Task UncharterOutposts()
        {
            var outpostIds = _context.OutpostBalances.Where(x => x.Id > 0).Select(x => x.OutpostId).ToList();
            var outposts = _context.Outposts.Where(x => !outpostIds.Contains(x.Id)).Select(x => x.Id).ToList();
            foreach (var o in outposts)
            {
                _context.Add(new OutpostBalance { OutpostId = o });
            }
            _context.SaveChanges();
        }
    }
}
