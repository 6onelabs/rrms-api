﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Quartz;
using RestSharp;
using RoyalRangersAPI.Models;
using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RoyalRangersAPI.Services
{
    [DisallowConcurrentExecution]
    public class TopUpProcessService : IJob
    {
        private readonly IServiceProvider _serviceProvider;
        public TopUpProcessService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            
        }
        public async Task Execute(IJobExecutionContext context)
        {
            await new TopUpProcessRepository(_serviceProvider).ProcessPayment();
        }
    }

    public class TopUpProcessRepository
    {
        private readonly ApplicationDbContext _context;
        public TopUpProcessRepository(IServiceProvider serviceProvider)
        {
            _context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
        }

        public async Task ProcessPayment()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string authorization = "joe5cef9f3c2d101:NWNiNzMwMjNmNTE0NDk0NjRhNzhlNTcxNjY4NTIyMzg=";
            var conversion = Encoding.UTF8.GetBytes(authorization);
            var encoded_authorization = Convert.ToBase64String(conversion);
            var merchantId = "TTM-00000457";
            try
            {
                var now = DateTime.Now;
                var topups = _context.TopupTransactions.Where(x => x.Status == TopupTransactionStatus.Pending).OrderBy(x => x.CreatedAt).Take(100).ToList();
                if (!topups.Any()) return;

                foreach (var t in topups)
                {
                    var topup = _context.TopupTransactions.First(x => x.Id == t.Id);
                    var user = _context.Users.First(x => x.Id == topup.UserId);
                    var number = topup.PhoneNumber;
                    number = number.Replace("-", "");
                    number = number.Replace(" ", "");
                    //strip the country code form it
                    if (!number.StartsWith("233") && !number.StartsWith("0"))
                    {
                        number = "233" + number;
                    }
                    if (number.StartsWith("00233"))
                    {
                        number = "233" + number.Substring(5);
                    }
                    if (number.StartsWith("+233")) number = number.Replace("+233", "233");
                    if (number.Length <= 10)
                    {
                        if (number.StartsWith("02"))
                        {
                            number = "2332" + number.Substring(2);
                        }
                        else if (number.StartsWith("05"))
                        {
                            number = "2335" + number.Substring(2);
                        }
                    }

                    var reqObj = new TheTellerPaymentRequest
                    {
                        processing_code = "000200",
                        merchant_id = merchantId,
                        amount = ((topup.Amount + topup.Fee) * 100).ToString("000000000000"),
                        rswitch = GetSwitch(topup.Provider.ToString().ToLower()),
                        transaction_id = topup.Reference,
                        subscriber_number = number,
                        voucher_code = topup.Token
                    };
                    var obj = JsonConvert.SerializeObject(reqObj);

                    var client = new RestClient("https://prod.theteller.net/v1.1/transaction/process");
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Cache-Control", "no-cache");
                    request.AddHeader("Authorization", "Basic " + encoded_authorization);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", obj, ParameterType.RequestBody);

                    var res = client.Execute(request);

                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        var response = JsonConvert.DeserializeObject<TheTellerPaymentResponse>(res.Content);
                        if (response.status.ToLower() == "approved")
                        {
                            topup.Status = TopupTransactionStatus.Processed;
                            topup.ExternalTransactionId = response.transaction_id;
                            topup.Response = response.reason;

                            var bal = _context.OutpostBalances.First(x => x.Id == topup.OutpostBalanceId);
                            bal.Amount = bal.Amount + topup.Amount;

                            var msg = new Message
                            {
                                Text =
                                        $"Hello {user.Name}, Your topup was successful. Topup Amount: GHS {topup.Amount}, Available Balance: GHS {bal.Amount}, TransactionId: {topup.ExternalTransactionId}, Reference: {topup.Reference}",
                                Subject = "Successful Topup",
                                Recipient = user.PhoneNumber,
                                TimeStamp = DateTime.Now
                            };
                            _context.Messages.Add(msg);
                            _context.SaveChanges();
                        }
                        else
                        {
                            topup.Status = TopupTransactionStatus.Failed;
                            var msg = new Message
                            {
                                Text =
                                   $"Hello {user.Name}, Your topup request was not authorized. Please ensure that you have sufficient funds in your wallet.",
                                Subject = "Successful Topup",
                                Recipient = user.PhoneNumber,
                                TimeStamp = DateTime.Now
                            };
                            _context.Messages.Add(msg);
                            _context.SaveChanges();
                        }
                        topup.Response = topup.Response + " \n" + response.code + "::" + response.reason;
                    }
                    else
                    {
                        var response = JsonConvert.DeserializeObject<TheTellerPaymentResponse>(res.Content);
                        topup.Status = TopupTransactionStatus.Failed;
                        var msg = new Message
                        {
                            Text =
                               $"Hello {user.Name}, Your topup request was not authorized. Please ensure that you have sufficient funds in your wallet.",
                            Subject = "Successful Topup",
                            Recipient = user.PhoneNumber,
                            TimeStamp = DateTime.Now
                        };
                        _context.Messages.Add(msg);
                        _context.SaveChanges();
                        topup.Response = topup.Response + " \n" + response.code + "::" + response.reason;
                    }
                    topup.ModifiedAt = DateTime.Now;
                    _context.SaveChanges();
                }
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                var ex = e;
            }
        }

        public string GetSwitch(string network)
        {
            switch (network.ToLower())
            {
                case "mtn":
                    return "MTN";
                case "vodafone":
                    return "VDF";
                case "airtel":
                    return "ATL";
                default:
                    return "TGO";
            }
        }
    }

    public class TheTellerPaymentRequest
    {
        public string merchant_id { get; set; }
        public string amount { get; set; }
        public string processing_code { get; set; }
        public string transaction_id { get; set; }
        public string desc { get; set; } = "Topup from Campogar";
        public string subscriber_number { get; set; }
        public string voucher_code { get; set; }
        [JsonProperty("r-switch")]
        public string rswitch { get; set; }
    }

    public class TheTellerPaymentResponse
    {
        public string status { get; set; }
        public string code { get; set; }
        public string transaction_id { get; set; }
        public string reason { get; set; }
    }
}
