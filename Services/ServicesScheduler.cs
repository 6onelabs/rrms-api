﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace RoyalRangersAPI.Services
{
    public class ServicesScheduler
    {
        public static async Task StartAsync(IServiceProvider serviceProvider)
        {
            try
            {
                // Grab the Scheduler instance from the Factory
                NameValueCollection props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" }
                };
                StdSchedulerFactory factory = new StdSchedulerFactory(props);
                IScheduler scheduler = await factory.GetScheduler();
                scheduler.JobFactory = new PortalJobFactory(serviceProvider);
                await scheduler.Start();

                var messageService = JobBuilder.Create<MessageProcessService>()
                    .WithIdentity("net_core_starter_job_1", "net_core_starter_group_1")
                    .Build();
                var msgTrigger = TriggerBuilder.Create()
                    .WithIdentity("net_core_starter_trigger_1", "net_core_starter_group_1")
                        .StartNow()
                        .WithSimpleSchedule(x => x
                            .WithIntervalInSeconds(5)
                            .RepeatForever())
                        .Build();
                await scheduler.ScheduleJob(messageService, msgTrigger);

                var outpostService = JobBuilder.Create<OutPostProcessService>()
                    .WithIdentity("net_core_starter_job_2", "net_core_starter_group_1")
                    .Build();
                var outpostTrigger = TriggerBuilder.Create()
                    .WithIdentity("net_core_starter_trigger_2", "net_core_starter_group_1")
                        .StartNow()
                        .WithSimpleSchedule(x => x
                            .WithIntervalInSeconds(20)
                            .RepeatForever())
                        .Build();
                await scheduler.ScheduleJob(outpostService, outpostTrigger);

                var topupService = JobBuilder.Create<TopUpProcessService>()
                    .WithIdentity("net_core_starter_job_4", "net_core_starter_group_1")
                    .Build();
                var topupTrigger = TriggerBuilder.Create()
                    .WithIdentity("net_core_starter_trigger_4", "net_core_starter_group_1")
                        .StartNow()
                        .WithSimpleSchedule(x => x
                            .WithIntervalInSeconds(2)
                            .RepeatForever())
                        .Build();
                await scheduler.ScheduleJob(topupService, topupTrigger);
            }
            catch (SchedulerException ex)
            {

            }
        }

    }
}
