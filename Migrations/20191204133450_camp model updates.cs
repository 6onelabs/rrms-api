﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RoyalRangersAPI.Migrations
{
    public partial class campmodelupdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "DistrictId",
                table: "Camps",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RegionId",
                table: "Camps",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SectorId",
                table: "Camps",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Camps",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Camps_DistrictId",
                table: "Camps",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Camps_RegionId",
                table: "Camps",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Camps_SectorId",
                table: "Camps",
                column: "SectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Camps_Districts_DistrictId",
                table: "Camps",
                column: "DistrictId",
                principalTable: "Districts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Camps_Regions_RegionId",
                table: "Camps",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Camps_Sectors_SectorId",
                table: "Camps",
                column: "SectorId",
                principalTable: "Sectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Camps_Districts_DistrictId",
                table: "Camps");

            migrationBuilder.DropForeignKey(
                name: "FK_Camps_Regions_RegionId",
                table: "Camps");

            migrationBuilder.DropForeignKey(
                name: "FK_Camps_Sectors_SectorId",
                table: "Camps");

            migrationBuilder.DropIndex(
                name: "IX_Camps_DistrictId",
                table: "Camps");

            migrationBuilder.DropIndex(
                name: "IX_Camps_RegionId",
                table: "Camps");

            migrationBuilder.DropIndex(
                name: "IX_Camps_SectorId",
                table: "Camps");

            migrationBuilder.DropColumn(
                name: "DistrictId",
                table: "Camps");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "Camps");

            migrationBuilder.DropColumn(
                name: "SectorId",
                table: "Camps");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Camps");
        }
    }
}
