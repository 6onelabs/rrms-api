﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RoyalRangersAPI.Migrations
{
    public partial class addedthecategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "CategoryId",
                table: "Trainings",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CategoryId",
                table: "Awards",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CategoryId",
                table: "Advancements",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AdvancementCategory",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 512, nullable: false),
                    Notes = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvancementCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AwardCategory",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 512, nullable: false),
                    Notes = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AwardCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TrainingCategory",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 512, nullable: false),
                    Notes = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingCategory", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Trainings_CategoryId",
                table: "Trainings",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Awards_CategoryId",
                table: "Awards",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Advancements_CategoryId",
                table: "Advancements",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Advancements_AdvancementCategory_CategoryId",
                table: "Advancements",
                column: "CategoryId",
                principalTable: "AdvancementCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Awards_AwardCategory_CategoryId",
                table: "Awards",
                column: "CategoryId",
                principalTable: "AwardCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trainings_TrainingCategory_CategoryId",
                table: "Trainings",
                column: "CategoryId",
                principalTable: "TrainingCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Advancements_AdvancementCategory_CategoryId",
                table: "Advancements");

            migrationBuilder.DropForeignKey(
                name: "FK_Awards_AwardCategory_CategoryId",
                table: "Awards");

            migrationBuilder.DropForeignKey(
                name: "FK_Trainings_TrainingCategory_CategoryId",
                table: "Trainings");

            migrationBuilder.DropTable(
                name: "AdvancementCategory");

            migrationBuilder.DropTable(
                name: "AwardCategory");

            migrationBuilder.DropTable(
                name: "TrainingCategory");

            migrationBuilder.DropIndex(
                name: "IX_Trainings_CategoryId",
                table: "Trainings");

            migrationBuilder.DropIndex(
                name: "IX_Awards_CategoryId",
                table: "Awards");

            migrationBuilder.DropIndex(
                name: "IX_Advancements_CategoryId",
                table: "Advancements");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Trainings");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Awards");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Advancements");
        }
    }
}
