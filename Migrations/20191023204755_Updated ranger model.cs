﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RoyalRangersAPI.Migrations
{
    public partial class Updatedrangermodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCommander",
                table: "Rangers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Occupation",
                table: "Rangers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Executives",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCommander",
                table: "Rangers");

            migrationBuilder.DropColumn(
                name: "Occupation",
                table: "Rangers");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Executives");
        }
    }
}
