﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RoyalRangersAPI.Migrations
{
    public partial class Addedthecampmodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Camps",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Code = table.Column<string>(maxLength: 32, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    Theme = table.Column<string>(nullable: true),
                    Logo = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Fee = table.Column<double>(nullable: false),
                    MinRegAmount = table.Column<double>(nullable: false),
                    HasClasses = table.Column<bool>(nullable: false),
                    MaxClassSize = table.Column<int>(nullable: false),
                    HasTables = table.Column<bool>(nullable: false),
                    MaxTableSize = table.Column<int>(nullable: false),
                    HasRooms = table.Column<bool>(nullable: false),
                    MaxRoomSize = table.Column<int>(nullable: false),
                    HasPatrols = table.Column<bool>(nullable: false),
                    MaxNumberOfPatrols = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Camps", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CampClasses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CampId = table.Column<long>(nullable: false),
                    Code = table.Column<string>(maxLength: 64, nullable: false),
                    Key = table.Column<string>(maxLength: 64, nullable: false),
                    Count = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampClasses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampClasses_Camps_CampId",
                        column: x => x.CampId,
                        principalTable: "Camps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampMembers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CampId = table.Column<long>(nullable: false),
                    RangerId = table.Column<long>(nullable: false),
                    Rank = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    AmountPaid = table.Column<double>(nullable: false),
                    Balance = table.Column<double>(nullable: false),
                    Discount = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampMembers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampMembers_Camps_CampId",
                        column: x => x.CampId,
                        principalTable: "Camps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampMembers_Rangers_RangerId",
                        column: x => x.RangerId,
                        principalTable: "Rangers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampPatrols",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CampId = table.Column<long>(nullable: false),
                    Code = table.Column<string>(maxLength: 64, nullable: false),
                    Key = table.Column<string>(maxLength: 64, nullable: false),
                    Count = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampPatrols", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampPatrols_Camps_CampId",
                        column: x => x.CampId,
                        principalTable: "Camps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampPricingSchedules",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CampId = table.Column<long>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    AmountPayable = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampPricingSchedules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampPricingSchedules_Camps_CampId",
                        column: x => x.CampId,
                        principalTable: "Camps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampRooms",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CampId = table.Column<long>(nullable: false),
                    Code = table.Column<string>(maxLength: 64, nullable: false),
                    Key = table.Column<string>(maxLength: 64, nullable: false),
                    Count = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampRooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampRooms_Camps_CampId",
                        column: x => x.CampId,
                        principalTable: "Camps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampTables",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CampId = table.Column<long>(nullable: false),
                    Code = table.Column<string>(maxLength: 32, nullable: false),
                    Key = table.Column<string>(maxLength: 64, nullable: false),
                    Count = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampTables", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampTables_Camps_CampId",
                        column: x => x.CampId,
                        principalTable: "Camps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reconciliations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    CampId = table.Column<long>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reconciliations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reconciliations_Camps_CampId",
                        column: x => x.CampId,
                        principalTable: "Camps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampMemberClasses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MemberId = table.Column<long>(nullable: false),
                    CampClassId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampMemberClasses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampMemberClasses_CampClasses_CampClassId",
                        column: x => x.CampClassId,
                        principalTable: "CampClasses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampMemberClasses_CampMembers_MemberId",
                        column: x => x.MemberId,
                        principalTable: "CampMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CampMemberId = table.Column<long>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Balance = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payments_CampMembers_CampMemberId",
                        column: x => x.CampMemberId,
                        principalTable: "CampMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampMemberPatrols",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MemberId = table.Column<long>(nullable: false),
                    CampPatrolId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampMemberPatrols", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampMemberPatrols_CampPatrols_CampPatrolId",
                        column: x => x.CampPatrolId,
                        principalTable: "CampPatrols",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampMemberPatrols_CampMembers_MemberId",
                        column: x => x.MemberId,
                        principalTable: "CampMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampMemberRooms",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MemberId = table.Column<long>(nullable: false),
                    CampRoomId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampMemberRooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampMemberRooms_CampRooms_CampRoomId",
                        column: x => x.CampRoomId,
                        principalTable: "CampRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampMemberRooms_CampMembers_MemberId",
                        column: x => x.MemberId,
                        principalTable: "CampMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampMemberTables",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MemberId = table.Column<long>(nullable: false),
                    CampTableId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampMemberTables", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampMemberTables_CampTables_CampTableId",
                        column: x => x.CampTableId,
                        principalTable: "CampTables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CampMemberTables_CampMembers_MemberId",
                        column: x => x.MemberId,
                        principalTable: "CampMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReconciliationDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CampMemberId = table.Column<long>(nullable: false),
                    ReconciliationId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReconciliationDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReconciliationDetails_CampMembers_CampMemberId",
                        column: x => x.CampMemberId,
                        principalTable: "CampMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReconciliationDetails_Reconciliations_ReconciliationId",
                        column: x => x.ReconciliationId,
                        principalTable: "Reconciliations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CampClasses_CampId",
                table: "CampClasses",
                column: "CampId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMemberClasses_CampClassId",
                table: "CampMemberClasses",
                column: "CampClassId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMemberClasses_MemberId",
                table: "CampMemberClasses",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMemberPatrols_CampPatrolId",
                table: "CampMemberPatrols",
                column: "CampPatrolId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMemberPatrols_MemberId",
                table: "CampMemberPatrols",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMemberRooms_CampRoomId",
                table: "CampMemberRooms",
                column: "CampRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMemberRooms_MemberId",
                table: "CampMemberRooms",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMembers_CampId",
                table: "CampMembers",
                column: "CampId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMembers_RangerId",
                table: "CampMembers",
                column: "RangerId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMemberTables_CampTableId",
                table: "CampMemberTables",
                column: "CampTableId");

            migrationBuilder.CreateIndex(
                name: "IX_CampMemberTables_MemberId",
                table: "CampMemberTables",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_CampPatrols_CampId",
                table: "CampPatrols",
                column: "CampId");

            migrationBuilder.CreateIndex(
                name: "IX_CampPricingSchedules_CampId",
                table: "CampPricingSchedules",
                column: "CampId");

            migrationBuilder.CreateIndex(
                name: "IX_CampRooms_CampId",
                table: "CampRooms",
                column: "CampId");

            migrationBuilder.CreateIndex(
                name: "IX_CampTables_CampId",
                table: "CampTables",
                column: "CampId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_CampMemberId",
                table: "Payments",
                column: "CampMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_ReconciliationDetails_CampMemberId",
                table: "ReconciliationDetails",
                column: "CampMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_ReconciliationDetails_ReconciliationId",
                table: "ReconciliationDetails",
                column: "ReconciliationId");

            migrationBuilder.CreateIndex(
                name: "IX_Reconciliations_CampId",
                table: "Reconciliations",
                column: "CampId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CampMemberClasses");

            migrationBuilder.DropTable(
                name: "CampMemberPatrols");

            migrationBuilder.DropTable(
                name: "CampMemberRooms");

            migrationBuilder.DropTable(
                name: "CampMemberTables");

            migrationBuilder.DropTable(
                name: "CampPricingSchedules");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "ReconciliationDetails");

            migrationBuilder.DropTable(
                name: "CampClasses");

            migrationBuilder.DropTable(
                name: "CampPatrols");

            migrationBuilder.DropTable(
                name: "CampRooms");

            migrationBuilder.DropTable(
                name: "CampTables");

            migrationBuilder.DropTable(
                name: "CampMembers");

            migrationBuilder.DropTable(
                name: "Reconciliations");

            migrationBuilder.DropTable(
                name: "Camps");
        }
    }
}
