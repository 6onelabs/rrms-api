﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RoyalRangersAPI.Migrations
{
    public partial class updatesonthecharteringmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommanderEmail",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "CommanderName",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "CommanderPhoneNumber",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "MmPresidentEmail",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "MmPresidentName",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "MmPresidentPhoneNumber",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "PastorEmail",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "PastorName",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "PastorPhoneNumber",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "Year",
                table: "Charterings");

            migrationBuilder.AddColumn<long>(
                name: "CommanderId",
                table: "Charterings",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "Charterings",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<double>(
                name: "Fee",
                table: "Charterings",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<long>(
                name: "MmPresidentId",
                table: "Charterings",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "NumberOfChallengerRangers",
                table: "Charterings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "PastorId",
                table: "Charterings",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "Charterings",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Charterings_CommanderId",
                table: "Charterings",
                column: "CommanderId");

            migrationBuilder.CreateIndex(
                name: "IX_Charterings_MmPresidentId",
                table: "Charterings",
                column: "MmPresidentId");

            migrationBuilder.CreateIndex(
                name: "IX_Charterings_PastorId",
                table: "Charterings",
                column: "PastorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Charterings_Executives_CommanderId",
                table: "Charterings",
                column: "CommanderId",
                principalTable: "Executives",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Charterings_Executives_MmPresidentId",
                table: "Charterings",
                column: "MmPresidentId",
                principalTable: "Executives",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Charterings_Executives_PastorId",
                table: "Charterings",
                column: "PastorId",
                principalTable: "Executives",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Charterings_Executives_CommanderId",
                table: "Charterings");

            migrationBuilder.DropForeignKey(
                name: "FK_Charterings_Executives_MmPresidentId",
                table: "Charterings");

            migrationBuilder.DropForeignKey(
                name: "FK_Charterings_Executives_PastorId",
                table: "Charterings");

            migrationBuilder.DropIndex(
                name: "IX_Charterings_CommanderId",
                table: "Charterings");

            migrationBuilder.DropIndex(
                name: "IX_Charterings_MmPresidentId",
                table: "Charterings");

            migrationBuilder.DropIndex(
                name: "IX_Charterings_PastorId",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "CommanderId",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "Fee",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "MmPresidentId",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "NumberOfChallengerRangers",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "PastorId",
                table: "Charterings");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Charterings");

            migrationBuilder.AddColumn<string>(
                name: "CommanderEmail",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CommanderName",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CommanderPhoneNumber",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MmPresidentEmail",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MmPresidentName",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MmPresidentPhoneNumber",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PastorEmail",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PastorName",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PastorPhoneNumber",
                table: "Charterings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Year",
                table: "Charterings",
                nullable: true);
        }
    }
}
