﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RoyalRangersAPI.Migrations
{
    public partial class updatedcampthings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Key",
                table: "CampTables");

            migrationBuilder.DropColumn(
                name: "Key",
                table: "CampRooms");

            migrationBuilder.DropColumn(
                name: "Key",
                table: "CampPatrols");

            migrationBuilder.DropColumn(
                name: "Key",
                table: "CampClasses");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Key",
                table: "CampTables",
                maxLength: 64,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Key",
                table: "CampRooms",
                maxLength: 64,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Key",
                table: "CampPatrols",
                maxLength: 64,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Key",
                table: "CampClasses",
                maxLength: 64,
                nullable: false,
                defaultValue: "");
        }
    }
}
