﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RoyalRangersAPI.Migrations
{
    public partial class updatedtheoutpostmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ChurchAddress",
                table: "Outposts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ChurchPhoneNumber",
                table: "Outposts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "Outposts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChurchAddress",
                table: "Outposts");

            migrationBuilder.DropColumn(
                name: "ChurchPhoneNumber",
                table: "Outposts");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Outposts");
        }
    }
}
