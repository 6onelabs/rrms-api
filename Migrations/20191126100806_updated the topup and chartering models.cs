﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RoyalRangersAPI.Migrations
{
    public partial class updatedthetopupandcharteringmodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Advancements_AdvancementCategory_CategoryId",
                table: "Advancements");

            migrationBuilder.DropForeignKey(
                name: "FK_Awards_AwardCategory_CategoryId",
                table: "Awards");

            migrationBuilder.DropForeignKey(
                name: "FK_Trainings_TrainingCategory_CategoryId",
                table: "Trainings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TrainingCategory",
                table: "TrainingCategory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AwardCategory",
                table: "AwardCategory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AdvancementCategory",
                table: "AdvancementCategory");

            migrationBuilder.RenameTable(
                name: "TrainingCategory",
                newName: "TrainingCategories");

            migrationBuilder.RenameTable(
                name: "AwardCategory",
                newName: "AwardCategories");

            migrationBuilder.RenameTable(
                name: "AdvancementCategory",
                newName: "AdvancementCategories");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TrainingCategories",
                table: "TrainingCategories",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AwardCategories",
                table: "AwardCategories",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AdvancementCategories",
                table: "AdvancementCategories",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Charterings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    OutpostId = table.Column<long>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Year = table.Column<string>(nullable: true),
                    CommanderName = table.Column<string>(nullable: true),
                    CommanderEmail = table.Column<string>(nullable: true),
                    CommanderPhoneNumber = table.Column<string>(nullable: true),
                    PastorName = table.Column<string>(nullable: true),
                    PastorEmail = table.Column<string>(nullable: true),
                    PastorPhoneNumber = table.Column<string>(nullable: true),
                    MmPresidentName = table.Column<string>(nullable: true),
                    MmPresidentEmail = table.Column<string>(nullable: true),
                    MmPresidentPhoneNumber = table.Column<string>(nullable: true),
                    TotalRangers = table.Column<int>(nullable: false),
                    NumberOfRagerKids = table.Column<int>(nullable: false),
                    NumberOfDiscoveryRangers = table.Column<int>(nullable: false),
                    NumberOfAdventureRangers = table.Column<int>(nullable: false),
                    NumberOfExpeditionRangers = table.Column<int>(nullable: false),
                    NumberOfCommanders = table.Column<int>(nullable: false),
                    VettedBy = table.Column<string>(nullable: true),
                    VettedOn = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Charterings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Charterings_Outposts_OutpostId",
                        column: x => x.OutpostId,
                        principalTable: "Outposts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OutpostBalances",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OutpostId = table.Column<long>(nullable: false),
                    Reference = table.Column<string>(maxLength: 32, nullable: false),
                    Amount = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutpostBalances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OutpostBalances_Outposts_OutpostId",
                        column: x => x.OutpostId,
                        principalTable: "Outposts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BalanceDebits",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    OutpostBalanceId = table.Column<long>(nullable: false),
                    Reference = table.Column<string>(maxLength: 32, nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BalanceDebits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BalanceDebits_OutpostBalances_OutpostBalanceId",
                        column: x => x.OutpostBalanceId,
                        principalTable: "OutpostBalances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ManualTopupTransactions",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    OutpostBalanceId = table.Column<long>(nullable: false),
                    Reference = table.Column<string>(maxLength: 32, nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Fee = table.Column<double>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManualTopupTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManualTopupTransactions_OutpostBalances_OutpostBalanceId",
                        column: x => x.OutpostBalanceId,
                        principalTable: "OutpostBalances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TopupTransactions",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    OutpostBalanceId = table.Column<long>(nullable: false),
                    Reference = table.Column<string>(maxLength: 32, nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    Provider = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    ExternalTransactionId = table.Column<string>(nullable: true),
                    Response = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopupTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TopupTransactions_OutpostBalances_OutpostBalanceId",
                        column: x => x.OutpostBalanceId,
                        principalTable: "OutpostBalances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BalanceDebits_OutpostBalanceId",
                table: "BalanceDebits",
                column: "OutpostBalanceId");

            migrationBuilder.CreateIndex(
                name: "IX_Charterings_OutpostId",
                table: "Charterings",
                column: "OutpostId");

            migrationBuilder.CreateIndex(
                name: "IX_ManualTopupTransactions_OutpostBalanceId",
                table: "ManualTopupTransactions",
                column: "OutpostBalanceId");

            migrationBuilder.CreateIndex(
                name: "IX_OutpostBalances_OutpostId",
                table: "OutpostBalances",
                column: "OutpostId");

            migrationBuilder.CreateIndex(
                name: "IX_TopupTransactions_OutpostBalanceId",
                table: "TopupTransactions",
                column: "OutpostBalanceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Advancements_AdvancementCategories_CategoryId",
                table: "Advancements",
                column: "CategoryId",
                principalTable: "AdvancementCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Awards_AwardCategories_CategoryId",
                table: "Awards",
                column: "CategoryId",
                principalTable: "AwardCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trainings_TrainingCategories_CategoryId",
                table: "Trainings",
                column: "CategoryId",
                principalTable: "TrainingCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Advancements_AdvancementCategories_CategoryId",
                table: "Advancements");

            migrationBuilder.DropForeignKey(
                name: "FK_Awards_AwardCategories_CategoryId",
                table: "Awards");

            migrationBuilder.DropForeignKey(
                name: "FK_Trainings_TrainingCategories_CategoryId",
                table: "Trainings");

            migrationBuilder.DropTable(
                name: "BalanceDebits");

            migrationBuilder.DropTable(
                name: "Charterings");

            migrationBuilder.DropTable(
                name: "ManualTopupTransactions");

            migrationBuilder.DropTable(
                name: "TopupTransactions");

            migrationBuilder.DropTable(
                name: "OutpostBalances");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TrainingCategories",
                table: "TrainingCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AwardCategories",
                table: "AwardCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AdvancementCategories",
                table: "AdvancementCategories");

            migrationBuilder.RenameTable(
                name: "TrainingCategories",
                newName: "TrainingCategory");

            migrationBuilder.RenameTable(
                name: "AwardCategories",
                newName: "AwardCategory");

            migrationBuilder.RenameTable(
                name: "AdvancementCategories",
                newName: "AdvancementCategory");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TrainingCategory",
                table: "TrainingCategory",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AwardCategory",
                table: "AwardCategory",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AdvancementCategory",
                table: "AdvancementCategory",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Advancements_AdvancementCategory_CategoryId",
                table: "Advancements",
                column: "CategoryId",
                principalTable: "AdvancementCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Awards_AwardCategory_CategoryId",
                table: "Awards",
                column: "CategoryId",
                principalTable: "AwardCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trainings_TrainingCategory_CategoryId",
                table: "Trainings",
                column: "CategoryId",
                principalTable: "TrainingCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
