﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class RangersFilter : Filter<Ranger>
    {
        public long Id;
        public long OutpostId;
        public string Name;
        public string Code;
        public string PhoneNumber; 
        public Gender? Gender;
        public RangerStatus? Status;
        public long SectorId;
        public long RegionId;
        public long DistrictId;
        

        public override IQueryable<Ranger> BuildQuery(IQueryable<Ranger> query)
        {
            query = query.Include(x => x.Outpost.District.Region.Sector).Include(x => x.Siblings);
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (OutpostId > 0) query = query.Where(x => x.OutpostId == OutpostId);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (Gender.HasValue) query = query.Where(x => x.Gender == Gender.Value);
            if (SectorId > 0) query = query.Where(x => x.Outpost.District.Region.SectorId == SectorId);
            if (RegionId > 0) query = query.Where(x => x.Outpost.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(x => x.Outpost.DistrictId == DistrictId);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
