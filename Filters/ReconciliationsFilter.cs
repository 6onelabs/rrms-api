﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class ReconciliationsFilter : Filter<Reconciliation>
    {
        public long Id;
        public long CampId;
        public ReconciliationType? Type;
        //public ReconciliationState? State;
        public ReconciliationStatus? Status;

        public override IQueryable<Reconciliation> BuildQuery(IQueryable<Reconciliation> query)
        {
            query = query.Include(x => x.Camp).Include(x => x.Details);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (CampId > 0) query = query.Where(q => q.CampId == CampId);
            if (Type.HasValue) query = query.Where(q => q.Type == Type);
            //if (State.HasValue) query = query.Where(q => q.State == State);
            if (Status.HasValue) query = query.Where(q => q.Status == Status);
            return query;
        }
    }
}
