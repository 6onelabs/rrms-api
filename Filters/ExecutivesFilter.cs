﻿using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class ExecutivesFilter : Filter<Executive>
    {
        public string PhoneNumber;
        public string Name;
        public long SectorId;
        public long RegionId;
        public long DistrictId;
        public long Id;

        public override IQueryable<Executive> BuildQuery(IQueryable<Executive> query)
        {
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (SectorId > 0) query = query.Where(x => x.District.Region.SectorId == SectorId);
            if (RegionId > 0) query = query.Where(x => x.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(x => x.DistrictId == DistrictId);
            if (Id > 0) query = query.Where(x => x.Id == Id);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
