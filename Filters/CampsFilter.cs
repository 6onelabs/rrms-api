﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class CampsFilter : Filter<Camp>
    {
        public long Id;
        public string Location;
        public string Name;
        public string Code;
        public string Theme;
        public bool? IsActive;

        public override IQueryable<Camp> BuildQuery(IQueryable<Camp> query)
        {
            query = query.Include(x => x.District).Include(x => x.Sector).Include(x => x.Region);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (IsActive.HasValue) query = query.Where(q => q.IsActive == IsActive);
            if (!string.IsNullOrEmpty(Theme)) query = query.Where(q => q.Theme.ToLower().Contains(Theme.ToLower()));
            if (!string.IsNullOrEmpty(Location)) query = query.Where(q => q.Location.ToLower().Contains(Location.ToLower()));
            if (Id > 0) query = query.Where(q => q.Id == Id);
            return query;
        }
    }
}
