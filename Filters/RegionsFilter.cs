﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class RegionsFilter : Filter<Region>
    {
        public string Name;
        public long SectorId;
        public long Id;

        public override IQueryable<Region> BuildQuery(IQueryable<Region> query)
        {
            query = query.Include(x => x.Sector);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (SectorId > 0) query = query.Where(x => x.SectorId == SectorId);
            if (Id > 0) query = query.Where(x => x.Id == Id);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
