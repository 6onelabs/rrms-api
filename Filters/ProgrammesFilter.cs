﻿using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class ProgrammesFilter : Filter<Programme>
    {
        public string Title;
        public string Notes;
        public long SectorId;
        public long RegionId;
        public long DistrictId;
        public long Id;

        public override IQueryable<Programme> BuildQuery(IQueryable<Programme> query)
        {
            if (!string.IsNullOrEmpty(Title)) query = query.Where(q => q.Title.ToLower().Contains(Title.ToLower()));
            if (!string.IsNullOrEmpty(Notes)) query = query.Where(q => q.Notes.ToLower().Contains(Notes.ToLower()));
            if (SectorId > 0) query = query.Where(x => x.District.Region.SectorId == SectorId);
            if (RegionId > 0) query = query.Where(x => x.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(x => x.DistrictId == DistrictId);
            if (Id > 0) query = query.Where(x => x.Id == Id);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
