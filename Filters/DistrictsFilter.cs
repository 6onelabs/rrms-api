﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class DistrictsFilter : Filter<District>
    {
        public string Name;
        public long SectorId;
        public long RegionId;
        public long Id;

        public override IQueryable<District> BuildQuery(IQueryable<District> query)
        {
            query = query.Include(x => x.Region.Sector);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (SectorId > 0) query = query.Where(x => x.Region.SectorId == SectorId);
            if (RegionId > 0) query = query.Where(x => x.RegionId == RegionId);
            if (Id > 0) query = query.Where(x => x.Id == Id);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
