﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class OutpostsFilter : Filter<Outpost>
    {
        public string Code;
        public string Name;
        public long SectorId;
        public long RegionId;
        public long DistrictId;
        public long Id;

        public override IQueryable<Outpost> BuildQuery(IQueryable<Outpost> query)
        {
            query = query.Include(x => x.District.Region.Sector).Include(x => x.Materials);
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (SectorId > 0) query = query.Where(x => x.District.Region.SectorId == SectorId);
            if (RegionId > 0) query = query.Where(x => x.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(x => x.DistrictId == DistrictId);
            if (Id > 0) query = query.Where(x => x.Id == Id);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class CharteringFilter : Filter<Chartering>
    {
        public string Code;
        public string Name;
        public long SectorId;
        public long RegionId;
        public long DistrictId;
        public long OutpostId;
        public long Id;
        public CharteringStatus? Status;

        public override IQueryable<Chartering> BuildQuery(IQueryable<Chartering> query)
        {
            query = query.Include(x => x.Outpost.District.Region.Sector);
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Outpost.Code.ToLower().Contains(Code.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Outpost.Name.ToLower().Contains(Name.ToLower()));
            if (SectorId > 0) query = query.Where(x => x.Outpost.District.Region.SectorId == SectorId);
            if (RegionId > 0) query = query.Where(x => x.Outpost.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(x => x.Outpost.DistrictId == DistrictId);
            if (OutpostId > 0) query = query.Where(x => x.OutpostId == OutpostId);
            if (Id > 0) query = query.Where(x => x.Id == Id);
            if (Status.HasValue) query = query.Where(x => x.Status == Status.Value);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
