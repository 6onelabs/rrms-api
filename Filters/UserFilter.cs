﻿using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class UserFilter : Filter<User>
    {
        public string Email;
        public string Username;
        public string Name;
        public string PhoneNumber;
        public long SectorId;
        public long RegionId;
        public long DistrictId;
        public long OutpostId;

        public override IQueryable<User> BuildQuery(IQueryable<User> query)
        {
            if (!string.IsNullOrEmpty(Email)) query = query.Where(q => q.Email.ToLower().Contains(Email.ToLower()));
            if (!string.IsNullOrEmpty(Username)) query = query.Where(q => q.UserName.ToLower().Contains(Username.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (SectorId > 0) query = query.Where(x => x.SectorId == SectorId);
            if (RegionId > 0) query = query.Where(x => x.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(x => x.DistrictId == DistrictId);
            if (OutpostId > 0) query = query.Where(x => x.OutpostId == OutpostId);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}
