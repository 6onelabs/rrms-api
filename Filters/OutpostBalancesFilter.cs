﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class OutpostBalancesFilter : Filter<OutpostBalance>
    {
        public long OutpostId;
        public string Code;
        public string Name;
        public long SectorId;
        public long RegionId;
        public long DistrictId;
        public long Id;

        public override IQueryable<OutpostBalance> BuildQuery(IQueryable<OutpostBalance> query)
        {
            query = query.Include(x => x.Outpost.District.Region.Sector);
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Outpost.Code.ToLower().Contains(Code.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Outpost.Name.ToLower().Contains(Name.ToLower()));
            if (SectorId > 0) query = query.Where(x => x.Outpost.District.Region.SectorId == SectorId);
            if (RegionId > 0) query = query.Where(x => x.Outpost.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(x => x.Outpost.DistrictId == DistrictId);
            if (OutpostId > 0) query = query.Where(x => x.OutpostId == OutpostId);
            if (Id > 0) query = query.Where(x => x.Id == Id);
            return query;
        }
    }

    public class OutpostBalanceStatementsFilter
    {
        public long OutpostId;
        public Pager Pager = new Pager();
    }

    public class OutpostBalanceStatementsFilterx
    {
        public Pager Pager = new Pager();
    }
}
