﻿using Microsoft.EntityFrameworkCore;
using RoyalRangersAPI.Models;
using System.Linq;

namespace RoyalRangersAPI.Filters
{
    public class CampMembersFilter : Filter<CampMember>
    {
        public long Id;
        public long CampId;
        public long RangerId;
        public long OutpostId;
        public long DistrictId;
        public long RegionId;
        public string Rank;
        public CampMemberStatus? Status;
        public string Name;

        public override IQueryable<CampMember> BuildQuery(IQueryable<CampMember> query)
        {
            query = query.Include(x => x.Camp).Include(x => x.Ranger.Outpost.District.Region);
            if (!string.IsNullOrEmpty(Rank)) query = query.Where(q => q.Rank.ToLower().Contains(Rank.ToLower()));
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Ranger.Name.ToLower().Contains(Name.ToLower()));
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (CampId > 0) query = query.Where(q => q.CampId == CampId);
            if (RangerId > 0) query = query.Where(q => q.RangerId == RangerId);
            if (OutpostId > 0) query = query.Where(q => q.Ranger.OutpostId == OutpostId);
            if (DistrictId > 0) query = query.Where(q => q.Ranger.Outpost.DistrictId == DistrictId);
            if (RegionId > 0) query = query.Where(q => q.Ranger.Outpost.District.RegionId == RegionId);
            if (Status.HasValue) query = query.Where(q => q.Status == Status);
            return query;
        }
    }

    public class AdminCampMembersFilter
    {
        public long CampId;
        public Pager Pager = new Pager();
    }
}
